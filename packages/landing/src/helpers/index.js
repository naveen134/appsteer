const AVERAGE_READING_SPEED = 265;

export const concatArrToStr = (arr) => {
  if (!arr || arr?.length === 0) return "";
  switch (arr.length) {
    case 0:
      return "";
    case 1:
      return arr[0];
    case 2:
      return arr.join(" and ");
    default:
      return `${arr.slice(0, -1).join(", ")} and ${arr[arr.length - 1]}`;
  }
};

export const formatData = (obj) => {
  const krr = [];
  for (let data in obj) {
    if (obj[data] !== "") {
      krr.push(
        ...[
          `${data}- `,
          typeof obj[data] === "string"
            ? `${obj[data]} \n `
            : `${concatArrToStr(obj[data])} \n`,
        ]
      );
    }
  }
  return krr.join(" ");
};

export const getPercentage = (number) => {
  // return % from the number without base but multiplied to nearest 10
  // 649 --> 60
  // 690 --> 70
  return Math.round(((number / 1000) * 100) / 10) * 10;
};

export const isFilled = (obj) => {
  if (!obj) return false;
  for (const key in obj) {
    if (Array.isArray(obj[key])) {
      if (obj[key].length === 0) return false;
    } else if (!obj[key]) {
      return false;
    }
  }
  return true;
};

export const isOthersSelected = (obj) => {
  if (!obj) return false;
  for (const key in obj) {
    if (obj[key]) {
      return true;
    }
  }
  return false;
};

export const generateFilterStr = (obj) => {
  let count = 1;
  let filter = "";
  for (let key in obj) {
    obj[key].forEach((value) => {
      filter = `${filter}filters[$or][${count}][${key}][text][$contains]=${value}&`;
      count++;
    });
  }
  return filter;
};

export const calculateReadingTime = (str) => {
  return Math.ceil(str?.length / AVERAGE_READING_SPEED);
};

export const stringShortner = (str, limit) => {
  if (!str) return "";
  if (!limit) return str;
  if (str?.length <= limit) return str;

  return `${str?.substr(0, limit)}...`;
};
