import React, { useState } from "react";
import Fade from "react-reveal/Fade";
import Text from "common/components/Text";
import Link from "next/link";
import Image from "common/components/Image";
import Heading from "common/components/Heading";
import Container from "common/components/UI/Container";
import BannerWrapper, {
  BannerContent,
  BannerImage,
  Pattern,
} from "./banner.style";
import PlayButton from "common/assets/image/PlayButton.svg";
import VideoModal from "../VideoModal";

const Banner = ({ data }) => {
  const { BannerTitle, BannerDescription } = data;
  const [modalStatus, setModalStatus] = useState(false);
  const [videoUrl, setUrl] = useState("");
  const showModal = () => setModalStatus(true);
  const closeModal = () => setModalStatus(false);
  const openModel = () => {
    setModalStatus(true);
    setUrl(data?.VideoLink);
  };
  return (
    <BannerWrapper id="banner_section">
      <Pattern />
      <Container>
        <BannerContent>
          <Fade up delay={100}>
            <Heading as="h1" content={BannerTitle} />
          </Fade>
          <Fade up delay={200}>
            <Text content={BannerDescription} />
          </Fade>
        </BannerContent>
        <BannerImage onClick={() => openModel()} className="heroImg">
          <Fade className="playButtonIcon" up delay={100}>
            <Image
              src={
                data?.BannerVideoImage?.data?.attributes?.formats?.small?.url
              }
              alt="Banner"
            />
            <Image src={PlayButton?.src} />
          </Fade>
        </BannerImage>
      </Container>
      {videoUrl && (
        <VideoModal url={videoUrl} open={modalStatus} close={closeModal} />
      )}
      {/* {console.log("first", videoUrl)} */}
      {/* <VideoModal url={data?.VideoLink} open={modalStatus} close={closeModal} /> */}
    </BannerWrapper>
  );
};

export default Banner;
