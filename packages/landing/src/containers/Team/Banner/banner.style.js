import styled from "styled-components";
import BannerPattern from "common/assets/image/usecase/UsecaseBannerBackground.png";

const BannerWrapper = styled.div`
  overflow: hidden;
  background-color: #191919;

  > div.container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    min-height: 805px;

    @media only screen and (max-width: 1224px) {
      min-height: auto;
      padding-top: 138px;
      padding-bottom: 100px;
      gap: 20px;
    }

    @media only screen and (max-width: 768px) {
      flex-direction: column;
      justify-content: flex-start;
      align-items: flex-start;
      gap: 0;
    }
  }
`;
export const Pattern = styled.div`
  @media only screen and (min-width: 319px) {
    position: absolute;
    width: 100%;
    min-height: 806px;
    // left: 50%;
    top: 0;
    &::before,
    &::after {
      content: "";
      position: absolute;
    }
    &::before {
      background: url(${BannerPattern?.src}) no-repeat;
      width: 50%;
      top: 0;
      left: 20%;
      height: 100%;
      opacity: 0.6;
      @media only screen and (min-width: 319px) {
        // background-size: 90%;
        // transform: rotate(347deg);
        // height: 139%;
        display: none;
      }
      @media only screen and (min-width: 768px) {
        // background-size: 70%;
        left: 5%;
        width: 95%;
        top: 30%;
        display: block;
      }
      @media only screen and (min-width: 1024px) {
        // background-size: 86%;
        left: 5%;
        width: 70%;
        top: 0;
      }
      @media only screen and (min-width: 1280px) {
        // background-size: 68%;
        left: 13%;
      }
      @media only screen and (min-width: 1440px) {
        // background-size: 80%;
        left: 20%;
      }
      @media only screen and (min-width: 1920px) {
        // background-size: 50%;
        left: 35%;
        width: 65%;
      }
    }
  }
`;

export const BannerContent = styled.div`
  width: 50%;
  @media only screen and (max-width: 768px) {
    width: 100%;
  }

  h1 {
    font-family: "Work Sans";
    font-weight: 500;
    font-size: 74px;
    line-height: 1.08;
    letter-spacing: -1px;
    color: white;
    margin-bottom: 36px;
    letter-spacing: -0.02em;
    // width: 506px;

    @media only screen and (max-width: 1224px) {
      width: 100%;
    }

    @media only screen and (max-width: 1024px) {
      font-size: 38px;
      line-height: 50px;
    }

    @media only screen and (max-width: 768px) {
      width: 100%;
      text-align: center;
      margin-bottom: 24px;
    }

    @media only screen and (min-width: 480px) and (max-width: 768px) {
      font-size: 55px;
      line-height: 75px;
    }
  }

  p {
    color: white;
    font-size: 20px;
    line-height: 23px;
    font-weight: 400;
    word-spacing: 1px;
    font-family: "Work Sans";
    width: auto;

    @media only screen and (max-width: 1224px) {
      font-size: 18px;
      line-height: 32px;
    }

    @media only screen and (max-width: 1220px) {
      width: 100%;
    }

    @media only screen and (max-width: 777px) {
      text-align: center;
    }

    @media only screen and (max-width: 480px) {
      font-size: 14px;
      line-height: 32px;
    }
  }
`;

export const BannerImage = styled.div`
  flex-shrink: 0;
  width: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  .react-reveal {
    &:nth-child(2) {
      position: absolute;
      cursor: pointer;
    }
  }
  @media only screen and (max-width: 768px) {
    width: 100%;
    margin-top: 60px;
  }

  img {
    width: 100%;
    margin-left: 80px;

    @media only screen and (max-width: 1600px) {
      margin-left: 0;
    }

    /* @media only screen and (max-width: 480px) {
      max-width: 70%;
    } */
  }
`;

export default BannerWrapper;
