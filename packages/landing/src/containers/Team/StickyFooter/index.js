import React from "react";
import Container from "common/components/UI/Container";
import Text from "common/components/Text";
import Heading from "common/components/Heading";
import Link from "next/link";
import Section, {
  Title,
  Description,
  TextWrappr,
  ButtonWrapper,
  StickyFooterWrapper,
} from "./stickyFooter.style";

const StickyFooter = () => {
  return (
    <Section>
      <Container width="1300px">
        <StickyFooterWrapper>
          <TextWrappr>
            <Title>
              <Heading as="h2" content={"Interested to Join?"} />
            </Title>
            <Description>
              <Text content="Check our careers page and apply" />
            </Description>
          </TextWrappr>
          <ButtonWrapper>
            <Link href="/contact-us">
              <a className="button">
                <span>
                  Join us
                  {/* <Icon icon={androidArrowForward} size={16} /> */}
                </span>
              </a>
            </Link>
          </ButtonWrapper>
        </StickyFooterWrapper>
      </Container>
    </Section>
  );
};

export default StickyFooter;
