import GlideCarousel from "common/components/GlideCarousel";
import GlideSlide from "common/components/GlideCarousel/glideSlide";
import Image from "common/components/Image";
import { Fragment } from "react";
import {
  Grid,
  Heading,
  SliderWrapper,
  BackgroundImage,
  HeadingWrapper,
} from "./GroupImages.style";
import Slider from "react-slick";
const settings = {
  infinite: false,
  accessibility: true,
  speed: 500,
  rows: 2,
  slidesPerRow: 1,
  slidesToShow: 6,
  autoplaySpeed: 2000,
  swipeToSlide: true,
  swipe: true,
  draggable: true,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  initialSlide: 0,

  responsive: [
    {
      breakpoint: 10000, // a unrealistically big number to cover up greatest screen resolution
      settings: "unslick",
    },
    {
      breakpoint: 1920,
      settings: {
        slidesToShow: 7,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1680,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1440,
      settings: {
        slidesToShow: 5.5,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1280,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 770,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 2.5,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1.5,
        slidesToScroll: 1,
      },
    },
  ],
};
const GroupImages = ({ data }) => {
  return (
    <Fragment>
      <HeadingWrapper>
        <Heading>See our team in action</Heading>
      </HeadingWrapper>

      <SliderWrapper>
        <Slider {...settings}>
          {data?.TeamImages.map((data, i) => {
            return (
              <GlideSlide key={i}>
                {/* {console.log("data", data?.TeamImages)} */}
                <BackgroundImage
                  background={data?.TeamImages?.data[0]?.attributes?.url}
                ></BackgroundImage>
                {/* <Image
                  src={
                    data?.TeamImages?.data[0]?.attributes?.formats?.thumbnail
                      ?.url
                  }
                /> */}
              </GlideSlide>
            );
          })}
        </Slider>
      </SliderWrapper>
    </Fragment>
  );
};

export default GroupImages;
