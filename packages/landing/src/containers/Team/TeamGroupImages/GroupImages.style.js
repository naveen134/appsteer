import styled from "styled-components";
export const HeadingWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const Heading = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 62px;
  line-height: 73px;
  text-align: center;
  letter-spacing: -0.02em;
  color: #191919;
  margin-top: 120px;
  margin-bottom: 60px;
  @media only screen and (max-width: 768px) {
    font-size: 40px;
    width: 80%;
    line-height: 60px;
    margin-bottom: 50px;
    margin-top: 100px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 30px;
    margin-bottom: 40px;
    margin-top: 80px;
  }
`;
export const BackgroundImage = styled.div`
  background: url(${(props) => props.background});
  /* width: 242px; */
  height: 242px;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
`;
export const SliderWrapper = styled.div`
  margin-top: 45px;
  max-width: 100%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 5%;
  .mySwiperCatergory {
    .swiper-wrapper {
      .swiper-slide {
        width: 250px !important;
        height: 250px;
        border: 1px solid yellow;
        img {
          width: 100%;
          height: 100%;
        }
      }
    }
  }
  @media only screen and (max-width: 768px) {
    margin-top: 15px;
  }
  .slider {
    display: flex;
    justify-content: space-between;
  }
  figure {
    img {
      // opacity: 0.7;
    }
  }
  .slick-slide > div {
    display: flex;
    align-items: center;
    min-height: 242px;
    min-width: 242px;
    &:nth-child(1) {
      margin-left: 50%;
    }
    &:nth-child(2) {
      margin-top: 30px;
      margin-left: 100%;
    }
    .glide__slide {
    }
    img {
      margin: 0 auto;
      width: 100%;
      height: 100%;
      padding: 10px;
    }
  }
`;
