// import Card from "common/components/Card";
import Container from "common/components/UI/Container";
import {
  Heading,
  TitleHeading,
  CardWrapper,
  Card,
  CardTitle,
  CardJobTitle,
  Description,
  CardImage,
  CardContainer,
  CardHeading,
  SocialLink,
  SocialLinkHeading,
  HeadingWrapper,
} from "./CoreAdvisory.style";
import Image from "common/components/Image";
import LinkedInIcon from "common/assets/image/LinkedInIcon.svg";
import TwitterIcon from "common/assets/image/TwitterIcon.svg";
const CoreAdvisoryCard = ({ data }) => {
  // console.log("first", TwitterIcon, LinkedInIcon);
  return (
    <Container>
      <Heading>
        <TitleHeading>Leaders of the pack</TitleHeading>
      </Heading>
      <TitleHeading>{data?.CoreTeamTitle}</TitleHeading>
      <CardContainer>
        {data?.CoreTeam.map((data, i) => {
          return (
            <CardWrapper key={i}>
              <Card>
                <CardImage>
                  <Image
                    src={data?.ProfileImage?.data?.attributes?.url}
                    alt="Image"
                  ></Image>{" "}
                </CardImage>
                <HeadingWrapper>
                  <CardHeading>
                    <CardTitle>{data?.Name},&nbsp;</CardTitle>
                    <CardJobTitle>{data?.JobTitle}</CardJobTitle>
                  </CardHeading>
                  <SocialLinkHeading>
                    <SocialLink>
                      <Image src={LinkedInIcon?.src} alt="Image" />
                    </SocialLink>
                    <SocialLink>
                      <Image src={TwitterIcon?.src} alt="Image" />
                    </SocialLink>
                  </SocialLinkHeading>
                </HeadingWrapper>
              </Card>
              <Description>{data?.JobDescription}</Description>
            </CardWrapper>
          );
        })}
      </CardContainer>
      <TitleHeading>{data?.AdvisoryTeamTitle}</TitleHeading>
      <CardContainer>
        {data?.AdvisoryTeam.map((data, i) => {
          return (
            <CardWrapper key={i}>
              <Card>
                <CardImage>
                  <Image
                    src={data?.ProfileImage?.data?.attributes?.url}
                    alt="Image"
                  ></Image>{" "}
                </CardImage>
                <HeadingWrapper>
                  <CardHeading>
                    <CardTitle>{data?.Name},&nbsp;</CardTitle>
                    <CardJobTitle>{data?.JobTitle}</CardJobTitle>
                  </CardHeading>
                  <SocialLinkHeading>
                    <SocialLink>
                      <Image src={LinkedInIcon?.src} alt="Image" />
                    </SocialLink>
                    <SocialLink>
                      <Image src={TwitterIcon?.src} alt="Image" />
                    </SocialLink>
                  </SocialLinkHeading>
                </HeadingWrapper>
              </Card>
              <Description>{data?.JobDescription}</Description>
            </CardWrapper>
          );
        })}
      </CardContainer>
    </Container>
  );
};

export default CoreAdvisoryCard;
