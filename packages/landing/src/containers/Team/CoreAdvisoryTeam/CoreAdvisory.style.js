import styled from "styled-components";

export const Heading = styled.div`
  margin-top: 120px;
  text-align: center;
  @media only screen and (max-width: 768px) {
    margin-top: 90px;
  }
`;
export const TitleHeading = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  letter-spacing: -0.02em;
  color: #191919;
  margin-top: 60px;
  @media only screen and (max-width: 768px) {
    font-size: 35px;
    margin-top: 40px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 28px;
  }
`;
export const CardContainer = styled.div`
  flex: 0 0 0;
  display: grid;
  grid-gap: 25px;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  @media only screen and (max-width: 991px) {
    flex: 0 0 100%;
    max-width: 79%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 0px;
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  @media only screen and (max-width: 768px) {
    max-width: 100%;
    grid-gap: 20px;
  }
  @media only screen and (max-width: 426px) {
    grid-gap: 25px;
    grid-template-columns: repeat(1, minmax(0, 1fr));
  }
`;
export const CardWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
  margin-top: 60px;
  padding: 40px;
  @media only screen and (max-width: 768px) {
    margin-top: 40px;
  }
`;
export const Card = styled.div`
  display: flex;
  align-items: center;
`;
export const CardHeading = styled.div`
  margin-left: 20px;
  display: flex;
  flex-direction: row;
  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
`;
export const CardTitle = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  letter-spacing: -0.02em;
  color: #191919;
  @media only screen and (max-width: 768px) {
    font-size: 20px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 16px;
  }
`;
export const CardJobTitle = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  letter-spacing: -0.02em;
  color: #666666;
  @media only screen and (max-width: 768px) {
    font-size: 20px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 12px;
  }
`;
export const Description = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 32px;
  letter-spacing: 0.01em;
  text-align: justify;
  color: #666666;
  margin-top: 30px;
  @media only screen and (max-width: 768px) {
    font-size: 14px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 12px;
  }
`;
export const SocialLink = styled.div`
  width: 42px;
  height: 42px;
  background: #f5f5f5;
  border-radius: 20px;
  img {
    margin: 10px;
  }
`;
export const SocialLinkHeading = styled.div`
  display: flex;
  margin-left: 20px;
  margin-top: 20px;
  gap: 10px;
`;
export const HeadingWrapper = styled.div``;
export const CardImage = styled.div``;
