import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";

export const FounderTeamWrapper = styled.div``;

export const Heading = styled.div`
  background: #fff4eb;
  margin-top: 120px;
  padding-top: 120px;
  padding-bottom: 60px;
  display: flex;
  justify-content: center;
`;
export const Title = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 62px;
  line-height: 73px;
  text-align: center;
  letter-spacing: -0.02em;
  color: #191919;
  width: 60%;
  @media only screen and (max-width: 768px) {
    font-size: 40px;
    width: 80%;
  }
  @media only screen and (max-width: 480px) {
    font-size: 30px;
  }
`;
export const CardWrapper = styled.div``;
export const CardContentWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  @media only screen and (max-width: 900px) {
    flex-direction: column;
  }
  &:nth-child(odd) {
    background: #fff4eb;
    .container {
      .cardImage {
        img {
          &:nth-child(1) {
            float: right;
          }
        }
      }
    }
  }
  &:nth-child(even) {
    .container {
      display: flex;
      flex-direction: row-reverse;
      @media only screen and (max-width: 900px) {
        flex-direction: column;
      }
    }
  }
  .container {
    display: flex;
    justify-content: center;
    align-items: center;
    @media only screen and (max-width: 900px) {
      flex-direction: column;
    }
  }
`;
export const CardContent = styled.div`
  width: 50%;
  @media only screen and (max-width: 900px) {
    width: 100%;
  }
`;
export const CardImage = styled.div`
  width: 50%;
  @media only screen and (max-width: 900px) {
    width: 100%;
  }
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    &:nth-child(2) {
      position: absolute;
      cursor: pointer;
    }
  }
`;
export const FounderName = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  letter-spacing: -0.02em;
  color: #191919;
  @media only screen and (max-width: 768px) {
    font-size: 40px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 20px;
  }
`;
export const TitleHeader = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  letter-spacing: -0.02em;

  color: #666666;
  padding-top: 18px;
  padding-bottom: 36px;
  @media only screen and (max-width: 768px) {
    font-size: 20px;
    padding-bottom: 15px;
    padding-top: 10px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 16px;
    padding-bottom: 15px;
    padding-top: 5px;
  }
`;
export const Description = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 32px;
  /* or 178% */

  letter-spacing: 0.01em;

  color: #666666;
  text-align: justify;
  @media only screen and (max-width: 768px) {
    font-size: 14px;
  }
  @media only screen and (max-width: 480px) {
    font-size: 12px;
  }
`;
export const VideoSectionWrapper = styled.section`
  padding: 80px 0;
  overflow: hidden;

  @media (max-width: 990px) {
    padding: 60px 0;
  }
  @media (max-width: 767px) {
    padding: 30px 0 60px 0;
  }

  .figure {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;

    img {
      border-radius: 4px;
    }

    .fig__caption {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      z-index: 2;

      .reusecore__button {
        .btn-icon {
          background-color: ${themeGet("colors.white", "#ffffff")};
          line-height: 0.4;
        }
      }
    }
  }
`;
