import React, { useState } from "react";

import Image from "common/components/Image";
import Container from "common/components/UI/Container";
import {
  FounderTeamWrapper,
  Heading,
  Title,
  CardWrapper,
  CardContentWrapper,
  CardContent,
  CardImage,
  FounderName,
  TitleHeader,
  Description,
} from "./FounderTeam.style";
import PlayButton from "common/assets/image/PlayButton.svg";
import VideoModal from "../VideoModal";

const FounderTeam = ({ data }) => {
  const [modalStatus, setModalStatus] = useState(false);
  const [videoUrl, setUrl] = useState("");
  const showModal = () => setModalStatus(true);
  const closeModal = () => setModalStatus(false);
  const openModel = (i) => {
    setModalStatus(true);
    setUrl(data?.FoundersTeam[i]?.VideoLink);
  };

  return (
    <FounderTeamWrapper>
      <Heading>
        <Title>Our Founders vision on the future of the company</Title>
      </Heading>

      <CardWrapper>
        {data?.FoundersTeam.map((data, i) => {
          return (
            <CardContentWrapper key={i}>
              <Container className="container">
                <CardContent key={i}>
                  <FounderName>{data?.FounderName}</FounderName>
                  <TitleHeader>
                    {data?.FounderTitle},&nbsp;{data?.JobTitle}
                  </TitleHeader>
                  <Description>{data?.JobDescription}</Description>
                </CardContent>

                <CardImage className="cardImage" onClick={() => openModel(i)}>
                  <Image
                    src={
                      data?.ProfileVideoImage?.data?.attributes?.formats?.small
                        ?.url
                    }
                    alt="Image"
                  />
                  <Image src={PlayButton?.src} />
                </CardImage>
              </Container>
            </CardContentWrapper>
          );
        })}
        {videoUrl && (
          <VideoModal url={videoUrl} open={modalStatus} close={closeModal} />
        )}
      </CardWrapper>
    </FounderTeamWrapper>
  );
};

export default FounderTeam;
