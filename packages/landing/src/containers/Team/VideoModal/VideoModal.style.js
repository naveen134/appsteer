import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";

export const VideoWrapper = styled.div`
  .ReactModal__Content {
    width: 70% !important;
    @media only screen and (max-width: 667px) {
      width: 90% !important;
    }
  }
  .ReactModal__Content--after-open {
    width: 70% !important;
    @media only screen and (max-width: 667px) {
      width: 90% !important;
    }
  }
  .modalCloseBtn {
    position: fixed !important;
    z-index: 999991 !important;
    background-color: transparent !important;
    top: 10px !important;
    right: 10px !important;
    min-width: 34px !important;
    min-height: 34px !important;
    padding: 0 !important;
    span.btn-icon {
      font-size: 22px !important;
      transform: rotate(45deg) !important;
    }
  }
`;
