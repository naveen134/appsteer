import Box from "common/components/Box";
import Button from "common/components/Button";
import ModalContact from "common/components/Modal";
import Container from "common/components/UI/Container";
import { VideoWrapper } from "./VideoModal.style";
import YouTube from "react-youtube";

const VideoModal = ({ url, open, close }) => {
  const opts = {
    height: "400",
    width: "100%",
    playerVars: {
      autoplay: 1,
    },
  };
  return (
    <ModalContact open={open} close={close}>
      <VideoWrapper>
        <Container>
          <Box>
            <YouTube
              className="video-box"
              id="youtube-video-wrapper"
              videoId={url}
              opts={opts}
            />
            <Button
              className="modalCloseBtn"
              variant="fab"
              onClick={close}
              icon={<i className="flaticon-plus-symbol" />}
            />
          </Box>
        </Container>
      </VideoWrapper>
    </ModalContact>
  );
};

export default VideoModal;
