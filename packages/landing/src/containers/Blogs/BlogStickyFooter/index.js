import React, { useState } from "react";
import Container from "common/components/UI/Container";
import Text from "common/components/Text";
import Heading from "common/components/Heading";
import {
  Section,
  Title,
  Description,
  TextWrappr,
  ButtonWrapper,
  StickyFooterWrapper,
  SubscribeButton,
  SubscribeModalContainer,
  SubscribeModalHeading,
  SubscribeModalCloseButton,
  ImageContainerWrapper,
} from "./blogStickyFooter.style";
import ModalContact from "common/components/Modal";
import NewsletterForm from "./NewsletterForm";
import { ic_close } from "react-icons-kit/md/ic_close";
import Icon from "react-icons-kit";
import bgIcon from "common/assets/image/about-us/subscribeModalImage.png";

const BlogStickyFooter = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  return (
    <Section>
      <Container width="1300px">
        <StickyFooterWrapper>
          <TextWrappr>
            <Title>
              <Heading as="h2" content={"Stay Connected"} />
            </Title>
            <Description>
              <Text content="Subscribe to our Weekly Newsletter" />
            </Description>
          </TextWrappr>
          <ButtonWrapper>
            <SubscribeButton onClick={() => setIsModalOpen(true)}>
              Subscribe
            </SubscribeButton>
          </ButtonWrapper>
        </StickyFooterWrapper>
      </Container>
      <ModalContact open={isModalOpen} close={() => setIsModalOpen(false)}>
        <SubscribeModalContainer>
          <SubscribeModalCloseButton onClick={() => setIsModalOpen(false)}>
            <Icon icon={ic_close} />
          </SubscribeModalCloseButton>
          <SubscribeModalHeading>
            Don’t miss out on our Weekly Newsletter!
          </SubscribeModalHeading>
          <NewsletterForm />
          <ImageContainerWrapper style={{ backgroundImage: `url(${bgIcon.src})` }} >
            {/* <img src={bgIcon} alt="image" /> */}
          </ImageContainerWrapper>
        </SubscribeModalContainer>
      </ModalContact>
    </Section>
  );
};

export default BlogStickyFooter;
