import themeGet from "@styled-system/theme-get";
import { rgba } from "polished";
import styled from "styled-components";

import StikyBgLogo from "common/assets/image/appsteer/sticky.png";

export const Section = styled.section`
  // padding-top: 40px;
  // padding-bottom: 40px;
  margin-bottom: 50px;
  background: ${themeGet("colors.white")};
  filter: drop-shadow(0px 0px 124px rgba(0, 0, 0, 0.08));
  border-radius: 40px;
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const Title = styled.div`
  h2 {
    font-weight: 600;
    font-size: 62px;
    line-height: 74px;

    @media only screen and (max-width: 426px) {
      font-size: 30px;
      line-height: 46px;
    }

    @media only screen and (min-width: 768px) {
      font-size: 40px;
      margin: 0;
      max-width: 470px;
    }
    @media only screen and (min-width: 1024px) {
      font-size: 50px;
      max-width: 1170px;
    }
    @media only screen and (min-width: 1440px) {
      font-size: 50px;
      max-width: 1170px;
    }

    @media (max-width: 468px) {
      font-size: 28px;
      line-height: 32px;
    }
  }
`;

export const StickyFooterWrapper = styled.div`
  display: block;
  padding: 60px 0;
  align-items: center;
  justify-content: space-between;
  background-image: url(${StikyBgLogo?.src});
  background-position: 87% 0;
  background-repeat: no-repeat;
  background-size: contain;
  text-align: center;
  @media only screen and (min-width: 319px) {
    padding: 20px;
    display: flex;
    text-align: center;
    flex-direction: column;
  }
  @media only screen and (min-width: 426px) {
    display: flex;
    text-align: left;
  }
  @media only screen and (min-width: 768px) {
    display: flex;
    flex-direction: row;
    text-align: left;
  }

  @media (max-width: 468px) {
    padding: 20px 0px;
  }
`;

export const TextWrappr = styled.div`
  margin-bottom: 30px;
  @media only screen and(min-width:319px) {
    margin-bottom: 20px;
  }
  @media (max-width: 468px) {
    margin-bottom: 15px;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SubscribeButton = styled.button`
  display: inline-flex;
  background-color: ${themeGet("colors.primary")};
  color: ${themeGet("colors.black")};
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  font-weight: 500;
  font-size: 20px;
  line-height: 22px;
  position: relative;
  padding: 19.5px 35px;
  border: none;
  outline: none;
  &:hover {
    background-color: ${themeGet("colors.primaryHover")};
  }
  transition: all 0.4s ease;
  i {
    margin-left: 10px;
    position: relative;
    top: -1px;
  }
  span {
    position: relative;
    display: flex;
  }
  @media (max-width: 468px) {
    padding: 10px 20px;
    font-size: 16px;
  }
`;

export const Description = styled.div`
  margin-top: 10px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  max-width: 1170px;
  p {
    font-weight: 500;
    font-size: 17px;
    margin-bottom: 0;
    color: ${rgba("#5B6F82", 0.6)};
    @media (max-width: 480px) {
      text-align: center;
      margin: auto;
    }
  }
`;

export const Figure = styled.figure`
  margin: 0;
  text-align: center;
  display: flex;
  align-items: center;
  // opacity: 0.7;
  transition: all 0.4s ease;
  &:hover {
    cursor: pointer;
    animation: var(--wobbleVertical);
    // opacity: 0.9;
  }
`;

export const SliderWrapper = styled.div`
  margin-top: 45px;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  .slider {
    display: flex;
    justify-content: space-between;
  }
  figure {
    img {
      // opacity: 0.7;
    }
  }
  .slick-slide > div {
    display: flex;
    min-height: 35px;
    align-items: center;
    img {
      margin: 0 auto;
    }
  }
  .slick-dots {
    display: flex !important;
    align-items: center;
    justify-content: center;
    margin-top: 30px;
    button {
      background-color: rgba(0, 0, 0, 0.2);
      border: 0;
      border-radius: 20px;
      height: 10px;
      width: 10px;
      margin: 0 5px;
      padding: 0;
      text-indent: -9999em;
    }
    .slick-active button {
      background-color: rgba(0, 0, 0, 0.6);
    }
  }
`;

export const SubscribeModalContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 0px 6rem;
  background-color: #ffffff;
  border-radius: 30px;
  @media (max-width: 480px) {
    padding: 0px;
  }
`;

export const SubscribeModalHeading = styled.h1`
  font-weight: 500;
  font-size: 24px;
  line-height: 36px;
  text-align: center;
  color: #000000;
  margin-top: 60px;
  margin-bottom: 30px;
  max-width: 270px;
`;
export const ImageContainerWrapper = styled.div`
  width: 20%;
  height: 35%;
  position: absolute;
  bottom: 0px;
  right: 0px;
  background-repeat: no-repeat;
  background-postion: center;
  margin: 0px;
`;

export const NewsletterButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  .reusecore__button {
    margin-top: 50px;
    width: 225px;
    height: 54px;
    padding: 14px 36px;
    border-radius: 30px;
    font-family: "Work Sans";
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 23px;
    letter-spacing: -0.02em;

    color: #191919;
  }
`;

export const SubscribeModalCloseButton = styled.button`
  position: absolute;
  top: 30px;
  right: 40px;
  border: none;
  outline: none;
  background: transparent;
  transform: scale(2);
  cursor: pointer;
  @media (max-width: 480px) {
    top: -5px;
    right: -5px;
    transform: scale(1.5);
  }
`;
