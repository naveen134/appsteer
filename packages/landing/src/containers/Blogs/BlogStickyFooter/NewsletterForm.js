import React, { useState } from "react";
import Input from "common/components/Input";
import { FormWrapper } from "containers/ContactUs/Form/connectModal.style";
import Button from "common/components/Button";
import { NewsletterButtonWrapper } from "./blogStickyFooter.style";

const NewsletterForm = () => {
  const [inputValues, setInputValue] = useState({ Email: "" });
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);
  const Emailregex =
    /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,6}))$/;

  const handleChange = (name, event) => {
    const value = event;
    setInputValue({ ...inputValues, [name]: value });
  };
  const handleBlur = (name, value) => {
    let errors = formErrors;
    switch (name) {
      case "Email": {
        if (value === "") {
          errors.Email = "Please enter the Email";
          setFormErrors({ ...errors });
        } else if (!Emailregex.test(value)) {
          errors.Email = "Please enter a valid Email";
          setFormErrors({ ...errors });
        } else {
          errors.Email = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      default:
        return;
    }
  };
  const checkValidation = (values) => {
    let errors = {};

    if (!values.Email) {
      errors.Email = "Email required!";
    } else if (!Emailregex.test(values.Email)) {
      errors.Email = "Enter valid Email!";
    }
    return errors;
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(checkValidation(inputValues));
    setIsSubmit(true);
  };

  return (
    <FormWrapper inputValue={inputValues}>
      <form onSubmit={handleSubmit}>
        <Input
          label="Email"
          onChange={(e) => handleChange("Email", e)}
          onBlur={(e) => handleBlur("Email", e?.target?.value)}
          name="Email"
          value={inputValues.Email}
          className={
            formErrors.Email
              ? "error_input"
              : inputValues.Email
              ? "input_value"
              : ""
          }
          icon={formErrors.Email}
        />
        <NewsletterButtonWrapper>
          <Button type="submit" title="Submit" iconPosition="center" />
        </NewsletterButtonWrapper>
      </form>
    </FormWrapper>
  );
};

export default NewsletterForm;
