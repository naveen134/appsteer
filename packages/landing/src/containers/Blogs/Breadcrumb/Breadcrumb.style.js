import styled from "styled-components";

export const BreadcrumbContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 16px;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  margin-top: 27px;
  color: #888888;
  .breadcrumb-link {
    color: #888888;
    text-transform: capitalize;
    &.active {
      color: #077bb2;
    }
  }
`;
