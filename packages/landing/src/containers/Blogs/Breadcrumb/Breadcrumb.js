import React, { Fragment } from "react";
import { BreadcrumbContainer } from "./Breadcrumb.style";
import { ic_keyboard_arrow_right_outline } from "react-icons-kit/md/ic_keyboard_arrow_right_outline";
import { ic_home_outline } from "react-icons-kit/md/ic_home_outline";
import Icon from "react-icons-kit";
import Link from "next/link";

const Breadcrumb = ({ crumbs }) => {
  return (
    <BreadcrumbContainer>
      <Link href="/">
        <a className="breadcrumb-link">
          <Icon icon={ic_home_outline} />
        </a>
      </Link>
      <Icon icon={ic_keyboard_arrow_right_outline} />
      <Link href="/blogs">
        <a className="breadcrumb-link">Blogs</a>
      </Link>
      <Icon icon={ic_keyboard_arrow_right_outline} />
      {crumbs?.map((crumb, index) => (
        <Fragment key={`blog--breadcrumb-${index}`}>
          <Link href={`/blogs/${crumb?.slug}`}>
            <a
              className={`breadcrumb-link ${
                index === crumbs?.length - 1 ? "active" : ""
              }`}
            >
              {crumb?.name?.length <= 15
                ? crumb?.name
                : `${crumb?.name?.substr(0, 15)}...`}
            </a>
          </Link>
          {!(index === crumbs?.length - 1) && (
            <Icon icon={ic_keyboard_arrow_right_outline} />
          )}
        </Fragment>
      ))}
    </BreadcrumbContainer>
  );
};

export default Breadcrumb;
