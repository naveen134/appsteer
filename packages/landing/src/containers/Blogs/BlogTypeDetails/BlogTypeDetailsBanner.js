import Container from "common/components/UI/Container";
import { Fade } from "react-reveal";
import Breadcrumb from "../Breadcrumb/Breadcrumb";
import { Section, BannerText } from "./BlogTypeDetailsBanner.style";

const BlogTypeDetailsBanner = ({ data }) => {
  return (
    <Section>
      <Container className="blog-container">
        <Fade up>
          <Breadcrumb crumbs={[{ name: data?.type, slug: data?.slug }]} />
        </Fade>
        <Fade up delay={100}>
          <BannerText>{data?.type}</BannerText>
        </Fade>
      </Container>
    </Section>
  );
};

export default BlogTypeDetailsBanner;
