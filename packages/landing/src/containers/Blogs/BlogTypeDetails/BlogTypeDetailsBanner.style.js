import styled from "styled-components";

export const Section = styled.section`
  padding: 6rem 0 0rem;
  overflow: hidden;
  position: inherit;
  background: #f5f5f5;
  @media (max-width: 1600px) {
    padding: 10rem 0 3rem;767px
  }
  @media (max-width: 1440px) {
    padding: 7rem 0 0rem;
  }
  @media (max-width: 768px) {
    padding: 5rem 0 0rem;
  }
  @media (max-width: 767px) {
    padding: 5rem 0 0;
  }
`;

export const BannerText = styled.h1`
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  text-align: center;
  color: #191919;
  margin-top: 54px;
  margin-bottom: 0px;
  text-transform: capitalize;
  @media (max-width: 468px) {
    font-size: 28px;
  }
`;
