import Container from "common/components/UI/Container";
import {
  BlogsListWrapper,
  ListWrapper,
  ShowMoreBtn,
  ShowMoreBtnWrapper,
} from "./BlogList.style";
import BlogSearch from "./BlogSearch";
import BlogItem from "./BlogItem/BlogItem";
import { useEffect, useMemo, useState } from "react";
import { debounce } from "lodash";
import { fetcher } from "../../../../lib/api";

const BlogList = ({
  textSearch,
  popularitySearch,
  noSearch,
  blogs,
  blogType,
  expandable = true,
}) => {
  const [isBlogEnd, setIsBlogEnd] = useState(false);
  const [currentPage, setCurrentPage] = useState(10);
  const [blogsList, setBlogList] = useState(blogs);
  const [searchTerm, setSearchTerm] = useState("");
  const [sort, setSort] = useState("");

  const handleChange = (val) => {
    setIsBlogEnd(false);
    setCurrentPage(10);
    setSearchTerm(val);
  };

  const handleSortChange = (val) => {
    setIsBlogEnd(false);
    setCurrentPage(10);
    setSort(val);
  };

  const searchBlog = async () => {
    let blogsData = await fetcher(
      !blogType
        ? `${
            process.env.NEXT_PUBLIC_STRAPI_URL
          }/blogs?start=0&&limit=10&sort=publishDate:${
            sort === "oldest" ? "DESC" : "ASC"
          }${searchTerm ? `&filters[title][$containsi]=${searchTerm}` : ""}`
        : `${
            process.env.NEXT_PUBLIC_STRAPI_URL
          }/blogs?start=0&limit=10&sort=publishDate:${
            sort === "oldest" ? "DESC" : "ASC"
          }&filters[blog_type][slug]=${blogType}${
            searchTerm ? `&filters[title][$containsi]=${searchTerm}` : ""
          }`
    );
    setBlogList(blogsData?.data);
  };

  const loadMoreBlogs = async () => {
    let blogsData = await fetcher(
      !blogType
        ? `${process.env.NEXT_PUBLIC_STRAPI_URL}/blogs?sort=publishDate:${
            sort ? (sort === "oldest" ? "DESC" : "ASC") : "ASC"
          }${
            searchTerm ? `&filters[title][$containsi]=${searchTerm}` : ""
          }&limit=10&start=${currentPage}`
        : `${process.env.NEXT_PUBLIC_STRAPI_URL}/blogs?sort=publishDate:${
            sort ? (sort === "oldest" ? "DESC" : "ASC") : "ASC"
          }&limit=10&filters[blog_type][slug]=${blogType}${
            searchTerm ? `&filters[title][$containsi]=${searchTerm}` : ""
          }&start=${currentPage}`
    );
    if (blogsData?.data?.length === 0) {
      setIsBlogEnd(true);
    }
    setCurrentPage((prevState) => prevState + 10);
    setBlogList([...blogsList, ...blogsData?.data]);
  };

  const debouncedSearch = useMemo(() => {
    return debounce(searchBlog, 300);
  }, [searchTerm, sort]);

  useEffect(() => {
    if (searchTerm || sort) debouncedSearch();
    if (searchTerm === "" || sort === "") setBlogList(blogs);
    return () => debouncedSearch.cancel();
  }, [searchTerm, sort]);

  return (
    <BlogsListWrapper>
      <Container>
        {!noSearch && (
          <BlogSearch
            blogsList={blogsList}
            searchTerm={searchTerm}
            onSearchChange={handleChange}
            textSearch={textSearch}
            popularitySearch={popularitySearch}
            sortBy={sort}
            onSortChange={handleSortChange}
            popularityOptions={[
              { label: "Newest", value: "newest" },
              { label: "Oldest", value: "oldest" },
            ]}
          />
        )}
        <ListWrapper>
          {blogsList?.map((blog) => (
            <BlogItem key={`Blog--item--${blog.id}`} data={blog} />
          ))}
        </ListWrapper>
        {expandable && (
          <ShowMoreBtnWrapper>
            {!isBlogEnd && (
              <ShowMoreBtn as={"button"} onClick={loadMoreBlogs}>
                Show more
              </ShowMoreBtn>
            )}
          </ShowMoreBtnWrapper>
        )}
      </Container>
    </BlogsListWrapper>
  );
};

export default BlogList;
