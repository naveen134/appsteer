import styled from "styled-components";

export const BlogsListWrapper = styled.div`
  background: #f5f5f5;
`;

export const ListWrapper = styled.div`
  background: #f5f5f5;
  display: flex;
  flex-wrap: wrap;
  gap: 70px;
`;

export const ShowMoreBtnWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 80px;
`;

export const ShowMoreBtn = styled.button`
  background: #ffb47b;
  border-radius: 30px;
  padding: 14px 36px;
  margin: 100px 0px;
  font-weight: 500;
  font-size: 20px;
  line-height: 22px;
  text-align: center;
  color: #191919;
  border: none;
  outline: none;
`;
