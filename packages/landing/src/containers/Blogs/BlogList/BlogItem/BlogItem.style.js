import styled from "styled-components";

export const BlogItemWrapper = styled.a`
  background: teal;
  width: 352px;
  background: #ffffff;
  border-radius: 40px;
  overflow: hidden;
  cursor: pointer;
`;

export const BlogImage = styled.div`
  width: 100%;
  max-height: 298px;
  min-height: 298px;
  position: relative;
  background-image: url(${(props) => props.bgImg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const BlogPill = styled.div`
  position: absolute;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #191919;
  padding: 10px 15px;
  background: #e0e0e0;
  border-radius: 20px;
  top: 50px;
  left: 30px;
  text-transform: capitalize;
  border: none;
  outline: none;
`;

export const BlogDetails = styled.div`
  padding: 24px;
`;

export const BlogHeading = styled.h2`
  font-weight: 500;
  font-size: 18px;
  line-height: 28px;
  color: #191919;
  margin-bottom: 16px;
  width: 100%;
  margin-top: 0;
  margin-bottom: 16px;
  min-height: 56px;
`;

export const BlogAuthorDetails = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const BlogAuthor = styled.p`
  text-align: left;
  flex: 1;
  text-overflow: ellipse;
  overflow: hidden;
  white-space: pre;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  color: #666666;
`;

export const BlogTime = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex: 1;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  text-align: right;
  letter-spacing: -0.02em;

  color: #666666;
`;

export const BlogTimeText = styled.span`
  padding-left: 5px;
  padding-right: 3px;
`;
