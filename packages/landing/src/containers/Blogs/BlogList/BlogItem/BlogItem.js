import Image from "next/image";
import {
  BlogDetails,
  BlogImage,
  BlogItemWrapper,
  BlogPill,
  BlogAuthor,
  BlogAuthorDetails,
  BlogHeading,
  BlogTime,
  BlogTimeText,
} from "./BlogItem.style";
import Link from "next/link";
import { useRouter } from "next/router";

import clock from "common/assets/image/blogs/clock.png";
import { calculateReadingTime, stringShortner } from "helpers";

const BlogItem = ({ data }) => {
  const router = useRouter();
  const handleClick = (e) => {
    e?.preventDefault();
    // console.log({ data });
    router.push(
      `/blogs/${data?.attributes?.blog_type?.data?.attributes?.slug}`
    );
  };
  return (
    <Link
      href={`/blogs/${data?.attributes?.blog_type?.data?.attributes?.slug}/${data?.attributes?.slug}`}
    >
      <BlogItemWrapper>
        <BlogImage
          bgImg={
            data?.attributes?.blogBanner?.data?.attributes?.formats?.medium?.url
          }
        >
          <BlogPill as="button" onClick={handleClick}>
            {data?.attributes?.blog_type?.data?.attributes?.type}
          </BlogPill>
        </BlogImage>
        <BlogDetails>
          <BlogHeading>
            {stringShortner(data?.attributes?.title, 50)}
          </BlogHeading>
          <BlogAuthorDetails>
            <BlogAuthor>
              {data?.attributes?.blog_author?.data?.attributes?.name}
            </BlogAuthor>
            <BlogTime>
              <Image src={clock} />
              <BlogTimeText>
                {calculateReadingTime(
                  data?.attributes?.content
                    ?.filter((i) => i?.__component === "blog.blog-text-content")
                    ?.map((k) => k?.BlogTextContent)
                    ?.join(" ")
                )}
              </BlogTimeText>
              min
            </BlogTime>
          </BlogAuthorDetails>
        </BlogDetails>
      </BlogItemWrapper>
    </Link>
  );
};

export default BlogItem;
