import styled from "styled-components";

export const BlogSearchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding-top: 100px;
  padding-bottom: 60px;
  @media (max-width: 480px) {
    padding-top: 50px;
    padding-bottom: 20px;
    flex-direction: column;
  }
`;

export const BlogSearchInputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 40%;
  background: #ffffff;
  border-radius: 10px;
  padding: 20px 25px;
  position: relative;
  @media (max-width: 480px) {
    margin-bottom: 15px;
    width: 100%;
    padding: 12px 18px;
  }
`;

export const BlogSearchInput = styled.input`
  width: 100%;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  color: #191919;
  border: none;
  outline: none;
  &:placeholder {
    color: #666666;
    font-weight: 400;
  }
  @media (max-width: 480px) {
    font-size: 16px;
  }
`;

export const BlogSearchDropdownWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 30%;
  background: #ffffff;
  border-radius: 10px;
  border: none;
  outline: none;
  position: relative;
  .dropdown-icon {
    position: absolute;
    right: 10px;
  }
  @media (max-width: 480px) {
    margin-bottom: 15px;
    width: 100%;
  }
`;

export const BlogSearchDropdownBtn = styled.button`
  margin: 0;
  width: 100%;
  text-align: left;
  cursor: pointer;
  padding: 20px 25px;
  background: #fff;
  border: none;
  outline: none;
  border-radius: 10px;
  text-transform: capitalize;
  @media (max-width: 480px) {
    font-size: 16px;
    padding: 13px 19px;
  }
`;

export const BlogSearchDropdownList = styled.div`
  position: absolute;
  width: 100%;
  background: #fff;
  top: 100%;
  z-index: 999;
  border-radius: 10px;
`;

export const BlogSearchDropdownListItem = styled.button`
  display: block;
  text-align: left;
  cursor: pointer;
  padding: 20px 25px;
  width: 100%;
  background: #fff;
  border: none;
  outline: none;
  border-radius: 10px;
  transition: 0.5s all ease;
  &:hover {
    background: rgba(0, 0, 0, 0.02);
  }
  @media (max-width: 480px) {
    font-size: 16px;
    padding: 13px 19px;
  }
`;

export const SearchListWrapper = styled.div`
  width: 100%;
  position: absolute;
  top: 100%;
  margin-left: -25px;
  z-index: 999;
  background: white;
  padding: -8px !important;
  padding-left: 20px;
  border-radius: 10px;
  @media (max-width: 481px) {
    margin-left: -17px;
  }
  .content-search-list {
    font-weight: 400;
    font-size: 18px;
    line-height: 21px;
    color: #191919;
    padding: 12px 12px;
    a {
      color: black;
    }

    &:hover {
      background: rgba(0, 0, 0, 0.02);
    }
  }
`;
