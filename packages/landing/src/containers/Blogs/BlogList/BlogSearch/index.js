import {
  BlogSearchContainer,
  BlogSearchInputWrapper,
  BlogSearchDropdownWrapper,
  BlogSearchInput,
  BlogSearchDropdownBtn,
  BlogSearchDropdownList,
  BlogSearchDropdownListItem,
  SearchListWrapper,
} from "./BlogSearch.style";
import { ic_search_outline } from "react-icons-kit/md/ic_search_outline";
import { ic_keyboard_arrow_down_twotone } from "react-icons-kit/md/ic_keyboard_arrow_down_twotone";
import { ic_close } from "react-icons-kit/md/ic_close";
import { Icon } from "react-icons-kit";
import { useEffect, useRef, useState } from "react";
import Button from "common/components/Button";
import Link from "next/link";

const BlogSearch = ({
  textSearch,
  popularitySearch,
  searchTerm,
  onSearchChange,
  popularityOptions,
  onSortChange,
  sortBy,
  blogsList,
}) => {
  const dropdownRef = useRef(null);
  const searchdropdownRef = useRef(null);
  const searchList = {
    id: 1,
    content: blogsList?.map((data, index) => ({
      id: index + 1,
      text: data?.attributes.title,
      link: `/blogs/${data?.attributes?.blog_type?.data?.attributes?.slug}/${data?.attributes?.slug}`,
    })),
  };
  const [openDropdown, setOpenDropdown] = useState(false);

  const [openSearchDropdown, setOpenSearchDropdown] = useState(false);

  const handleDropdownClick = (val) => {
    onSortChange(val);
    setOpenDropdown(false);
  };
  const handleDropdownClickOutside = (e) => {
    if (!dropdownRef?.current?.contains(e.target)) {
      setOpenDropdown(false);
      // setOpenSearchDropdown(true);
    }
    if (!searchdropdownRef?.current?.contains(e.target)) {
      // setOpenDropdown(false);
      setOpenSearchDropdown(false);
    }
    // setOpenSearchDropdown(false);
  };
  const handleSearchDropdown = () => {
    setOpenSearchDropdown(true);
  };
  useEffect(() => {
    document.addEventListener("mousedown", handleDropdownClickOutside);
    return () =>
      document.removeEventListener("mousedown", handleDropdownClickOutside);
  }, []);

  return (
    <BlogSearchContainer>
      {textSearch && (
        <BlogSearchInputWrapper onClick={() => setOpenSearchDropdown(true)}>
          <Icon style={{ marginRight: "10px" }} icon={ic_search_outline} />
          <BlogSearchInput
            type="text"
            placeholder="Search By Title"
            value={searchTerm}
            onChange={(e) => onSearchChange(e.target.value)}
          />
          <Icon
            icon={ic_keyboard_arrow_down_twotone}
            // onClick={() => setOpenSearchDropdown(true)}
          />
          {openSearchDropdown && (
            <SearchListWrapper ref={searchdropdownRef}>
              {searchList?.content?.map((data, index) => {
                return (
                  <div className="content-search-list" key={data.id}>
                    <Link href={data?.link}>{data?.text}</Link>
                  </div>
                );
              })}
            </SearchListWrapper>
          )}
        </BlogSearchInputWrapper>
      )}
      {popularitySearch && (
        <BlogSearchDropdownWrapper ref={dropdownRef}>
          <BlogSearchDropdownBtn onClick={() => setOpenDropdown(true)}>
            {sortBy || "Popularity"}
          </BlogSearchDropdownBtn>
          {/* <button> */}
          <Icon
            className="dropdown-icon"
            icon={sortBy ? ic_close : ic_keyboard_arrow_down_twotone}
            onClick={
              sortBy ? () => onSortChange("") : () => setOpenDropdown(true)
            }
          />
          {/* </button> */}

          {openDropdown && popularityOptions && (
            <BlogSearchDropdownList>
              {popularityOptions?.map((option) => (
                <BlogSearchDropdownListItem
                  key={option?.value}
                  onClick={() => handleDropdownClick(option?.value)}
                >
                  {option?.label}
                </BlogSearchDropdownListItem>
              ))}
            </BlogSearchDropdownList>
          )}
        </BlogSearchDropdownWrapper>
      )}
    </BlogSearchContainer>
  );
};
export default BlogSearch;
