import Container from "common/components/UI/Container";
import React from "react";
import Breadcrumb from "../Breadcrumb/Breadcrumb";
import BlogDetailsBanner from "./BlogDetailsBanner/BlogDetailsBanner";
import {
  BlogBanner,
  BlogImage,
  BlogShareBtn,
  BlogShareContainer,
  BlogText,
  BlogTextImageContainer,
  Section,
} from "./BlogDetailsScreen.style";
import blogBanner from "common/assets/image/blogs/blogBanner.png";
import blogDetailsImage from "common/assets/image/blogs/blogDetailsImage.png";
import linkedInImg from "common/assets/image/blogs/linkedin.png";
import twitterImg from "common/assets/image/blogs/twitter.png";
import emailImg from "common/assets/image/blogs/email.png";
import Image from "next/image";
import BlogExplore from "./BlogExplore/BlogExplore";
import {
  TwitterIcon,
  LinkedinIcon,
  LinkedinShareButton,
  EmailShareButton,
  EmailIcon,
  TwitterShareButton,
} from "next-share";
import Share from "next-share";

const BlogDetailsScreen = ({ blogData, blogTypeData, relatedBlogs }) => {
  return (
    <Section>
      <Container>
        <Breadcrumb
          crumbs={[
            // {
            //   name: blogTypeData?.attributes?.type,
            //   slug: blogTypeData?.attributes?.slug,
            // },
            {
              name: blogData?.[0]?.attributes?.title,
              slug: `/${blogTypeData?.attributes?.slug}/${blogData?.[0]?.attributes?.slug}`,
            },
          ]}
        />
        <BlogDetailsBanner blogData={blogData} />
        <BlogBanner
          bgImg={
            blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.formats
              ?.large?.url ||
            blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.formats
              ?.medium?.url ||
            blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.formats
              ?.small?.url
          }
        />
        <BlogTextImageContainer>
          {blogData?.[0]?.attributes?.content?.map((item) => {
            if (item?.__component === "blog.blog-text-content") {
              return (
                <BlogText
                  key={item?.id}
                  style={{ whiteSpace: "pre-wrap" }}
                  dangerouslySetInnerHTML={{ __html: item?.BlogTextContent }}
                />
              );
            }
            return (
              <BlogImage
                key={item?.id}
                bgImg={
                  item?.blogMediaContent?.data?.attributes?.formats?.medium?.url
                }
              />
            );
          })}
        </BlogTextImageContainer>
        <BlogShareContainer>
          <BlogShareBtn>
            <Image src={linkedInImg} />
          </BlogShareBtn>
          <BlogShareBtn>
            <Image src={twitterImg} />
          </BlogShareBtn>
          <BlogShareBtn>
            <Image src={emailImg} />
          </BlogShareBtn>
        </BlogShareContainer>
        {/* <BlogShareContainer>
          <BlogShareBtn>
            <TwitterShareButton
              url={`https://www.appsteer.io/blogs/${blogTypeData?.attributes?.slug}/${blogData?.[0]?.attributes?.slug}`}
              title={blogData?.[0]?.attributes?.title}
              imageUrl={
                blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.url
              }
            >
              <BlogShareBtn>
                <Image src={twitterImg} />
              </BlogShareBtn>
            </TwitterShareButton>
          </BlogShareBtn>
          <BlogShareBtn>
            <LinkedinShareButton
              url={`https://www.appsteer.io/blogs/${blogTypeData?.attributes?.slug}/${blogData?.[0]?.attributes?.slug}`}
              title={blogData?.[0]?.attributes?.title}
              imageUrl={
                blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.url
              }
            >
              <BlogShareBtn>
                <Image src={linkedInImg} />
              </BlogShareBtn>
            </LinkedinShareButton>
          </BlogShareBtn>
          <BlogShareBtn>
            <EmailShareButton
              url={`https://www.appsteer.io/blogs/${blogTypeData?.attributes?.slug}/${blogData?.[0]?.attributes?.slug}`}
              title={blogData?.[0]?.attributes?.title}
              imageUrl={
                blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.url
              }
            >
              <BlogShareBtn>
                <Image src={emailImg} />
              </BlogShareBtn>
             <EmailIcon
                size={32}
                iconFillColor="#fff"
                bgStyle={{ fill: "black" }}
                borderRadius={15}
              />
            </EmailShareButton>
          </BlogShareBtn>
        <BlogShareBtn>
            <Image src={linkedInImg} />
          </BlogShareBtn>
          <BlogShareBtn>
            <Image src={twitterImg} />
          </BlogShareBtn>
          <BlogShareBtn>
            <Image src={emailImg} />
          </BlogShareBtn> 
        </BlogShareContainer> */}
      </Container>
      <BlogExplore relatedBlogs={relatedBlogs} />
    </Section>
  );
};

export default BlogDetailsScreen;
