import styled from "styled-components";

export const Section = styled.section`
  padding: 6rem 0 0rem;
  overflow: hidden;
  position: inherit;
  background: #ffffff;
  @media (max-width: 1600px) {
    padding: 10rem 0 3rem;767px
  }
  @media (max-width: 1440px) {
    padding: 7rem 0 0rem;
  }
  @media (max-width: 768px) {
    padding: 5rem 0 0rem;
  }
  @media (max-width: 767px) {
    padding: 5rem 0 0;
  }
`;

export const BlogBanner = styled.div`
  width: 100%;
  height: 408px;
  background-image: url(${(props) => props.bgImg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 40px;
  overflow: hidden;
  margin-bottom: 60px;
  @media (max-width: 468px) {
    height: 208px;
    border-radius: 20px;
  }
`;

export const BlogTextImageContainer = styled.div`
  padding: 0rem 5rem;
  @media (max-width: 468px) {
    padding: 0rem 1rem;
  }
`;

export const BlogText = styled.div`
  font-weight: 400;
  font-size: 18px;
  line-height: 32px;
  color: #666666;
`;

export const BlogImage = styled.div`
  width: 100%;
  height: 364px;
  background-image: url(${(props) => props.bgImg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 40px;
  overflow: hidden;
  margin-bottom: 60px;
  margin-top: 60px;
  @media (max-width: 468px) {
    height: 164px;
    border-radius: 15px;
  }
`;

export const BlogShareContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 42px;
  padding: 5rem 0px;
`;

export const BlogShareBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background: #f5f5f5;
  border-radius: 50%;
  border: none;
  outline: none;
  padding: 12px;
  cursor: pointer;
  @media (max-width: 468px) {
    transform: scale(0.7);
  }
`;
