import styled from "styled-components";

export const BlogExploreContainer = styled.div`
  background: #f5f5f5;
  padding: 7rem 0px;
  @media (max-width: 468px) {
    padding: 3rem 0px;
  }
`;

export const BlogExploreHeading = styled.h2`
  margin-top: 0px;
  margin-bottom: 54px;
  font-weight: 600;
  font-size: 62px;
  line-height: 74px;
  color: #191919;
  @media (max-width: 468px) {
    font-size: 32px;
    margin-bottom: 24px;
  }
`;
