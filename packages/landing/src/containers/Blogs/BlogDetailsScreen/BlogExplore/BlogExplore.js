import Container from "common/components/UI/Container";
import BlogList from "containers/Blogs/BlogList";
import React from "react";
import { BlogExploreContainer, BlogExploreHeading } from "./BlogExplore.style";

const BlogExplore = ({ relatedBlogs }) => {
  return (
    <BlogExploreContainer>
      <Container>
        <BlogExploreHeading>Explore more</BlogExploreHeading>
        <BlogList blogs={relatedBlogs} noSearch expandable={false} />
      </Container>
    </BlogExploreContainer>
  );
};

export default BlogExplore;
