import styled from "styled-components";

export const BannerContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const BlogTypeChip = styled.p`
  background: #f5f5f5;
  border-radius: 30px;
  padding: 10px 16px;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  text-align: center;
  color: #000000;
  margin-top: 26px;
  margin-bottom: 14px;
  text-transform: capitalize;
`;

export const BlogTitle = styled.h1`
  margin-top: 0px;
  margin-bottom: 32px;
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  text-align: center;
  color: #191919;
  max-width: 628px;
  @media (max-width: 468px) {
    font-size: 28px;
    line-height: 38px;
  }
`;

export const BlogAuthorContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 38px;
`;

export const BlogAuthorAvatar = styled.div`
  border-radius: 50%;
  width: 64px;
  height: 64px;
  background: teal;
  background-image: url(${(props) => props.bgImg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  margin-right: 18px;
  @media (max-width: 468px) {
    height: 40px;
    width: 40px;
  }
`;

export const BlogAuthorDetails = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
`;

export const BlogAuthorName = styled.p`
  margin-top: 0px;
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  color: #191919;
  @media (max-width: 468px) {
    font-size: 14px;
    line-height: 18px;
    margin-bottom: 5px;
  }
`;

export const BlogAuthorDesignation = styled.p`
  margin-top: 0px;
  margin-bottom: 0px;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  color: #666666;
  @media (max-width: 468px) {
    font-size: 14px;
    line-height: 18px;
  }
`;

export const BlogPublishDate = styled.p`
  margin-top: 0px;
  margin-bottom: 32px;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  text-align: center;
  color: #666666;
`;
