import React from "react";
import {
  BannerContainer,
  BlogAuthorAvatar,
  BlogAuthorContainer,
  BlogAuthorDesignation,
  BlogAuthorDetails,
  BlogAuthorName,
  BlogPublishDate,
  BlogTitle,
  BlogTypeChip,
} from "./BlogDetailsBanner.style";
import moment from "moment";

const BlogDetailsBanner = ({ blogData }) => {
  return (
    <BannerContainer>
      <BlogTypeChip>
        {blogData?.[0]?.attributes?.blog_type?.data?.attributes?.type}
      </BlogTypeChip>
      <BlogTitle>{blogData?.[0]?.attributes?.title}</BlogTitle>
      <BlogAuthorContainer>
        <BlogAuthorAvatar
          bgImg={
            blogData?.[0]?.attributes?.blog_author?.data?.attributes?.avatar
              ?.data?.[0]?.attributes?.url
          }
        ></BlogAuthorAvatar>
        <BlogAuthorDetails>
          <BlogAuthorName>
            {blogData?.[0]?.attributes?.blog_author?.data?.attributes?.name}
          </BlogAuthorName>
          <BlogAuthorDesignation>
            {
              blogData?.[0]?.attributes?.blog_author?.data?.attributes
                ?.designtion
            }
          </BlogAuthorDesignation>
        </BlogAuthorDetails>
      </BlogAuthorContainer>
      <BlogPublishDate>
        {moment(blogData?.[0]?.attributes?.publishDate).format("DD MMMM, YYYY")}
      </BlogPublishDate>
    </BannerContainer>
  );
};

export default BlogDetailsBanner;
