import React from "react";
import {
  Section,
  BannerSlideContainer,
  BannerSlideTextContainer,
  BannerSlideHeading,
  BannerSlideSubHeading,
  BannerSlideBtn,
  BannerImageContainer,
} from "./BlogsBanner.style";
import Container from "common/components/UI/Container";
import Fade from "react-reveal/Fade";
import GlideCarousel from "common/components/GlideCarousel";
import GlideSlide from "common/components/GlideCarousel/glideSlide";
import { useRouter } from "next/router";

const BlogsBanner = ({ blogs }) => {
  const router = useRouter();
  const glideOptions = {
    type: "carousel",
    gap: 0,
    autoplay: 5000,
    perView: 1,
    animationDuration: 700,
  };

  const handleClick = (item) => {
    // console.log({ item });
    router.push(
      `/blogs/${item?.attributes?.blog_type?.data?.attributes?.slug}/${item?.attributes?.slug}`
    );
  };
  return (
    <Section>
      <Container className="blog-container">
        <Fade up delay={100}>
          <GlideCarousel
            className="blog-carousel"
            options={glideOptions}
            prevButton={null}
            nextButton={null}
          >
            {blogs?.map((item, index) => (
              <GlideSlide key={`blogs--key${item.id}`}>
                <BannerSlideContainer>
                  <BannerImageContainer
                    bgImg={
                      item?.attributes?.blogBanner?.data?.attributes?.formats
                        ?.large?.url ||
                      item?.attributes?.blogBanner?.data?.attributes?.formats
                        ?.medium?.url ||
                      item?.attributes?.blogBanner?.data?.attributes?.formats
                        ?.small?.url
                    }
                  >
                    {/* <Image
                      src={item.slideImage}
                      layout="fill"
                      alt="Slide Image"
                      className="banner-image"
                    /> */}
                  </BannerImageContainer>
                  <BannerSlideTextContainer>
                    <BannerSlideHeading>
                      {item?.attributes?.title}
                    </BannerSlideHeading>
                    <BannerSlideSubHeading
                      dangerouslySetInnerHTML={{
                        __html:
                          item?.attributes?.content?.[0]?.BlogTextContent?.substr(
                            0,
                            140
                          ),
                      }}
                    >
                      {item?.subHeading}
                    </BannerSlideSubHeading>
                    <BannerSlideBtn onClick={() => handleClick(item)}>
                      Read more
                    </BannerSlideBtn>
                  </BannerSlideTextContainer>
                </BannerSlideContainer>
              </GlideSlide>
            ))}
          </GlideCarousel>
        </Fade>
      </Container>
    </Section>
  );
};

export default BlogsBanner;
