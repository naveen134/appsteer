import styled from "styled-components";

export const Section = styled.section`
  padding: 5rem 0 3rem;
  overflow: hidden;
  position: inherit;

  .blog-container {
    margin-top: 50px;
    .blog-carousel {
      .glide__controls {
        display: none;
      }
    }
  }
  @media (max-width: 1600px) {
    padding: 10rem 0 3rem;767px
  }
  @media (max-width: 1440px) {
    padding: 7rem 0 3rem;
  }
  @media (max-width: 768px) {
    padding: 5rem 0 3rem;
  }
  @media (max-width: 767px) {
    padding: 5rem 0 0;
  }
`;

export const BannerSlideContainer = styled.div`
  position: relative;
  min-height: 544px;
  display: flex;
  align-items: center;
  padding-left: 40px;
  @media (max-width: 1440px) {
    min-height: 400px;
  }
  @media (max-width: 767px) {
    min-height: 250px;
    padding-left: 0px;
    align-items: flex-end;
    padding-bottom: 20px;
    justify-content: center;
  }
`;
export const BannerImageContainer = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  z-index: 5;
  background-image: url(${(props) => props.bgImg});
  height: 100%;
  width: 80%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 40px;
  @media (max-width: 1440px) {
    width: 60%;
  }
  @media (max-width: 767px) {
    width: 100%;
    border-radius: 20px;
  }
`;
export const BannerSlideTextContainer = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
  z-index: 10;
  position: relative;
  width: 60%;
  padding: 70px 50px;
  @media (max-width: 1600px) {
  }
  @media (max-width: 1440px) {
    padding: 40px 30px;
  }
  @media (max-width: 768px) {
  }
  @media (max-width: 767px) {
    width: 90%;
    padding: 15px 25px;
    border-radius: 20px;
  }
`;
export const BannerSlideHeading = styled.h2`
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  color: #191919;
  margin-bottom: 24px;
  margin-top: 0px;
  @media (max-width: 1600px) {
  }
  @media (max-width: 1440px) {
    font-size: 38px;
    line-height: 46px;
  }
  @media (max-width: 768px) {
  }
  @media (max-width: 767px) {
    font-size: 18px;
    line-height: 18px;
    margin-bottom: 12px;
  }
`;
export const BannerSlideSubHeading = styled.h4`
  font-weight: 400;
  font-size: 18px;
  line-height: 36px;
  color: #191919;
  margin-bottom: 30px;
  @media (max-width: 1440px) {
    font-size: 14px;
    line-height: 32px;
  }
  @media (max-width: 767px) {
    font-size: 10px;
    line-height: 16px;
    margin-bottom: 16px;
  }
`;
export const BannerSlideBtn = styled.button`
  background: #ffb47b;
  border-radius: 30px;
  padding: 14px 36px;
  font-weight: 500;
  font-size: 20px;
  line-height: 23px;
  color: #191919;
  border: none;
  outline: none;
  @media (max-width: 1440px) {
    padding: 10px 20px;
    font-size: 16px;
    line-height: 18px;
  }
  @media (max-width: 767px) {
    border-radius: 30px;
    padding: 8px 24px;
    font-size: 12px;
    line-height: 16px;
  }
`;
