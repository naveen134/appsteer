import React from "react";
import Fade from "react-reveal/Fade";
import Text from "common/components/Text";
import { Icon } from "react-icons-kit";
import { chevronRight } from "react-icons-kit/feather/chevronRight";
import Link from "next/link";
import NextImage from "common/components/NextImage";
import Image from "common/components/Image";
import Button from "common/components/Button";
import Heading from "common/components/Heading";
import Container from "common/components/UI/Container";
import BannerWrapper, {
  BannerContent,
  BannerImage,
  Breadcrums,
  Pattern,
} from "./banner.style";
import { desaturate } from "polished";
import { Description } from "containers/Appsteer/StickyFooter/stickyFooter.style";

const Banner = ({ data }) => {
  const { title, description, bannerImage, usecaseCategory, usecaseTitle } =
    data;

  return (
    <BannerWrapper id="banner_section">
      <Pattern />
      <Container>
        <BannerContent>
          <Fade up delay={50}>
            <Breadcrums>
              <Link
                href={"/industry-vertical/" + usecaseCategory?.slug}
                className="Breadcrums"
              >
                <Text className="Breadcrums" content={usecaseCategory?.name} />
              </Link>
            </Breadcrums>
          </Fade>
          <Fade up delay={100}>
            <Heading as="h1" content={title} />
          </Fade>
          <Fade up delay={200}>
            <Text content={description} />
          </Fade>
        </BannerContent>
        <BannerImage className="heroImg">
          <Fade up delay={100}>
            <Image src={bannerImage} alt="Banner" />
          </Fade>
        </BannerImage>
      </Container>
    </BannerWrapper>
  );
};

export default Banner;
