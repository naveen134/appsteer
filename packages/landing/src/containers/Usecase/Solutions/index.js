import React from "react";
import Fade from "react-reveal/Fade";
import { Icon } from "react-icons-kit";
import { check } from "react-icons-kit/feather/check";
import { ic_keyboard_arrow_right } from "react-icons-kit/md/ic_keyboard_arrow_right";
import Container from "common/components/UI/Container";
import NextImage from "common/components/NextImage";
import Button from "common/components/Button";
import Text from "common/components/Text";
import Link from "common/components/Link";
import Heading from "common/components/Heading";
import Image from "common/components/Image";

import Section, {
  Grid,
  Figure,
  Content,
  Features,
  Pattern,
} from "./solution.style";
// import parallaxBg from 'common/assets/image/webAppMinimal/parallax-1.png';
// import app from 'common/assets/image/webAppMinimal/app.png';

// import { analyticsTool } from "common/data/WebAppMinimal";

const UsecaseSolution = ({ data }) => {
  const { title, description, bannerImage } = data;
  return (
    <Section>
      <Pattern />
      <Container>
        <Grid>
          <Figure>
            <Fade up>
              <Image src={bannerImage} alt="Banner" />
            </Fade>
          </Figure>
          <Content>
            <Fade right>
              <Heading content={title} />
              <Features>
                {description.map((SolutionListItems) => (
                  <li key={SolutionListItems.id}>
                    {/* <Icon icon={check} size={22} /> */}
                    {SolutionListItems.ListText}
                  </li>
                ))}
              </Features>
            </Fade>
          </Content>
        </Grid>
      </Container>
    </Section>
  );
};

export default UsecaseSolution;
