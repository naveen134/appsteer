import styled from "styled-components";

export const FormSelectContainer = styled.span`
  // position: relative;
`;

export const FormSelect = styled.span`
  position: relative;
  background: transparent;

  border: none;
  outline: none;
  font-weight: 500;
  font-size: 24px;
  line-height: 38px;
  color: #c9c9c9;
  text-align: left;
  display: inline;
  margin: 0px 12px;
  cursor: pointer;
  @media (max-width: 768px) {
    font-size: 18px;
    line-height: 24px;
    margin: 0px 6px;
  }
`;

export const FormSelectValue = styled.span`
  border-bottom: 2px solid
    ${(props) =>
      props.isOpen ? "#FFB47B" : props.hasValue ? "transparent" : "#c9c9c9"};
  color: ${(props) => (props.hasValue ? "#FFB47B" : "#c9c9c9")};
`;

export const FormSelectIcon = styled.span`
  padding-left: 10px;
  border-bottom: 2px solid
    ${(props) =>
      props.isOpen ? "#FFB47B" : props.hasValue ? "transparent" : "#c9c9c9"};
  & .select-icon {
    transition: 0.5s all ease;
    transform: rotate(${(props) => (props.isOpen ? "180deg" : "0deg")});
    svg {
      fill: ${(props) =>
        props.isOpen || props.hasValue ? "#FFB47B" : "currentColor"};
      height: 30px;
      width: 30px;
      @media (max-width: 768px) {
        height: 20px;
        width: 20px;
      }
    }
  }
  @media (max-width: 768px) {
    padding-left: 5px;
  }
`;

export const FormSelectMenu = styled.span`
  position: absolute;
  padding: 30px 40px;
  background: #ffffff;
  box-shadow: 0px 0px 22px rgba(0, 0, 0, 0.12);
  border-radius: 20px;
  // position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 70vw;
  max-width: calc(100vw - 100px);
  z-index: 999;
  @media (max-width: 768px) {
    width: 80vw;
    padding: 20px;
    position: absolute;
    left: 50%;
    top: 35%;
    transform: translate(-50%, -30%);
  }
  @media (min-width: 1440px) {
    width: 50vw;
  }

  @media (min-width: 1920px) {
    width: 720px;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    
    //  // left: ${(props) =>
      (props.left || 0) > 100 ? 100 - (props.left || 0) : props.left || 0}%;
    // transform: translateX(
    //   -${(props) =>
      (props.left || 0) > 100 ? 100 - (props.left || 0) : props.left || 0}%
    // );
  
  // }
`;
export const popupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
`;
export const MenuHeading = styled.span`
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #666666;
  display: block;
  margin-bottom: 24px;
  @media (max-width: 768px) {
    font-size: 14px;
  }
`;

export const MenuList = styled.span`
  display: flex;
  flex-wrap: wrap;
  border-bottom: 2px solid
    ${(props) => (!props.showBorder ? "#f5f5f5" : "transparent")};
`;

export const MenuListItem = styled.button`
  background: ${(props) => (props.isSelected ? "#ffffff" : "#f5f5f5")};
  border-radius: 10px;
  padding: 10px 15px;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #191919;
  margin: 0px 14px 18px 0px;
  outline: none;
  border: 2px solid ${(props) => (props.isSelected ? "#ffb47b" : "#f5f5f5")};
  cursor: pointer;
  text-align: left;
  text-transform: capitalize;

  &:disabled {
    cursor: not-allowed;
  }
  & .selected-item-icon {
    margin-right: 10px;
    pointer-events: none;
    svg {
      pointer-events: none;
      fill #FFB47B;
      height: 20px;
      width: 20px;
      @media (max-width: 768px) {
        height: 15px;
        width: 15px;
      }
    }
    @media (max-width: 768px) {
      margin-right: 6px;
    }
  }

  @media (max-width: 768px) {
    padding: 5px 10px;
    font-size: 14px;
  }
`;
export const ButtonWrapper = styled.span`
  position: relative;
`;

export const CloseButton = styled.button`
  // background: ${(props) => (props.isSelected ? "#ffffff" : "#f5f5f5")};
  background: tranparent;
  position: absolute;
  top: -30%;
  right: -25%;
  background-color: transparent;
  border-radius: 10px;
  padding: 10px 10px;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #191919;
  margin: 0px 14px 18px 0px;
  outline: none;
  border: none;
  // border: 2px solid ${(props) => (props.isSelected ? "#ffb47b" : "#f5f5f5")};
  cursor: pointer;
  text-align: left;
  text-transform: capitalize;
`;

export const FormButtonContainer = styled.span`
  display: block;
  margin-top: 30px;
`;

export const FormButton = styled.span`
  background: ${(props) => (props.noBackground ? "transparent" : "#ffb47b")};
  border-radius: 40px;
  padding: 10px 24px;
  font-weight: 500;
  font-size: 18px;
  line-height: 21px;
  text-align: center;
  color: ${(props) => (props.noBackground ? "#666666" : "#191919")};
  cursor: pointer;
  border: none;
  outline: none;
  @media (max-width: 768px) {
    padding: 10px 14px;
    font-size: 14px;
  }
`;

export const FormInputContainer = styled.span`
  display: block;
  width: 100%;
  padding-bottom: 18px;
  border-bottom: 2px solid #f5f5f5;
`;

export const FormInput = styled.input`
  background: #ffffff;
  border: 2px solid #f5f5f5;
  border-radius: 10px;
  padding: 20px 30px;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: #191919;
  display: block;
  width: 100%;

  &::placeholder {
    color: #666666;
    font-weight: 400;
  }

  @media (max-width: 768px) {
    padding: 10px 15px;
    font-size: 12px;
  }
`;
