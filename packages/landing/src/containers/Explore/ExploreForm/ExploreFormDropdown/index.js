import { concatArrToStr, getPercentage } from "helpers";
import { useEffect, useRef, useState } from "react";
import { Icon } from "react-icons-kit";
import { ic_keyboard_arrow_down_twotone } from "react-icons-kit/md/ic_keyboard_arrow_down_twotone";
import { ic_done_twotone } from "react-icons-kit/md/ic_done_twotone";
import {
  FormSelectContainer,
  FormSelect,
  FormSelectIcon,
  FormSelectMenu,
  MenuHeading,
  MenuList,
  MenuListItem,
  FormSelectValue,
  FormButtonContainer,
  FormButton,
  FormInput,
  FormInputContainer,
  CloseButton,
  ButtonWrapper,
  popupOverlay,
} from "./exploreFormDropdown.style";
import CloseButtonImg from "../../../../common/assets/image/explore/CloseButton.png";

const ExploreFormDropdown = ({
  value,
  dropdownHeading,
  options,
  isMulti,
  onChange,
  disableOnSelect,
  onInputChange,
  inputValue,
}) => {
  const dropdownTextRef = useRef(null);
  const dropdownRef = useRef(null);
  const [openDropdown, setOpenDropdown] = useState(false);
  const [staticBackground, setStaticBackground] = useState(false);
  const [percent, setPercent] = useState(0);

  const addOverflow = () => {
    document.body.style.overflow = "hidden";
  };
  const removeOverflow = () => {
    document.body.style = "";
  };

  // useEffect(() => {
  //   console.log("openDropdown", openDropdown);
  //   if (openDropdown) document.body.style.overflow = "hidden";
  //   else document.body.style.overflow = "visible";
  // }, [openDropdown]);

  // const [othersValue, setOthersValue] = useState({ value });

  // Determine the position of dropdown based on element
  const handleOpenDropdown = (e) => {
    // const parent = dropdownRef.current?.offsetParent?.offsetWidth;
    const position = dropdownRef.current?.offsetLeft;
    setPercent(getPercentage(position));
    setOpenDropdown(true);
  };

  // Event Listener for special option click like "Others"
  const handleSpecialKeywordSelect = (keyword) => {
    if (value?.includes(keyword)) {
      onChange(isMulti ? [] : "");
      return;
    }
    onChange(isMulti ? [keyword] : keyword);
  };

  // Event Listener for each dropdown item click
  const handleClickItem = (e) => {
    e?.stopPropagation();
    const selectedValue = e?.target?.value;

    if (selectedValue === disableOnSelect) {
      // CHANGING THE INPUT VALUE TO BLANK
      onInputChange("");
      handleSpecialKeywordSelect(selectedValue);
      return;
    }

    if (isMulti) {
      if (value?.includes(selectedValue)) {
        onChange(value?.filter((i) => i !== selectedValue));
      } else {
        onChange([...value, selectedValue]);
      }
    }

    if (!isMulti) {
      // setOpenDropdown(false);
      if (value === selectedValue) {
        onChange("");
      } else {
        onChange(selectedValue);
        // setOpenDropdown(false);
      }
    }
  };

  // Event Listner for closing dropdown on outside click
  const handleClickOutside = (e) => {
    if (!dropdownRef?.current?.contains(e.target)) {
      setOpenDropdown(false);
    }
  };

  // Binding Event Listner for closing dropdown on outside click
  useEffect(() => {
    if (typeof window !== "undefined") {
      document.addEventListener("click", handleClickOutside);
    }
    return () =>
      typeof window !== "undefined" &&
      document.removeEventListener("click", handleClickOutside);
  }, []);

  return (
    <FormSelectContainer ref={dropdownRef}>
      <FormSelect
        ref={dropdownTextRef}
        isOpen={openDropdown}
        role="button"
        onClick={handleOpenDropdown}
      >
        <FormSelectValue hasValue={value?.length > 0} isOpen={openDropdown}>
          {concatArrToStr(
            options
              ?.filter((i) => value?.includes(i?.value))
              ?.map((i) => i?.label)
          ) || "Select from the list"}
        </FormSelectValue>
        <FormSelectIcon hasValue={value?.length > 0} isOpen={openDropdown}>
          <Icon className="select-icon" icon={ic_keyboard_arrow_down_twotone} />
        </FormSelectIcon>
      </FormSelect>

      {openDropdown && (
        <FormSelectMenu>
          {/* left={percent} */}
          <MenuHeading>{dropdownHeading}</MenuHeading>
          <MenuList showBorder={value?.includes(disableOnSelect)}>
            {options?.map((item) => (
              <ButtonWrapper>
                <MenuListItem
                  key={item?.value}
                  value={item?.value}
                  onClick={handleClickItem}
                  isSelected={value?.includes(item?.value)}
                  disabled={
                    value?.includes(disableOnSelect) &&
                    item?.value !== disableOnSelect
                  }
                >
                  {isMulti && value?.includes(item?.value) && (
                    <Icon
                      className="selected-item-icon"
                      icon={ic_done_twotone}
                    />
                  )}
                  {item?.label}
                </MenuListItem>
                {value === "others" && item?.value === "others" && (
                  <CloseButton value={item?.value} onClick={handleClickItem}>
                    <img
                      src={CloseButtonImg?.src}
                      style={{ height: "10px" }}
                    ></img>
                  </CloseButton>
                )}
              </ButtonWrapper>
            ))}
          </MenuList>
          {value?.includes(disableOnSelect) && (
            <FormInputContainer>
              <FormInput
                type="text"
                placeholder="If others, specify here"
                onChange={(e) => onInputChange(e.target.value)}
                value={inputValue}
              />
            </FormInputContainer>
          )}

          {((value?.length > 0 && value !== "others") ||
            (!isMulti && inputValue && onInputChange)) && (
            <FormButtonContainer>
              <FormButton
                onClick={() => {
                  setOpenDropdown(false);
                }}
              >
                Okay
              </FormButton>
              <FormButton
                noBackground
                onClick={() => onChange(isMulti ? [] : "")}
              >
                Clear all
              </FormButton>
            </FormButtonContainer>
          )}
        </FormSelectMenu>
      )}
    </FormSelectContainer>
  );
};

export default ExploreFormDropdown;
