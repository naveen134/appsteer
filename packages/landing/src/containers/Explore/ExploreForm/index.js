import Container from "common/components/UI/Container";
import { useRouter } from "next/router";
import Image from "next/image";
import quoteIcon from "common/assets/image/explore/quoteIcon.png";
import {
  Form,
  FormTextWrapper,
  FormTextWithSelect,
  FormText,
  ResultButtonContainer,
} from "./exploreForm.style";
import ExploreFormDropdown from "./ExploreFormDropdown";
import { useState, Fragment } from "react";
import { generateFilterStr, isFilled, isOthersSelected } from "helpers";
import CustomNeedForm from "./CustomNeedForm";
import {
  CustomNeedFormContainer,
  CustomNeedFormHeading,
  CustomNeedFormWrapper,
} from "./CustomNeedForm/customNeedForm.style";
import { FormButton } from "./ExploreFormDropdown/exploreFormDropdown.style";

const hasRequiredData = (obj) => {
  if (!obj) return false;
  const data = { ...obj };
  if (!data?.obstacle?.includes("others")) delete data?.obstacleInput;
  if (!data?.industry?.includes("others")) delete data?.industryInput;
  if (!data?.eliminate?.includes("others")) delete data?.eliminateInput;
  if (!data?.wantToDo?.includes("others")) delete data?.wantToDoInput;
  return isFilled(data);
};

const ExploreForm = ({
  obstacles,
  industries,
  elimination,
  futureThings,
  teamSize,
}) => {
  const router = useRouter();
  const [formData, setFormData] = useState({
    obstacle: [],
    teamSize: "",
    industry: "",
    eliminate: [],
    wantToDo: [],
    obstacleInput: "",
    industryInput: "",
    eliminateInput: "",
    wantToDoInput: "",
  });
  const [openNewRegForm, setOpenNewRegForm] = useState(false);

  // onChange handler for the text dropdowns
  const handleChange = (key, value) => {
    setOpenNewRegForm(false);
    const data = formData;
    data[key] = value;

    setFormData({ ...data });
  };

  // onClick handler for "Shown Options" Button
  const handleExploreFormSubmit = () => {
    const { obstacleInput, industryInput, eliminateInput, wantToDoInput } =
      formData;
    const shouldOpen = isOthersSelected({
      obstacleInput,
      industryInput,
      eliminateInput,
      wantToDoInput,
    });
    setOpenNewRegForm(shouldOpen);

    if (!shouldOpen) {
      router.push(
        `/solutions?populate=*&filters[$and][0][industry_vertical][IndustryTitle][$contains]=${
          formData?.industry
        }&filters[$and][1][team_sizes][text][$contains]=${
          formData?.teamSize
        }&${generateFilterStr({
          obstacles: formData.obstacle,
          eliminates: formData.eliminate,
          future_things: formData?.wantToDo,
        })}`
      );
    }
    localStorage.setItem("explore-form", JSON.stringify(formData));
  };
  // onSubmit handler for Custome need form
  const handleCustomNeedFormSubmit = (value) => {
    // console.log({ customDetails: value, exploreOption: formData });
  };
  // console.log({ obstacles, elimination, industries, teamSize, futureThings });

  return (
    <Container>
      <Form>
        <Image className="explore-form-icon" src={quoteIcon} alt="" />
        <FormTextWrapper>
          <FormTextWithSelect>
            <FormText>Currently I’m struggling with</FormText>
            <ExploreFormDropdown
              isMulti
              onChange={(value) => handleChange("obstacle", value)}
              dropdownHeading="Select the obstacles to growth that is leading to missing bigger opportunities"
              options={obstacles?.map(({ attributes }) => ({
                label: attributes?.text,
                value: attributes?.text,
              }))}
              value={formData.obstacle}
              disableOnSelect="others"
              onInputChange={(value) => handleChange("obstacleInput", value)}
              inputValue={formData.obstacleInput}
            />
            {formData.obstacle.length > 0 && (
              <Fragment>
                <FormText>We are a team of about</FormText>
                <ExploreFormDropdown
                  isMulti={false}
                  onChange={(value) => handleChange("teamSize", value)}
                  dropdownHeading="How big is your team?"
                  options={teamSize?.map(({ attributes }) => ({
                    label: attributes?.text,
                    value: attributes?.text,
                  }))}
                  value={formData.teamSize}
                />
              </Fragment>
            )}
            {formData.obstacle.length > 0 && formData.teamSize && (
              <Fragment>
                <FormText>people, working in</FormText>
                <ExploreFormDropdown
                  isMulti={false}
                  onChange={(value) => handleChange("industry", value)}
                  dropdownHeading="Select your Industry"
                  options={[
                    ...industries?.map(({ linktext }) => ({
                      label: linktext,
                      value: linktext,
                    })),
                    {
                      label: "others",
                      value: "others",
                    },
                  ]}
                  value={formData.industry}
                  disableOnSelect="others"
                  onInputChange={(value) => {
                    handleChange("industryInput", value?.trim());
                  }}
                  inputValue={formData.industryInput}
                />
              </Fragment>
            )}
            {formData.obstacle.length > 0 &&
              formData.teamSize &&
              formData.industry && (
                <Fragment>
                  <FormText>I want to eliminate</FormText>
                  <ExploreFormDropdown
                    isMulti
                    onChange={(value) => handleChange("eliminate", value)}
                    dropdownHeading="What are the things you don’t want to do in Future?"
                    options={elimination?.map(({ attributes }) => ({
                      label: attributes?.text,
                      value: attributes?.text,
                    }))}
                    value={formData.eliminate}
                    disableOnSelect="others"
                    onInputChange={(value) => {
                      handleChange("eliminateInput", value);
                    }}
                    inputValue={formData.eliminateInput}
                  />
                </Fragment>
              )}
            {formData.obstacle.length > 0 &&
              formData.teamSize &&
              formData.industry &&
              formData.eliminate.length > 0 && (
                <Fragment>
                  <FormText>have</FormText>
                  <ExploreFormDropdown
                    isMulti
                    onChange={(value) => handleChange("wantToDo", value)}
                    dropdownHeading="Some of the things you want to do in the Future"
                    options={futureThings?.map(({ attributes }) => ({
                      label: attributes?.text,
                      value: attributes?.text,
                    }))}
                    value={formData.wantToDo}
                    disableOnSelect="others"
                    onInputChange={(value) => {
                      handleChange("wantToDoInput", value);
                    }}
                    inputValue={formData.wantToDoInput}
                  />
                </Fragment>
              )}
          </FormTextWithSelect>
        </FormTextWrapper>
        {openNewRegForm && (
          <CustomNeedFormContainer>
            <CustomNeedFormHeading>
              Looks like you have a custom need,
            </CustomNeedFormHeading>
            <CustomNeedFormHeading>
              please provide more details
            </CustomNeedFormHeading>
            <CustomNeedFormWrapper>
              <CustomNeedForm onSubmit={handleCustomNeedFormSubmit} />
            </CustomNeedFormWrapper>
          </CustomNeedFormContainer>
        )}
        {hasRequiredData(formData) && !openNewRegForm && (
          <ResultButtonContainer>
            <FormButton as="button" onClick={handleExploreFormSubmit}>
              Show options
            </FormButton>
          </ResultButtonContainer>
        )}
      </Form>
    </Container>
  );
};

export default ExploreForm;
