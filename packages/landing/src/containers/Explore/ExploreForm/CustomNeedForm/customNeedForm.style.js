import styled from "styled-components";

export const CustomNeedFormContainer = styled.div`
  margin-top: 70px;
`;

export const CustomNeedFormHeading = styled.h2`
  font-weight: 500;
  font-size: 24px;
  line-height: 38px;
  text-align: center;
  color: #666666;
  margin: 0;
`;

export const CustomNeedFormWrapper = styled.div`
  background: #ffffff;
  border-radius: 40px;
`;

export const FormWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width:100%;
  @media (max-width: 1280px) {
  }
  @media (max-width: 480px) {
    padding-top:-40px;
  }

  textarea 
  {
    border: none !important;
    border-bottom: 0.6px solid #bababa !important;
    min-height: 30px !important;
    resize: none;
    border-color: none;
    border-radius: 0 !important;
    padding-top: 15px !important;
    padding-bottom: 0px !important;
    color: black;
    padding-left: 0px !important;
    font-family: "Work Sans";
    font-style: normal;
    font-weight: 500;
    font-size: 18px !important;
    line-height: 21px;
    letter-spacing: -0.02em;
    color: #000000 !important;
    @media (max-width: 480px) {
      padding-top: 17px !important;
    }
    @media (max-width: 400px) {
      padding-top: 30px !important;
    }
    @media (max-width: 370px) {
      padding-top: 35px !important;
    }
  }
  .underline {
    textarea {
       overflow:hidden;
    }
  }
  > form {
     width: 80%;
    // margin-top: 40px;
    margin-bottom: 60px;
    @media (max-width: 480px) {

      width: 85%;
    }
    }
  }

  .error_input {
    .field-wrapper {
      > input {
        border-bottom: 1.6px solid #e52f2f !important;
      }
    }
    >label{
        top:-10px !important;
    }
  }
  .input_value {
    > label {
      top: -10px !important;
    }
  }
  .phone-input {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    position: relative;
    .err {
      position: absolute;
      top: 100%;
      left: 0;
      color: #e62f2f;
      font-weight: 400;
      font-size: 16px;
      line-height: 19px;
    }
    @media only screen and (max-width: 667px) {
      margin-top: 40px;
    }
  }
  .contact-phone-input {
    flex:1;
    border-bottom: 0.6px solid #bababa;
    padding-bottom: 5px !important;
    align-items: flex-end !important;
    font-weight: 500 !important;
    font-size: 18px !important;
    line-height: 21px !important;
    color: #666666 !important;
    &.ph-err{
      border-bottom: 1.6px solid #e62f2f;
    }
    .PhoneInputCountry {
      align-items: flex-end !important;
      .PhoneInputCountrySelectArrow {
        margin-bottom: 5px;
      }
    }
    // .error-phone-input{
    //   flex:1;
    //   border-bottom: 0.6px solid #bababa;
    //   padding-bottom: 5px !important;
    //   align-items: flex-end !important;
    //   font-weight: 500 !important;
    //   font-size: 18px !important;
    //   line-height: 21px !important;
    //   color: #666666 !important;
    //   .PhoneInputCountry {
    //     align-items: flex-end !important;
    //     .PhoneInputCountrySelectArrow {
    //       margin-bottom: 5px;
    //     }
    // }
    .PhoneInputInput {
      border: none;
      outline: none;
      font-weight: 500 !important;
      font-size: 18px !important;
      line-height: 21px !important;
      color: #666666 !important;
      @media screen and (max-width: 480px) {
        font-size: 16px !important;
      }
    }
  }

  .reusecore__input:focus-within {
    .field-wrapper {
      > input {
        border-bottom: 1.6px solid #077bb2;
      }
    }
    > textarea {
      border-bottom: 1.6px solid #077bb2 !important;
      border: none;
      min-height: 30px !important;
      @media screen and (max-width: 480px) {
        min-height: 81px !important;
      }
    }
    > label {
      top: -10px;
      color: #077bb2;
    }
  }

  .reusecore__input {
    margin-top: 40px;

    .field-wrapper {
      position: initial;
      > input {
        border: none;
        border-bottom: 0.6px solid #bababa;
        border-radius: 0;
        padding-top: 20px;
        padding-bottom: 5px;
        color: black;
        padding-left: 0px !important;
        font-family: "Work Sans";
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 21px;
        letter-spacing: -0.02em;
        color: #000000;
        background-color: white !important;
      }
      > input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 40px white inset !important;
      }

      .input-icon {
        position: absolute !important;
        width: 100% !important;
        top: 80% !important;
        font-family: "Work Sans";
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
        line-height: 19px;
        letter-spacing: -0.02em;
        display: flex;
        justify-content: flex-start !important;
        color: #e62f2f;
      }
    }
    > label {
      position: absolute;
      top: 20px;
      left: 0;
      display: flex;
      align-items: center;
      font-weight: 500;
      font-size: 18px;
      line-height: 21px;
      letter-spacing: -0.02em;
      color: #666666;
      @media screen and (max-width: 480px) {
        font-size: 14px;
      }
    }
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  .reusecore__button {
    margin-top: 50px;
    width: 225px;
    height: 54px;
    padding: 14px 36px;
    border-radius: 30px;
    font-family: "Work Sans";
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 23px;
    letter-spacing: -0.02em;

    color: #191919;
  }
`;

export const CombineInput = styled.div`
  display: flex;
  gap: 35px;
  & > * {
    width: 50%;
    @media only screen and (max-width: 667px) {
      width: 100% !important;
    }
  }
  @media screen and (max-width: 700px) {
    display: block;
  }
  .custom_need_form_input {
    flex: 1;
  }
  .error_input {
    .field-wrapper {
      > input {
        border-bottom: 1.6px solid #e52f2f !important;
      }
    }
  }
`;
export const ErrorInput = styled.div`
  borderbottom: 1.6px solid red;
`;
