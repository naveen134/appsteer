// import ModalContact from "common/components/Modal";
import React, { useState, useEffect } from "react";
import {
  FormWrapper,
  ButtonWrapper,
  CombineInput,
} from "./customNeedForm.style";
import Input from "common/components/Input";
import Button from "common/components/Button";
import PhoneInput from "react-phone-number-input";
import ReCAPTCHA from "react-google-recaptcha";
import { isValidPhoneNumber } from "react-phone-number-input";
import axios from "axios";
import Swal from "sweetalert2";
import Spinner from "common/components/Spinner/Spinner";
import { formatData } from "helpers";

const maxlength = 70;

const CustomNeedForm = ({ onSubmit }) => {
  const [expData, setExpData] = useState();

  const [formErrors, setFormErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [isCaptchaChecked, setIsCaptchaChecked] = useState(false);

  const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
  const Emailregex =
    /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,6}))$/;
  const NumberRegex = /^[0]?[789]\d{9}$/;
  const usPhoneNumberRegex = /^\d{3}-\d{3}-\d{4}$|^\(\d{3}\)\d{3}-\d{4}$/;
  const indianPhoneNumberRegex = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;

  useEffect(() => {
    const exploreData = JSON.parse(localStorage.getItem("explore-form"));
    setExpData(formatData(exploreData));
  }, []);

  const [inputValues, setInputValue] = useState({
    FirstName: "",
    LastName: "",
    Email: "",
    Number: "",
    Message: "",
    // explore_data: "",
  });
  //   const [validation, setValidation] = useState({
  //     Name: "",
  //     Email: "",
  //     Number: "",
  //     Message: "",
  //   });

  function handleChange(name, event) {
    const value = event;
    // if (name == "Number") {
    //   if (NumberRegex.test(event)) {
    //     setInputValue({ ...inputValues, [name]: value });
    //   }
    // } else {
    setInputValue({ ...inputValues, [name]: value });
    // }
  }
  function handleInput(event) {
    event.target.style.height = "auto";
    event.target.style.height = `${event.target.scrollHeight}px`;
  }
  const handleBlur = (name, value) => {
    let errors = formErrors;
    switch (name) {
      case "FirstName": {
        if (value === "") {
          errors.FirstName = "First Name required!";
          setFormErrors({ ...errors });
        } else if (!nameRegex.test(value)) {
          errors.FirstName = "Enter valid First Name!";
          setFormErrors({ ...errors });
        } else {
          errors.FirstName = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      case "LastName": {
        if (value === "") {
          errors.LastName = "Last Name required!";
          setFormErrors({ ...errors });
        } else if (!nameRegex.test(value)) {
          errors.LastName = "Enter valid Last Name!";
          setFormErrors({ ...errors });
        } else {
          errors.LastName = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      case "Email": {
        if (value === "") {
          errors.Email = "Email required!";
          setFormErrors({ ...errors });
        } else if (!Emailregex.test(value)) {
          errors.Email = "Enter valid Email";
          setFormErrors({ ...errors });
        } else {
          errors.Email = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      case "Number": {
        if (inputValues?.Number === "") {
          errors.Number = "Number required!";
          setFormErrors({ ...errors });
        } else if (!isValidPhoneNumber(`${inputValues?.Number}`)) {
          errors.Number = "Enter valid Number!";
          setFormErrors({ ...errors });
        } else {
          errors.Number = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      // case "Number": {
      //   if (
      //     !usPhoneNumberRegex.test(value) &&
      //     !indianPhoneNumberRegex.test(value)
      //   ) {
      //     errors.Number = "Number required!";
      //     setFormErrors({ ...errors });
      //   } else if (!isValidPhoneNumber(`${inputValues?.Number}`)) {
      //     errors.Number = "Enter valid Number";
      //     setFormErrors({ ...errors });
      //   } else {
      //     errors.Number = "";
      //     setFormErrors({ ...errors });
      //   }
      //   return;
      // }
      // case "State": {
      //   if (value === "") {
      //     errors.State = "State required!";
      //     setFormErrors({ ...errors });
      //   } else if (!nameRegex.test(value)) {
      //     errors.State = "Enter valid State!";
      //     setFormErrors({ ...errors });
      //   } else {
      //     errors.State = "";
      //     setFormErrors({ ...errors });
      //   }
      //   return;
      // }
      default:
        return;
    }
  };
  const checkValidation = (values) => {
    let errors = {};

    if (!values.FirstName) {
      errors.FirstName = "First Name required!";
    } else if (!nameRegex.test(values.FirstName)) {
      errors.FirstName = "Enter valid FirstName!";
    }
    if (!values.LastName) {
      errors.LastName = "Last Name required!";
    } else if (!nameRegex.test(values.LastName)) {
      errors.LastName = "Enter valid LastName!";
    }
    if (!values.Email) {
      errors.Email = "Email required!";
    } else if (!Emailregex.test(values.Email)) {
      errors.Email = "Enter valid Email!";
    }
    if (!values.Number) {
      errors.Number = "Number required!";
    } else if (!isValidPhoneNumber(values.Number)) {
      errors.Number = "Invalid Number!";
    }
    // if (!values.JobTitle) {
    //   errors.JobTitle = "Job Title required";
    // }
    // if (!values.Company) {
    //   errors.Company = "Company Name required";
    // }
    return errors;
  };
  // useEffect(() => {
  //   if (Object.keys(formErrors).length === 0 && isSubmit) {
  //   }
  // }, [inputValues]);
  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   setFormErrors(checkValidation(inputValues));
  //   setIsSubmit(true);
  //   onSubmit(inputValues);
  // };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const err = checkValidation(inputValues);

    // console.log(err, "as");
    if (Object.keys(err).length > 0) {
      setFormErrors(err);
    } else if (isCaptchaChecked) {
      setLoading(true);
      Swal.fire({
        title: "Thank you for your interest",
        icon: "success",
        showCancelButton: false,
        showConfirmButton: false,
      });
      setInputValue({
        FirstName: "",
        LastName: "",
        Email: "",
        Number: "",
        Message: "",
        // explore_data: "",
      });
      // console.log("forms", { inputValues }) if (!isCaptchaChecked) ;
      const postLead = await axios.post(
        `https://qtkbppesld.execute-api.ap-southeast-2.amazonaws.com/qa/createlead`,
        {
          first_name: inputValues?.FirstName,
          last_name: inputValues?.LastName,
          // Lead_Source: "Appsteer website - Contact us",
          email: inputValues?.Email,
          contact_number: inputValues?.Number,
          message: `Message -  ${inputValues.Message} \n\n  ${expData}`,

          // explore_data: inputValues.explore_data,
          // company: inputValues?.Company,
          // job_title: inputValues?.JobTitle,
          // state: inputValues?.State,
        }

        // {
        //   headers: {
        //     Authorization: `Zoho-oauthtoken 1000.f8bcf46551e166e545819469f3bed2a6.5ddd87e5b99abb6479ac3f443afc21a1`,
        //     "Content-Type": "application/json",
        //   },
        // }
      );
      if (postLead.status === 200) {
        // Swal.fire("Thank you for your interest", "", "success");
        // Swal.fire({
        //   title: "Thank you for your interest",
        //   icon: "success",
        //   showCancelButton: false,
        //   showConfirmButton: false,
        // });
        setLoading(false);
        localStorage.removeItem("explore-form");
      }
      // router.push(
      //   {
      //     pathname: "/contact-us/success",
      //     query: {
      //       data: JSON.stringify(inputValues),
      //     },
      //   },
      //   "/contact-us/success"
      // );
      // console.log("inputvalues", postLead);
    }

    setIsSubmit(true);
  };
  return (
    <FormWrapper inputValue={inputValues}>
      <form onSubmit={handleSubmit}>
        <CombineInput>
          <Input
            label="First Name"
            onChange={(e) => handleChange("FirstName", e)}
            onBlur={(e) => handleBlur("FirstName", e?.target?.value)}
            name="FirstName"
            value={inputValues.FirstName}
            className={`custom_need_form_input ${
              formErrors.FirstName
                ? "error_input"
                : inputValues.FirstName
                ? "input_value"
                : ""
            }`}
            icon={formErrors.FirstName}
          />
          <Input
            label="Last Name"
            onChange={(e) => handleChange("LastName", e)}
            onBlur={(e) => handleBlur("LastName", e?.target?.value)}
            name="LastName"
            value={inputValues.LastName}
            className={`custom_need_form_input ${
              formErrors.LastName
                ? "error_input"
                : inputValues.LastName
                ? "input_value"
                : ""
            }`}
            icon={formErrors.LastName}
          />
        </CombineInput>
        <CombineInput>
          <Input
            label="Email"
            onChange={(e) => handleChange("Email", e)}
            onBlur={(e) => handleBlur("Email", e?.target?.value)}
            name="Email"
            value={inputValues.Email}
            className={`custom_need_form_input ${
              formErrors.Email
                ? "error_input"
                : inputValues.Email
                ? "input_value"
                : ""
            }`}
            icon={formErrors.Email}
          />
          {/* <Input
            label="Phone Number"
            onChange={(e) => handleChange("Number", e)}
            onBlur={(e) => handleBlur("Number", e?.target?.value)}
            className={`custom_need_form_input ${
              formErrors.Number
                ? "error_input"
                : inputValues.Number
                ? "input_value"
                : ""
            }`}
            // inputType="number"
            value={inputValues.Number}
            name="Number"
            icon={formErrors.Number}
          /> */}
          <span className="phone-input">
            <PhoneInput
              international={false}
              // defaultCountry="IN"
              className={`contact-phone-input ${
                formErrors.Number ? "ph-err" : ""
              }`}
              // className={` ${
              //   formErrors.Number
              //     ? "contact-phone-input"
              //     : inputValues.Number
              //     ? "contact-phone-input"
              //     : ""
              // }`}
              placeholder="Enter phone number"
              value={inputValues.Number}
              onChange={(e) => handleChange("Number", e)}
              onBlur={(e) => handleBlur("Number", e?.target?.value)}
              icon={formErrors.Number}
            />
            <span className="err">{formErrors.Number}</span>
          </span>

          {/* <PhoneInput
            international={false}
            // defaultCountry="IN"
            className={`contact-phone-input ${
              formErrors.Number ? "ph-err" : ""
            }`}
            placeholder="Enter phone number"
            value={inputValues.Number}
            onChange={(e) => handleChange("Number", e)}
            onBlur={(e) => handleBlur("Number", e?.target?.value)}
          />
          <span className="err">{formErrors.Number}</span> */}
        </CombineInput>
        {/* <CombineInput>
          <Input
            label="Company"
            onChange={(e) => handleChange("Company", e)}
            name="Company"
            className={`custom_need_form_input ${
              formErrors.Company
                ? "error_input"
                : inputValues.Company
                ? "input_value"
                : ""
            }`}
            icon={formErrors.Company}
          />
          <Input
            label="Job Title"
            onChange={(e) => handleChange("JobTitle", e)}
            name="JobTitle"
            className={`custom_need_form_input ${
              formErrors.JobTitle
                ? "error_input"
                : inputValues.JobTitle
                ? "input_value"
                : ""
            }`}
            icon={formErrors.JobTitle}
          />
        </CombineInput> */}
        {/* <Input
          label="State"
          onChange={(e) => handleChange("State", e)}
          onBlur={(e) => handleBlur("State", e?.target?.value)}
          name="State"
          value={inputValues.State}
          className={
            formErrors.State
              ? "error_input"
              : inputValues.State
              ? "input_value"
              : ""
          }
          icon={formErrors.State}
        /> */}
        <Input
          name="Message"
          label={
            inputValues.Message
              ? `Your message (${
                  maxlength - inputValues.Message.length
                } characters left)`
              : "Your message"
          }
          onChange={(e) => handleChange("Message", e)}
          value={inputValues.Message}
          className={
            formErrors.Message
              ? "error_input"
              : inputValues.Message
              ? "underline input_value"
              : " "
          }
          icon={formErrors.Message}
          inputType="textarea"
          spellcheck="false"
          maxlength={maxlength}
          contenteditable="true"
          autosize
          onInput={handleInput}
        />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBlock: "20px",
          }}
        >
          <ReCAPTCHA
            sitekey="6LfarnspAAAAAPbEZeizygiytGpk6tOYZHol0DBq"
            onChange={() => setIsCaptchaChecked(true)}
          />
        </div>
        {/* {loading ? (
          <Spinner />
        ) : ( */}
        <ButtonWrapper>
          <Button
            type="submit"
            // onClick={}
            onClick={handleSubmit}
            title="Submit"
            iconPosition="center"
            disabled={!isCaptchaChecked}
          />
        </ButtonWrapper>
        {/* )} */}
      </form>
    </FormWrapper>
  );
};

export default CustomNeedForm;
