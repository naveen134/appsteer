import styled from "styled-components";

export const Form = styled.div`
  width: 100%;
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
  padding: 30px 60px 198px 60px;
  margin-bottom: 228px;
  & > span {
    display: block;
  }
  @media (max-width: 768px) {
    padding: 30px;
  }
`;

export const FormTextWrapper = styled.div`
  width: 100%;
  position: relative;
`;

export const FormTextWithSelect = styled.p`
  font-weight: 500;
  font-size: 24px;
  line-height: 24px;
  color: #666666;
  margin: 0;
  @media (max-width: 768px) {
    font-size: 16px;
    line-height: 18px;
  }
`;

export const FormText = styled.span`
  line-height: 38px;
  @media (max-width: 768px) {
    line-height: 24px;
  }
`;

export const ResultButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;
