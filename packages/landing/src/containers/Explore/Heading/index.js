import Container from "common/components/UI/Container";
import { ColoredText } from "../explore.styles";
import { Section, HeadingText } from "./heading.style";

const ExploreHeading = () => {
  return (
    <Section>
      <Container>
        <HeadingText>
          Let’s <ColoredText>analyse</ColoredText> your needs
        </HeadingText>
      </Container>
    </Section>
  );
};

export default ExploreHeading;
