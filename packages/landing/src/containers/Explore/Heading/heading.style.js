import styled from "styled-components";

export const Section = styled.section`
  padding: 5rem 0 3rem;
  overflow: hidden;
  position: inherit;
  @media (max-width: 2000px) {
    padding: 10rem 0 3rem;
  }
  @media (max-width: 1440px) {
    padding: 9rem 0 3rem;
  }
  @media (max-width: 768px) {
    padding: 8rem 0 3rem;
  }
  @media (max-width: 480px) {
    padding: 7rem 0 0;
  }
`;

export const HeadingText = styled.h1`
  font-size: 62px;
  line-height: 1.35;
  margin-bottom: 0px;
  margin-top: 0px;
  letter-spacing: -0.5px;
  text-align: center;
  font-weight: 500;
  @media (max-width: 1366px) {
    font-size: 62px;
  }
  @media (max-width: 1024px) {
    font-size: 50px;
    line-height: 52px;
  }
  @media (max-width: 768px) {
    font-size: 42px;
  }
  @media (max-width: 480px) {
    font-size: 30px;
    line-height: 32px;
    margin-bottom: 10px;
    margin-top: 10px;
  }
`;
