import { useEffect, useState } from "react";
import Container from "common/components/UI/Container";
import {
  Section,
  StyledHeading,
  CardWrapper,
  Card,
  CardTitle,
  CardJobTitle,
  Description,
  CardImage,
  CardContainer,
  CardHeading,
  SocialLink,
  SocialLinkHeading,
  HeadingWrapper,
  ButtonWrapper,
  StyledButton,
} from "./gameChangers.style";
import Image from "common/components/Image";
import LinkedInIcon from "common/assets/image/LinkedInIcon.svg";
import TwitterIcon from "common/assets/image/TwitterIcon.svg";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import leftArrow from "common/assets/image/appsteer/testimonial-arrow.png";
import rightArrow from "common/assets/image/appsteer/right-arrow.png";
import Link from "next/link";

const GameChangers = ({ gameChangers }) => {
  const [swiper, setSwiper] = useState(null);
  const [domLoaded, setDomLoaded] = useState(false);
  const nexto = () => {
    swiper.slideNext();
  };
  const prevto = () => {
    swiper.slidePrev();
  };

  useEffect(() => {
    setDomLoaded(true);
  }, []);
  return (
    <Section>
      <Container>
        <StyledHeading> The game changers </StyledHeading>
        <div style={{ display: "flex", alignItems: "center" }}>
          <img src={leftArrow.src} onClick={prevto} />
          {domLoaded && (
            <Swiper
              slidesPerView={3}
              spaceBetween={20}
              // centeredSlides={true}
              initialSlide={0}
              loop={true}
              onSwiper={(s) => {
                setSwiper(s);
              }}
              centeredSlidesBounds={true}
              // slidesPerGroup={2}
              className="mySwiper"
              breakpoints={{
                1000: {
                  slidesPerView: 2,
                },
                300: {
                  slidesPerView: 1,
                },
              }}
            >
              {gameChangers.map((gameChanger) => {
                return (
                  <SwiperSlide key={gameChanger?.id}>
                    <CardContainer>
                      <CardWrapper>
                        <Card>
                          <CardImage>
                            <Image
                              src={
                                gameChanger?.ProfileImage?.data?.attributes?.url
                              }
                              alt="Image"
                            ></Image>{" "}
                          </CardImage>
                          <HeadingWrapper>
                            <CardHeading>
                              <CardTitle>{gameChanger.Name},&nbsp;</CardTitle>
                              <CardJobTitle>
                                {gameChanger.JobTitle}
                              </CardJobTitle>
                            </CardHeading>
                            <SocialLinkHeading>
                              <SocialLink>
                                <a
                                  href={
                                    gameChanger.LinkedInLink
                                      ? gameChanger.LinkedInLink
                                      : "#"
                                  }
                                  target="_blank"
                                >
                                  <Image src={LinkedInIcon?.src} alt="Image" />
                                </a>
                              </SocialLink>
                              <SocialLink>
                                <a
                                  href={
                                    gameChanger.TwitterLink
                                      ? gameChanger.TwitterLink
                                      : "#"
                                  }
                                  target="_blank"
                                >
                                  <Image src={TwitterIcon?.src} alt="Image" />
                                </a>
                              </SocialLink>
                            </SocialLinkHeading>
                          </HeadingWrapper>
                        </Card>
                        <Description>{gameChanger.JobDescription}</Description>
                      </CardWrapper>
                    </CardContainer>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          )}
          <img src={rightArrow.src} onClick={nexto} />
        </div>
        <ButtonWrapper>
          <Link href="/team">
            <StyledButton>See our full team</StyledButton>
          </Link>
        </ButtonWrapper>
      </Container>
    </Section>
  );
};

export default GameChangers;
