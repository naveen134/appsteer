import styled from "styled-components";
import themeGet from "@styled-system/theme-get";

export const Section = styled.section`
  position: relative;
  z-index: 0;
  padding-block:5rem;
  background-color: #fff5eb;
  // &::after {
  //   content: "";
  //   position: absolute;
  //   width: 100%;
  //   bottom: -1px;
  //   height: 45px;
  //   @media only screen and (min-width: 768px) {
  //     // bottom: -1px;
  //     // height: 80px;
  //   }
  //   @media only screen and (min-width: 1024px) {
  //     // height: 90px;
  //   }
  //   @media only screen and (min-width: 1280px) {
  //     // height: 170px;
  //   }
  //   @media only screen and (min-width: 1366px) {
  //     // height: 130px;
  //   }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  gap: 50px;
  align-items: center;
  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

export const StyledHeading = styled.h2`
  font-size: 48px;
  font-weight: 500;
  text-align: center;
  margin-block: 0;
`;

export const StyledSubText = styled.p`
  font-size: 18px;
  font-weight: 400;
  line-height: 32px;
  color: #666666;
`;

export const StyledImg = styled.img`
  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const Heading = styled.div`
  margin-top: 120px;
  text-align: center;
`;
export const TitleHeading = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 48px;
  line-height: 56px;
  letter-spacing: -0.02em;
  color: #191919;
  margin-top: 60px;
`;
export const CardContainer = styled.div`
  flex: 0 0 0;
  margin-inline: 15px;
  grid-gap: 25px;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  @media (max-width: 991px) {
    flex: 0 0 100%;
    max-width: 79%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 0px;
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  @media (max-width: 768px) {
    max-width: 100%;
    grid-gap: 20px;
  }
  @media (max-width: 426px) {
    grid-gap: 25px;
    grid-template-columns: repeat(1, minmax(0, 1fr));
  }
`;
export const CardWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
  margin-top: 60px;
  padding: 40px;
  @media (max-width: 480px) {
    padding: 20px;
  }
`;
export const Card = styled.div`
  display: flex;
  align-items: center;
`;
export const CardHeading = styled.div`
  margin-left: 20px;
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
export const CardTitle = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  line-height: 24px;
  letter-spacing: -0.02em;
  color: #191919;
  @media (max-width: 480px) {
    font-size: 20px;
    line-height: 24px;
  }
`;
export const CardJobTitle = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  line-height: 24px;
  letter-spacing: -0.02em;
  color: #666666;
  @media (max-width: 480px) {
    font-size: 20px;
    line-height: 24px;
  }
`;
export const Description = styled.div`
  font-family: "Work Sans";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 28px;
  letter-spacing: 0.01em;
  text-align: justify;
  color: #666666;
  margin-top: 30px;
  @media (max-width: 480px) {
    font-size: 16px;
    line-height: 24px;
  }
`;
export const SocialLink = styled.div`
  width: 42px;
  height: 42px;
  background: #f5f5f5;
  border-radius: 20px;
  img {
    margin: 10px;
  }
`;
export const SocialLinkHeading = styled.div`
  display: flex;
  margin-left: 20px;
  margin-top: 20px;
  gap: 10px;
`;
export const HeadingWrapper = styled.div``;
export const CardImage = styled.div``;

export const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 60px;
`;

export const StyledButton = styled.button`
  display: inline-flex;
  background-color: ${themeGet("colors.primary")};
  color: ${themeGet("colors.black")};
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  font-weight: 500;
  font-size: 20px;
  cursor: pointer;
  line-height: 22px;
  position: relative;
  padding: 19.5px 35px;
  border: none;
  outline: none;
  &:hover {
    background-color: ${themeGet("colors.primaryHover")};
  }
  transition: all 0.4s ease;
  i {
    margin-left: 10px;
    position: relative;
    top: -1px;
  }
  span {
    position: relative;
    display: flex;
  }
  @media (max-width: 468px) {
    padding: 10px 20px;
    font-size: 16px;
  }
`;
