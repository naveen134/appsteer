import Container from "common/components/UI/Container";
import {
  StyledHeading,
  StyledSection,
  StyledCard,
  IconHolder,
  TextHolder,
  SubText,
  StyledCardPlain,
  CardHeading,
  CardSubText,
  RightPattern,
  LeftPattern,
  CardHolder,
  CardHolderUpper,
} from "./coreValues.style";
import lightBulb from "common/assets/image/about-us/light-bulb.png";
import people from "common/assets/image/about-us/people.png";
import frame from "common/assets/image/about-us/frame.png";
import lightBulbFrame from "common/assets/image/about-us/light-bulb-frame.png";
import peopleFrame from "common/assets/image/about-us/people-frame.png";
import integrityFrame from "common/assets/image/about-us/integrity-frame.png";
import rightPattern from "common/assets/image/about-us/core-values-right-pattern.png";
import leftPattern from "common/assets/image/about-us/core-values-left-pattern.png";

const CoreValues = ({ aboutUsCoreValues }) => {
  const {
    coreValueOne,
    coreValueTwo,
    coreValueThree,
    coreValueWithDescription,
    coreValueWithDescriptionHeading,
  } = aboutUsCoreValues;

  return (
    <Container>
      <div style={{ position: "relative" }}>
        <RightPattern src={rightPattern.src} />
        <LeftPattern src={leftPattern.src} />
        <StyledSection>
          <StyledHeading> Our Core Values </StyledHeading>
          <CardHolderUpper
            style={{ display: "flex", gap: "50px", flexWrap: "wrap" }}
          >
            <StyledCard
              style={{ backgroundImage: `url(${lightBulbFrame.src})` }}
            >
              {/* <IconHolder>
              <img src={lightBulb.src} />
            </IconHolder> */}
              <TextHolder>
                <SubText> {coreValueOne} </SubText>
              </TextHolder>
            </StyledCard>

            <StyledCard style={{ backgroundImage: `url(${peopleFrame.src})` }}>
              {/* <IconHolder>
              <img src={people.src} />
            </IconHolder> */}
              <TextHolder>
                <SubText> {coreValueTwo} </SubText>
              </TextHolder>
            </StyledCard>
          </CardHolderUpper>

          <CardHolder
            style={{
              display: "flex",
              gap: "50px",
              marginTop: "60px",
              justifyContent: "end",
              flexWrap: "wrap",
            }}
          >
            <StyledCardPlain>
              <CardHeading> {coreValueWithDescriptionHeading} </CardHeading>
              <CardSubText> {`"${coreValueWithDescription}"`} </CardSubText>
            </StyledCardPlain>

            <StyledCard
              style={{ backgroundImage: `url(${integrityFrame.src})` }}
            >
              {/* <IconHolder>
              <img src={frame.src} />
            </IconHolder> */}
              <TextHolder>
                <SubText> {coreValueThree} </SubText>
              </TextHolder>
            </StyledCard>
          </CardHolder>
        </StyledSection>
      </div>
    </Container>
  );
};

export default CoreValues;
