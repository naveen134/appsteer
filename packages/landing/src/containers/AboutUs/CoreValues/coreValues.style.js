import styled from "styled-components";
import orangeRectangle from "common/assets/image/about-us/orange-rectangle.png";

export const StyledSection = styled.section`
  padding-block: 5rem;
  @media (max-width: 600px) {
    padding-block: 3rem;
  }
`;

export const StyledHeading = styled.h2`
  font-size: 48px;
  font-weight: 500;
  text-align: center;
  margin-top: 0px;
  @media (max-width: 600px) {
    font-size: 38px;
  }
`;

export const CardHolderUpper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 50px;
  @media (max-width: 600px) {
    justify-content: center;
  }
`;

export const CardHolder = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 50px;
  margin-top: 60px;
  justify-content: end;
  @media (max-width: 600px) {
    justify-content: center !important;
  }
`;

export const StyledCard = styled.div`
  border-radius: 40px;
  height: 400px;
  width: 365px;
  background-size: contain;
  background-repeat: no-repeat;
  position: relative;
  @media (max-width: 480px) {
    width: 320px;
  }
  @media (max-width: 900px) {
    margin: auto;
  }
`;

export const StyledCardPlain = styled.div`
  border-radius: 40px;
  height: 400px;
  width: 365px;
  box-shadow: 0px 10px 90px rgba(0, 0, 0, 0.12);
  padding: 40px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 480px) {
    width: 320px;
  }
  @media (max-width: 900px) {
    margin: auto;
  }
`;

export const RightPattern = styled.img`
  position: absolute;
  right: -85px;
  z-index: -1;
  top: 25%;
  @media (max-width: 880px) {
    right: 0;
  }
`;

export const LeftPattern = styled.img`
  position: absolute;
  bottom: 0px;
  left: -61px;
  z-index: -3;
  @media (max-width: 500px) {
    width: 100%;
  }
`;

export const IconHolder = styled.div`
  background-color: #f5f5f5;
  position: absolute;
  top: 90px;
  left: 30px;
  border-radius: 50%;
  padding: 15px;
`;

export const TextHolder = styled.div`
  background-image: url(${orangeRectangle.src});
  position: absolute;
  width: 100%;
  bottom: 0px;
  display: flex;
  align-items: center;
  height: 124px;
  border-radius: 40px;
  @media (max-width: 480px) {
    bottom: 30px;
  }
  @media (max-width: 320px) {
    bottom: 80px;
  }
`;

export const SubText = styled.h4`
  font-size: 32px;
  font-weight: 500;
  color: #55290d;
  margin-left: 45px;
  @media (max-width: 480px) {
    font-size: 28px;
    text-align: center;
    margin: auto;
  }
`;

export const CardHeading = styled.h6`
  font-size: 32px;
  font-weight: 500;
  color: #191919;
  margin-block: 0px;
`;

export const CardSubText = styled.p`
  font-size: 20px;
  font-weight: 400;
  line-height: 32px;
  color: #666666;
  @media (max-width: 480px) {
    font-size: 18px;
    line-height: 32px;
  }
`;
