import {
  FlexContainer,
  Section,
  StyledHeading,
  StyledImg,
} from "./aboutUsBusinesses.style";

const AboutUsBusinesses = ({ businessHeading, clientLogos }) => {
  return (
    <Section>
      <StyledHeading> {businessHeading} </StyledHeading>
      <FlexContainer>
        {clientLogos.map((logo) => {
          return (
            <StyledImg
              key={logo.id}
              src={logo?.CompanyLogo?.data?.attributes?.url}
            />
          );
        })}
      </FlexContainer>
    </Section>
  );
};

export default AboutUsBusinesses;
