import styled from "styled-components";

export const Section = styled.section`
  padding-inline: 50px;
  padding-top: 3rem;
  background-color: #f5f5f5;
`;

export const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-block: 3rem;
`;

export const StyledHeading = styled.h2`
  font-size: 48px;
  line-height: 56px;
  margin-block: 0px;
  font-weight: 500;
  text-align: center;
  @media (max-width: 500px) {
    font-size: 43px;
    line-height: 60px;
  }
`;

export const StyledImg = styled.img`
  margin-block: 30px;
`;
