import styled from "styled-components";
import themeGet from "@styled-system/theme-get";

export const Section = styled.section`
  position: relative;
  background-color: #191919;
  z-index: 0;
  padding-block:5rem;
  // &::after {
  //   content: "";
  //   position: absolute;
  //   width: 100%;
  //   bottom: -1px;
  //   height: 45px;
  //   @media only screen and (min-width: 768px) {
  //     // bottom: -1px;
  //     // height: 80px;
  //   }
  //   @media only screen and (min-width: 1024px) {
  //     // height: 90px;
  //   }
  //   @media only screen and (min-width: 1280px) {
  //     // height: 170px;
  //   }
  //   @media only screen and (min-width: 1366px) {
  //     // height: 130px;
  //   }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  gap: 50px;
  align-items: center;
  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const StyledContainer = styled.div`
  @media only screen and (max-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const StyledHeading = styled.h2`
  font-size: 48px;
  font-weight: 500;
  color: #ffffff;
  margin-top: 0px;
  margin-bottom: 20px;
  @media only screen and (max-width: 768px) {
    text-align: center;
  }
  @media only screen and (max-width: 480px) {
    font-size: 38px;
  }
`;

export const StyledSubText = styled.p`
  font-size: 20px;
  font-weight: 400;
  line-height: 38px;
  color: #ffffff;
  @media only screen and (max-width: 480px) {
    font-size: 18px;
    line-height: 32px;
  }
`;

export const StyledImg = styled.img`
  @media only screen and (max-width: 1000px) {
    width: 100%;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: start;
`;

export const StyledImgText = styled.p`
  font-size: 40px;
  font-weight: 500;
  line-height: 57px;
  color: #ffffff;
  margin-block: 0px;
  position: absolute;
  bottom: 40px;
  left: 0;
  right: 0;
  text-align: center;
  @media (max-width: 1440px) {
    font-size: 32px;
    line-height: 40px;
  }
  @media (max-width: 959px) {
    font-size: 24px;
    line-height: 32px;
  }
  @media (max-width: 480px) {
    font-size: 20px;
    line-height: 24px;
  }
`;

export const StyledButton = styled.button`
  display: inline-flex;
  background-color: ${themeGet("colors.primary")};
  color: ${themeGet("colors.black")};
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  font-weight: 500;
  font-size: 20px;
  cursor: pointer;
  line-height: 22px;
  position: relative;
  padding: 19.5px 35px;
  border: none;
  outline: none;
  &:hover {
    background-color: ${themeGet("colors.primaryHover")};
  }
  transition: all 0.4s ease;
  i {
    margin-left: 10px;
    position: relative;
    top: -1px;
  }
  span {
    position: relative;
    display: flex;
  }
  @media (max-width: 468px) {
    padding: 10px 20px;
    font-size: 16px;
  }
`;
