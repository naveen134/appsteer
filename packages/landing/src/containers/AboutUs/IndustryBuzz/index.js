import {
  FlexContainer,
  Section,
  StyledHeading,
  StyledImg,
  StyledButton,
  ButtonWrapper,
  StyledSubText,
  StyledImgText,
  StyledContainer,
} from "./industryBuzz.style";
import Container from "common/components/UI/Container";
import leftPattern from "common/assets/image/about-us/industry-buzz-left-pattern.png";
import rightPattern from "common/assets/image/about-us/industry-buzz-right-pattern.png";

const IndustryBuzz = ({
  industryBuzzHeading,
  industryBuzzSubheading,
  industryBuzzImage,
  industryBuzzImageText,
}) => {
  return (
    <Section>
      <Container>
        {/* <img src={leftPattern.src} />
        <img src={rightPattern.src} /> */}
        <FlexContainer>
          <StyledContainer>
            <StyledHeading> {industryBuzzHeading} </StyledHeading>
            <StyledSubText>{industryBuzzSubheading}</StyledSubText>
            <ButtonWrapper>
              <StyledButton>Check out Newsroom</StyledButton>
            </ButtonWrapper>
          </StyledContainer>
          <div style={{ position: "relative" }}>
            <StyledImg
              src={industryBuzzImage?.data?.attributes?.url}
              alt="Award"
            />
            <StyledImgText>"{industryBuzzImageText}"</StyledImgText>
          </div>
        </FlexContainer>
      </Container>
    </Section>
  );
};

export default IndustryBuzz;
