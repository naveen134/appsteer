import styled from "styled-components";

export const Section = styled.section`
  position: relative;
  z-index: 0;
  padding-bottom:5rem;
  // &::after {
  //   content: "";
  //   position: absolute;
  //   width: 100%;
  //   bottom: -1px;
  //   height: 45px;
  //   @media only screen and (min-width: 768px) {
  //     // bottom: -1px;
  //     // height: 80px;
  //   }
  //   @media only screen and (min-width: 1024px) {
  //     // height: 90px;
  //   }
  //   @media only screen and (min-width: 1280px) {
  //     // height: 170px;
  //   }
  //   @media only screen and (min-width: 1366px) {
  //     // height: 130px;
  //   }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  gap: 50px;
  align-items: center;
  @media only screen and (max-width: 1000px) {
    flex-wrap: wrap;
    justify-content: center;
  }
`;

export const StyledHeading = styled.h2`
  font-size: 48px;
  font-weight: 500;
  @media (max-width: 480px) {
    font-size: 40px;
    line-height: 61px;
    text-align: center;
  }
`;

export const StyledSubText = styled.p`
  font-size: 18px;
  font-weight: 400;
  line-height: 32px;
  color: #666666;
  @media (max-width: 480px) {
    font-size: 14px;
    line-height: 28px;
  }
`;

export const StyledImg = styled.img`
  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const LeftImage = styled.img`
  position: absolute;
  left: -64px;
  z-index: 10;
  bottom: 5px;
  width: 250px;
`;

export const RightImage = styled.img`
  position: absolute;
  right: -35px;
  top: 35%;
  width: 150px;
  z-index: 10;
`;
