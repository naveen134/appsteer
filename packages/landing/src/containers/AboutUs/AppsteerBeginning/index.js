import Container from "common/components/UI/Container";
import {
  FlexContainer,
  Section,
  StyledHeading,
  StyledImg,
  StyledSubText,
} from "./appsteerBeginning.style";

const AppsteerBeginning = ({
  secondSectionHeading,
  secondSectionDescription,
  secondSectionImage,
}) => {
  return (
    <Section>
      <Container>
        <FlexContainer>
          <div>
            <StyledHeading> {secondSectionHeading} </StyledHeading>
            <StyledSubText>
              {secondSectionDescription.replace(/\n/gi, "&nbsp; \n")}
            </StyledSubText>
          </div>
          <div style={{ position: "relative" }}>
            <StyledImg
              src={secondSectionImage?.data?.attributes?.url}
              alt="Cafe"
            />
          </div>
        </FlexContainer>
      </Container>
    </Section>
  );
};

export default AppsteerBeginning;
