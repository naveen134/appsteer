import styled from "styled-components";

export const Section = styled.section`
  position: relative;
  z-index: 0;
  background-color: #FFF4EB;
  // &::after {
  //   content: "";
  //   position: absolute;
  //   width: 100%;
  //   bottom: -1px;
  //   height: 45px;
  //   @media only screen and (min-width: 768px) {
  //     // bottom: -1px;
  //     // height: 80px;
  //   }
  //   @media only screen and (min-width: 1024px) {
  //     // height: 90px;
  //   }
  //   @media only screen and (min-width: 1280px) {
  //     // height: 170px;
  //   }
  //   @media only screen and (min-width: 1366px) {
  //     // height: 130px;
  //   }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  gap: 50px;
  justify-content: space-between;
  padding-block: 3rem;
  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
    gap: 30px;
  }
`;

export const StyledHeading = styled.h3`
  font-size: 62px;
  line-height: 87px;
  margin-block: 0px;
  font-weight: 600;
  @media (max-width: 480px) {
    font-size: 43px;
    line-height: 61px;
  }
`;

export const StyledSubText = styled.p`
  font-size: 24px;
  line-height: 28px;
  color: #666666;
  font-weight: 500;
  margin-block: 0px;
  @media (max-width: 480px) {
    font-size: 18px;
    line-height: 24px;
  }
`;
