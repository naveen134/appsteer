import Container from "common/components/UI/Container";
import {
  FlexContainer,
  Section,
  StyledHeading,
  StyledSubText,
} from "./aboutUsStats.style";
import flag from "common/assets/image/about-us/about-us-flag.png";
import group from "common/assets/image/about-us/about-us-group.png";
import smiley from "common/assets/image/about-us/about-us-smiley.png";

const AboutUsStats = ({ aboutUsStats }) => {
  const { foundedYear, teamMembers, customersData } = aboutUsStats;
  return (
    <Section>
      <Container>
        <FlexContainer>
          <div>
            <img src={flag.src} alt="flag icon" style={{ height: "40px" }} />
            <StyledHeading> {foundedYear} </StyledHeading>
            <StyledSubText> Year founded </StyledSubText>
          </div>
          <div>
            <img src={group.src} alt="group icon" />
            <StyledHeading> {`${teamMembers}+`} </StyledHeading>
            <StyledSubText> Team members </StyledSubText>
          </div>
          <div>
            <img src={smiley.src} alt="smiley icon" />
            <StyledHeading> {`${customersData}+`} </StyledHeading>
            <StyledSubText> Happy Customers </StyledSubText>
          </div>
        </FlexContainer>
      </Container>
    </Section>
  );
};

export default AboutUsStats;
