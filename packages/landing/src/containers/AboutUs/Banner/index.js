import Container from "common/components/UI/Container";
import Heading from "common/components/Heading";
import Text from "common/components/Text";
import Section, {
  BannerContentWrapper,
  BannerContent,
  StyledImg,
  CenterPattern,
  RightPattern,
  StyledHeading,
} from "./banner.style";
import pattern from "common/assets/image/about-us/about-us-left-design-pattern.png";
import centerPattern from "common/assets/image/about-us/about-us-center-design-pattern.png";
import rightPattern from "common/assets/image/about-us/about-us-right-design-pattern.png";

const Banner = ({ bannerHeading, bannerSubHeading, bannerImage }) => {
  return (
    <Section id="banner">
      <Container width="1400px">
        <BannerContentWrapper>
          <BannerContent>
            <StyledImg src={pattern.src} />

            {/* <Heading
              className="animate__animated animate__fadeInUp"
              content={`${bannerHeading}`}
              style
            /> */}
            <StyledHeading> {bannerHeading} </StyledHeading>

            <Text
              className="animate__animated animate__fadeInUp"
              content={`${bannerSubHeading}`}
            />
            <div style={{ position: "relative" }}>
              <img
                src={bannerImage?.data?.attributes?.url}
                alt="About Us Hero Image"
                style={{ width: "100%", zIndex: "30" }}
              />
              <CenterPattern src={centerPattern.src} />
            </div>
          </BannerContent>
        </BannerContentWrapper>
        <RightPattern src={rightPattern.src} />
      </Container>
    </Section>
  );
};

export default Banner;
