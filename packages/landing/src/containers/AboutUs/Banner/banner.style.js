import { rgba } from "polished";
import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";
// import pattern1 from 'common/assets/image/appsteer/banner-pattern1.png';
import pattern2 from "common/assets/image/Industry/industry-vertical-pattern.png";
import pattern from "common/assets/image/about-us/about-us-left-design-pattern.png";

const Section = styled.section`
  position: relative;
  z-index: 0;
  padding-bottom:5rem;
  // &::after {
  //   content: "";
  //   position: absolute;
  //   width: 100%;
  //   bottom: -1px;
  //   height: 45px;
  //   @media only screen and (min-width: 768px) {
  //     // bottom: -1px;
  //     // height: 80px;
  //   }
  //   @media only screen and (min-width: 1024px) {
  //     // height: 90px;
  //   }
  //   @media only screen and (min-width: 1280px) {
  //     // height: 170px;
  //   }
  //   @media only screen and (min-width: 1366px) {
  //     // height: 130px;
  //   }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const StyledHeading = styled.h1`
  font-size: 62px;
  font-weight: 500;
  line-height: 76px;
  @media (max-width: 480px) {
    font-size: 43px;
    line-height: 61px;
    text-align: center;
  }
`;

export const Pattern = styled.div`
  @media only screen and (min-width: 1024px) {
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    &::before,
    &::before {
      content: "";
      position: absolute;
    }
    &::before {
      background: url(${pattern?.src}) no-repeat;
      right: 109px;
      top: 91px;
      width: 100%;
      height: 100%;
      @media only screen and (min-width: 768px) {
        background-size: 30%;
        background-position: right 0;
      }
      @media only screen and (min-width: 1024px) {
        background-size: 60%;
      }
      @media only screen and (min-width: 1440px) {
        background-size: 74%;
        right: 0%;
        top: 0;
        height: 130%;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 100%;
        right: 25%;
        background-size: 35%;
      }
    }
  }
`;

export const BannerContentWrapper = styled.div`
  // @media (min-width: 1280px) {
  //   min-height: 100vh;
  // }
`;

export const BannerContent = styled.div`
  padding-top: 210px;
  max-width: 870px;
  margin: 0 auto;
  text-align: center;
  @media (max-width: 1366px) {
    padding-top: 170px;
    max-width: 750px;
  }
  @media (max-width: 1024px) {
    max-width: 660px;
  }
  @media (max-width: 768px) {
    max-width: 550px;
    padding-top: 130px;
  }
  @media (max-width: 480px) {
    padding-top: 100px;
  }
  h2 {
    color: ${themeGet("colors.black")};
    font-weight: 500;
    font-size: 74px;
    line-height: 97px;
    text-align: center;
    letter-spacing: -0.02em;
    margin-inline: auto;
    margin-bottom: 30px;
    @media (max-width: 1366px) {
      font-size: 62px;
      line-height: 85px;
      max-width: 675px;
    }
    @media (max-width: 1024px) {
      font-size: 62px;
      line-height: 85px;
      max-width: 675px;
    }
    @media (max-width: 768px) {
      font-size: 56px;
      line-height: 80px;
      max-width: 675px;
    }
    @media (max-width: 480px) {
      font-size: 43px;
      line-height: 61px;
    }
  }
  p {
    font-weight: 400;
    font-size: 20px;
    line-height: 36px;
    text-align: center;
    letter-spacing: -0.02em;
    color: ${rgba("#666666", 0.6)};
    max-width: 679px;
    margin: 0 auto;
    margin-bottom: 58px;

    @media (max-width: 1366px) {
      max-width: 550px;
      font-size: 18px;
      line-height: 39px;
    }

    @media (max-width: 768px) {
      font-size: 16px;
      line-height: 33px;
      margin-bottom: 39px;
    }

    @media (max-width: 480px) {
      font-size: 14px;
      line-height: 2;
    }
  }
  .button {
    display: inline-flex;
    background-color: ${themeGet("colors.primary")};
    color: ${themeGet("colors.black")};
    align-items: center;
    justify-content: center;
    border-radius: 30px;
    font-weight: 500;
    font-size: 20px;
    line-height: 22px;
    position: relative;
    padding: 19.5px 70px;
    cursor: pointer;
    border: 0;
    &:hover {
      transform: translateY(-7px);
    }
    transition: all 0.4s ease;
    i {
      margin-left: 10px;
      position: relative;
      top: -1px;
    }
    span {
      position: relative;
      display: flex;
    }
    @media (max-width: 767px) {
      padding: 16px 50px;
      font-size: 20px;
    }
  }
`;

export const StyledImg = styled.img`
  position: absolute;
  left: 0px;
  @media (max-width: 800px) {
    width: 20%;
  }
  @media (max-width: 640px) {
    width: 15%;
    top: 165px;
  }
  @media (max-width: 500px) {
    width: 20%;
  }
`;

export const Figure = styled.figure`
  margin: 30px 0 0;
  position: relative;
  text-align: center;
  z-index: 10;
  > div {
    filter: drop-shadow(0px 4px 50px rgba(86, 99, 132, 0.1));
  }
  @media (min-width: 768px) {
    margin-top: 30px;
    max-width: 600px;
    margin-left: auto;
    margin-right: auto;
  }
  @media (min-width: 1024px) {
    max-width: 720px;
    margin-top: 50px;
  }
  @media (min-width: 1366px) {
    max-width: 1040px;
  }
  @media (min-width: 1440px) {
    max-width: 100%;
  }
  @media (min-width: 1600px) {
    margin-top: 60px;
  }
`;

export const CenterPattern = styled.img`
  position: absolute;
  left: 0px;
  width: 90%;
  z-index: -1;
`;

export const RightPattern = styled.img`
  position: absolute;
  right: 0px;
  bottom: -267px;
  z-index: -1;
  @media (max-width: 768px) {
    width: 20%;
  }
  @media (max-width: 668px) {
    bottom: -100px;
  }
`;

export default Section;
