import React, { useEffect, useState } from "react";
import {
  MegaMenuWrapper,
  MegaMenuIndustryColum,
  MegaMenuIndustryColumTwo,
  MegaMenuIndustryColumImage,
} from "./navBar.style";
import { Reveal } from "react-reveal";
import NextImage from "common/components/NextImage";
import Link from "next/link";

import logoIcon from "common/assets/image/appsteer/logo-stats.png";

const MegaMenu = ({ menuStyle, list, setShowMegaMenu, open, top }) => {
  const [secondColumnList, setsecondColumnList] = useState(
    list && list[0]?.content ? list[0]?.content : null
  );
  const [imageUrl, setImageUrl] = useState(list && list[0]?.featuredImage);
  const [selectedIndustry, setSelectedIndustry] = useState(0);

  useEffect(() => {
    if (menuStyle === "columnTwo") {
      const url = list[0]?.featuredImage;
      setImageUrl(url);
    } else if (menuStyle === "columnThree") {
      const columnContent = list[0].content ? list[0].content : null;
      if (columnContent) {
        setsecondColumnList(columnContent);
      } else setsecondColumnList(null);

      const url = list[0]?.featuredImage;
      setImageUrl(url);
    }
  }, [open]);

  const handleMegaMenuColumn = (index) => {
    setSelectedIndustry(index);
    if (menuStyle === "columnTwo") {
      const url = list[index]?.featuredImage;
      setImageUrl(url);
    } else if (menuStyle === "columnThree") {
      const content = list[index].content ? list[index].content : null;
      if (content) {
        setsecondColumnList(content);
      } else setsecondColumnList(null);

      const url = list[index]?.featuredImage;
      setImageUrl(url);
    }
  };

  return (
    open && (
      <MegaMenuWrapper className="mega-top-fix">
        <Reveal effect="animate grow-top">
          <div
            className="center-wraper"
            onMouseLeave={() => setShowMegaMenu(false)}
            role="none"
          >
            <div className="row">
              {menuStyle === "columnThree" && (
                <>
                  <div className="col-12 col-md-4 industry">
                    {list &&
                      list.map((eachIndustry, index) => (
                        <Link href={eachIndustry.link}>
                          <MegaMenuIndustryColum
                            onClick={() => setShowMegaMenu(false)}
                            key={eachIndustry.id}
                            onMouseEnter={() => handleMegaMenuColumn(index)}
                            selected={selectedIndustry === index}
                          >
                            <span> {eachIndustry.text}</span>
                            {selectedIndustry === index && (
                              <NextImage src={logoIcon} alt="logo" />
                            )}
                          </MegaMenuIndustryColum>
                        </Link>
                      ))}
                  </div>
                  <div className="col-12 col-md-4">
                    {secondColumnList &&
                      secondColumnList.map((eachUsecase, index) => (
                        <Link href={eachUsecase.link}>
                          <MegaMenuIndustryColumTwo
                            key={eachUsecase.text}
                            onClick={() => setShowMegaMenu(false)}
                          >
                            {eachUsecase.text}
                          </MegaMenuIndustryColumTwo>
                        </Link>
                      ))}
                  </div>
                  <div className="col-12 col-md-4 megamenu-image">
                    <MegaMenuIndustryColumImage>
                      {" "}
                      {imageUrl && (
                        <NextImage
                          width="236px"
                          height="336px"
                          src={imageUrl}
                          alt="featured Image"
                        />
                      )}
                    </MegaMenuIndustryColumImage>
                  </div>
                </>
              )}

              {menuStyle === "columnTwo" && (
                <>
                  <div className="col-12 col-md-6">
                    {list &&
                      list.map((eachItem, index) => (
                        <Link href={eachItem.link}>
                          <MegaMenuIndustryColum
                            key={eachItem.id}
                            onMouseEnter={() => handleMegaMenuColumn(index)}
                          >
                            {/* {console.log({ eachItem })} */}
                            {eachItem.text}
                          </MegaMenuIndustryColum>
                        </Link>
                      ))}
                  </div>
                  <div className="col-12 col-md-6">
                    <MegaMenuIndustryColumImage>
                      {" "}
                      {imageUrl && (
                        <NextImage
                          width="236px"
                          height="336px"
                          src={imageUrl}
                          alt="featured Image"
                        />
                      )}
                    </MegaMenuIndustryColumImage>
                  </div>
                </>
              )}
            </div>
          </div>
        </Reveal>
      </MegaMenuWrapper>
    )
  );
};

export default MegaMenu;

