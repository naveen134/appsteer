import React from "react";
import { Icon } from "react-icons-kit";
import Fade from "react-reveal/Fade";
import Image from "common/components/Image";
import { arrowRight } from "react-icons-kit/feather/arrowRight";
import Container from "common/components/UI/Container";
import Heading from "common/components/Heading";
import NextImage from "common/components/NextImage";
import Text from "common/components/Text";
import Link from "common/components/Link";

import { Section, SectionHeading, Grid, Article } from "./Usecase.style";

const IndustryVerticals = ({ Usecases }) => {
  // console.log(Usecases)
  return (
    <Section id="newsfeed">
      <Container width="1300px">
        <SectionHeading>
          <Heading content="Use Cases" />
        </SectionHeading>
        <Grid>
          {Usecases &&
            Usecases?.data?.map((Usecase) => (
              <Fade key={Usecase.id} up delay={Usecase.id - 1 * 100}>
                <Article>
                  <Link
                    href={`/usecase/${Usecase?.attributes?.slug}`}
                    key={Usecase.id}
                  >
                    <Image
                      src={
                        Usecase?.attributes?.UseCaseFeaturedImage?.data
                          ?.attributes?.formats?.thumbnail?.url
                      }
                      width={
                        Usecase?.attributes?.UseCaseFeaturedImage?.data
                          ?.attributes?.formats?.thumbnail?.width
                      }
                      height={
                        Usecase?.attributes?.UseCaseFeaturedImage?.data
                          ?.attributes?.formats?.thumbnail?.height
                      }
                      alt="use list image"
                    />
                    {/* <Text content={post.date} /> */}
                    <Heading
                      as="h4"
                      content={Usecase?.attributes?.UseCaseTitle}
                    />
                  </Link>
                  {/* <Link href={post.excerpt.link}>
                  {post.excerpt.label} <Icon icon={arrowRight} />
                </Link> */}
                </Article>
              </Fade>
            ))}
        </Grid>
      </Container>
    </Section>
  );
};

export default IndustryVerticals;
