import { themeGet } from "@styled-system/theme-get";
import styled from "styled-components";

export const Section = styled.section`
  padding-top: 70px;
  padding-bottom: 60px;
  @media screen and (max-width: 480px) {
    padding-bottom: 45px;
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const SectionHeading = styled.div`
  text-align: left;
  padding: 0 15px;
  margin-bottom: 45px;
  @media screen and (max-width: 768px) {
    margin-bottom: 40px;
  }
  h2 {
    font-weight: 700;
    font-size: 62px;
    line-height: 1.5;
    letter-spacing: -0.2px;
    @media screen and (max-width: 768px) {
      font-size: 26px;
    }
    @media screen and (max-width: 480px) {
      font-size: 24px;
    }
  }
`;

export const Grid = styled.div`
  text-align: center;
  gap: 30px;
  display: grid;
  justify-content: center;
  grid-template-columns: repeat(3, 1fr);

  @media screen and (max-width: 1280px) {
    gap: 30px;
    grid-template-columns: repeat(3, 1fr);
  }
  @media screen and (max-width: 1024px) {
    gap: 15px;
  }
  @media screen and (max-width: 991px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media screen and (max-width: 480px) {
    grid-template-columns: 1fr;
    grid-gap: 40px 0;
  }
`;

export const Article = styled.article`
  transition: all 0.2s ease 0s;
  padding: 0 15px;
  text-align:center;
  :hover {
    transform: translateY(-7px);
  }
  a {
    flex-wrap: wrap;
    color: #09131F;
    font-weight: 700;
    font-size: 15px;
    line-height: 20px;
    letter-spacing: -0.1px;
    margin-top: 10px;
    justify-content: center;
    @media screen and (max-width: 480px) {
      font-size: 14px;
    }
    i {
      line-height: 1;
      margin-left: 5px;
      transform: translateX(0px);
      transition: 0.3s ease 0s;
    }
    &:hover i {
      transform: translateX(3px);
    }
  img {
    border-radius: 20px;
    background: #191919;
    width:100%
    display: inline-flex;
    justify-content: center;
  }
  p {
    font-family: Inter, sans-serif;
    font-weight: 500;
    font-size: 15px;
    line-height: 18px;
    margin: 20px 0 0;
  }
  h4 {
    color:#09131F;
    width:100%;
    text-align: center;
    font-weight: 700;
    font-size: 18px;
    line-height: 1.68;
    margin: 8px 0 0;
    @media screen and (max-width: 1024px) {
      font-size: 17px;
    }
    @media screen and (max-width: 768px) {
      font-size: 16px;
    }
  }
  
  }
`;
