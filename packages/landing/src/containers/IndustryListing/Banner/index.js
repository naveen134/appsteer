import React from "react";
import PropTypes from "prop-types";
import Text from "common/components/Text";
import Image from "common/components/Image";
import NextImage from "common/components/NextImage";
import Heading from "common/components/Heading";
import Container from "common/components/UI/Container";
import { SectionHeader } from "../appsteer-listing.style";
import appsteerlogo from "../../../common/assets/image/Industry/appstreer-listing-logo.png";

import Section, {
  SectionWrapper,
  ThumbWrapper,
  TextWrapper,
  Pattern,
} from "./banner.style";

// import { walletExperience } from "common/data/Appsteerlisting";

const AppsteerlistingBanner = ({ content }) => {
  // const { image, features } = walletExperience;
  const { title, description, bannerimage, bannerimage1 } = content;

  return (
    <Section>
      <Pattern />
      {/* <SectionWrapper> */}
      <Container width="1300px">
        <TextWrapper>
          <SectionHeader className="section-header-two">
            <Heading content={title} />
            <Text content={description} />
          </SectionHeader>
        </TextWrapper>
        <ThumbWrapper>
          <Image src={bannerimage} alt="Wallet Thumbnail" />
          {bannerimage1 && (
            <Image
              src={bannerimage1}
              className="bubble-image-2"
              alt="banner buble image"
            />
          )}
          <Image
            src={appsteerlogo.src}
            className="bubble-image-1"
            alt="Wallet Thumbnail"
          />
        </ThumbWrapper>
      </Container>
      {/* </SectionWrapper> */}
    </Section>
  );
};

AppsteerlistingBanner.propTypes = {
  /** templet content */
  content: PropTypes.object,
};

AppsteerlistingBanner.defaultProps = {
  content: { title: "No data found", description: "No description data found" },
};

export default AppsteerlistingBanner;
