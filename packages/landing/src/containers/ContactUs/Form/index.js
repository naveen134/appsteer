// import ModalContact from "common/components/Modal";
import React, { useState, useEffect, useRef } from "react";
import {
  ModalWrapper,
  FormWrapper,
  ButtonWrapper,
  CombineInput,
} from "./connectModal.style";
// import crossicon from "common/assets/image/appsteer/CrossIcon.svg";
import NextImage from "common/components/NextImage";
import Input from "common/components/Input";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import PrimaryButton from "common/components/Appsteer/primaryButton";
import Button from "common/components/Button";
import { useRouter } from "next/router";
import axios from "axios";
import ReCAPTCHA from "react-google-recaptcha";
import { StyledButton } from "./button.style";
import Spinner from "common/components/Spinner/Spinner";
import CheckBox from "common/components/Checkbox";
import Swal from "sweetalert2";
import Link from 'next/link';

const maxlength = 70;

const ContactForm = ({ open, close }) => {
  const router = useRouter();

  const [inputValues, setInputValue] = useState({
    FirstName: "",
    LastName: "",
    Email: "",
    Number: "",
    Message: "",
  });
  const [loading, setLoading] = useState(false);
  const [formErrors, setFormErrors] = useState({});
  const [isCaptchaChecked, setIsCaptchaChecked] = useState(false);
  const [isPrivacyPolicyChecked, setIsPrivacyPolicyChecked] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [validatePhoneNo, setValidatePhoneNo] = useState(false);

  const nameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
  const Emailregex =
    /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,6}))$/;
  //   const NumberRegex = /^[0]?[789]\d{9}$/;
  const usPhoneNumberRegex = /^\d{3}-\d{3}-\d{4}$|^\(\d{3}\)\d{3}-\d{4}$/;
  const indianPhoneNumberRegex = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;

  //   const [validation, setValidation] = useState({
  //     Name: "",
  //     Email: "",
  //     Number: "",
  //     Message: "",
  //   });

  function handleChange(name, value) {
    // console.log(isValidPhoneNumber(`${value}`), "as");
    setInputValue({ ...inputValues, [name]: value });
  }
  function handleInput(event) {
    event.target.style.height = "auto";
    event.target.style.height = `${event.target.scrollHeight}px`;
  }
  // console.log(inputValues, "as");
  const handleBlur = (name, value) => {
    let errors = formErrors;
    // console.log(value, "as");
    switch (name) {
      case "FirstName": {
        if (value === "") {
          errors.FirstName = "First Name required!";
          setFormErrors({ ...errors });
        } else if (!nameRegex.test(value)) {
          errors.FirstName = "Enter valid First Name!";
          setFormErrors({ ...errors });
        } else {
          errors.FirstName = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      case "LastName": {
        if (value === "") {
          errors.LastName = "Last Name required!";
          setFormErrors({ ...errors });
        } else if (!nameRegex.test(value)) {
          errors.LastName = "Enter valid Last Name!";
          setFormErrors({ ...errors });
        } else {
          errors.LastName = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      case "Email": {
        if (value === "") {
          errors.Email = "Email required!";
          setFormErrors({ ...errors });
        } else if (!Emailregex.test(value)) {
          errors.Email = "Enter valid Email";
          setFormErrors({ ...errors });
        } else {
          errors.Email = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      // case "Company": {
      //   if (value === "") {
      //     errors.Company = "Company Name required!";
      //     setFormErrors({ ...errors });
      //   } else if (!nameRegex.test(value)) {
      //     errors.Company = "Enter valid company";
      //     setFormErrors({ ...errors });
      //   } else {
      //     errors.Company = "";
      //     setFormErrors({ ...errors });
      //   }
      //   return;
      // }
      // case "JobTitle": {
      //   if (value === "") {
      //     errors.JobTitle = "JobTitle required!";
      //     setFormErrors({ ...errors });
      //   } else if (!nameRegex.test(value)) {
      //     errors.JobTitle = "Enter valid JobTitle";
      //     setFormErrors({ ...errors });
      //   } else {
      //     errors.JobTitle = "";
      //     setFormErrors({ ...errors });
      //   }
      //   return;
      // }
      case "Number": {
        if (inputValues?.Number === "") {
          errors.Number = "Number required!";
          setFormErrors({ ...errors });
        } else if (!isValidPhoneNumber(`${inputValues?.Number}`)) {
          errors.Number = "Enter valid Number!";
          setFormErrors({ ...errors });
        } else {
          errors.Number = "";
          setFormErrors({ ...errors });
        }
        return;
      }
      // case "State": {
      //   if (value === "") {
      //     errors.State = "State required!";
      //     setFormErrors({ ...errors });
      //   } else if (!nameRegex.test(value)) {
      //     errors.State = "Enter valid State!";
      //     setFormErrors({ ...errors });
      //   } else {
      //     errors.State = "";
      //     setFormErrors({ ...errors });
      //   }
      //   return;
      // }
      default:
        return;
    }
  };
  const checkValidation = (values) => {
    let errors = {};

    if (!values.FirstName) {
      errors.FirstName = "First Name required!";
    } else if (!nameRegex.test(values.FirstName)) {
      errors.FirstName = "Enter valid FirstName!";
    }
    if (!values.LastName) {
      errors.LastName = "Last Name required!";
    } else if (!nameRegex.test(values.LastName)) {
      errors.LastName = "Enter valid LastName!";
    }
    if (!values.Email) {
      errors.Email = "Email required!";
    } else if (!Emailregex.test(values.Email)) {
      errors.Email = "Enter valid Email!";
    }
    if (!values.Number) {
      errors.Number = "Number required!";
    } else if (!isValidPhoneNumber(values.Number)) {
      errors.Number = "Invalid Number!";
    }
    // if (!values.Message) {
    //   errors.Message = "Please Add message";
    // }
    // if (!values.JobTitle) {
    //   errors.JobTitle = "Job Title required";
    // }
    // if (!values.Company) {
    //   errors.Company = "Company Name required";
    // }
    // if (!values.State) {
    //   errors.State = "State Name required";
    // }
    return errors;
  };
  // useEffect(() => {
  //   // console.log(inputValues.Number);
  //   if (inputValues.Number) console.log(isValidPhoneNumber(inputValues.Number));
  // }, [inputValues.Number]);
  const handleSubmit = async (e) => {
    e.preventDefault();

    const err = checkValidation(inputValues);
    // console.log(err, "as");
    if (Object.keys(err).length > 0) {
      setFormErrors(err);
    } else if (isCaptchaChecked) {
      setLoading(true);
      // console.log("forms", { inputValues }) if (!isCaptchaChecked) ;
      Swal.fire({
        title: "Thank you for your interest",
        icon: "success",
        showCancelButton: false,
        showConfirmButton: false,
      });
      setInputValue({
        FirstName: "",
        LastName: "",
        Email: "",
        Number: "",
        Message: "",
      });
      const postLead = await axios.post(
        `https://qtkbppesld.execute-api.ap-southeast-2.amazonaws.com/qa/createlead`,
        {
          first_name: inputValues?.FirstName,
          last_name: inputValues?.LastName,
          email: inputValues?.Email,
          contact_number: inputValues?.Number,
          message: inputValues?.Message,
        }
      );
      setIsCaptchaChecked(false);
      setIsPrivacyPolicyChecked(false);

      if (postLead.status === 200) {
        // Swal.fire({
        //   title: "Thank you for your interest",
        //   icon: "success",
        //   showCancelButton: false,
        //   showConfirmButton: false,
        // });
        setLoading(false);
      }
    }
  };

  return (
    <FormWrapper inputValue={inputValues}>
      {" "}
      <form onSubmit={handleSubmit}>
        <CombineInput>
          <Input
            label="First Name"
            onChange={(e) => handleChange("FirstName", e)}
            onBlur={(e) => handleBlur("FirstName", e?.target?.value)}
            name="FirstName"
            value={inputValues.FirstName || ""}
            className={
              formErrors.FirstName
                ? "error_input"
                : inputValues.FirstName
                ? "input_value"
                : ""
            }
            icon={formErrors.FirstName}
          />
          <Input
            label="Last Name"
            onChange={(e) => handleChange("LastName", e)}
            onBlur={(e) => handleBlur("LastName", e?.target?.value)}
            name="LastName"
            value={inputValues.LastName || ""}
            className={
              formErrors.LastName
                ? "error_input"
                : inputValues.LastName
                ? "input_value"
                : ""
            }
            icon={formErrors.LastName}
          />
        </CombineInput>

        <CombineInput>
          <Input
            label="Email"
            onChange={(e) => handleChange("Email", e)}
            onBlur={(e) => handleBlur("Email", e?.target?.value)}
            name="Email"
            value={inputValues.Email}
            className={
              formErrors.Email
                ? "error_input"
                : inputValues.Email
                ? "input_value"
                : ""
            }
            icon={formErrors.Email}
          />
          {/* <Input
            label="Phone Number"
            onChange={(e) => handleChange("Number", e)}
            onBlur={(e) => handleBlur("Number", e?.target?.value)}
            className={
              formErrors.Number
                ? "error_input"
                : inputValues.Number
                ? "input_value"
                : ""
            }
            inputType="phone"
            value={inputValues.Number}
            name="Number"
            icon={formErrors.Number}
          /> */}
          <span className="phone-input">
            <PhoneInput
              international={false}
              // defaultCountry="IN"
              className={`contact-phone-input ${
                formErrors.Number ? "ph-err" : ""
              }`}
              placeholder="Enter phone number"
              value={inputValues.Number}
              onChange={(e) => handleChange("Number", e)}
              onBlur={(e) => handleBlur("Number", e?.target?.value)}
            />
            <span className="err">{formErrors.Number}</span>
          </span>
        </CombineInput>
        {/* <CombineInput>
          <Input
            label="Company"
            name="Company"
            onChange={(e) => handleChange("Company", e)}
            onBlur={(e) => handleBlur("Company", e.target.value)}
            value={inputValues.Company}
            className={
              formErrors.Company
                ? "error_input"
                : inputValues.Company
                ? "input_value"
                : ""
            }
            icon={formErrors.Company}
          />
          <Input
            label="Job Title"
            name="JobTitle"
            onChange={(e) => handleChange("JobTitle", e)}
            onBlur={(e) => handleBlur("JobTitle", e.target.value)}
            value={inputValues.JobTitle}
            className={
              formErrors.JobTitle
                ? "error_input"
                : inputValues.JobTitle
                ? "input_value"
                : ""
            }
            icon={formErrors.JobTitle}
          />
        </CombineInput> */}
        {/* <Input
          label="State"
          onChange={(e) => handleChange("State", e)}
          onBlur={(e) => handleBlur("State", e?.target?.value)}
          name="State"
          value={inputValues.State}
          className={
            formErrors.State
              ? "error_input"
              : inputValues.State
              ? "input_value"
              : ""
          }
          icon={formErrors.State}
        /> */}
        <Input
          name="Message"
          label={
            inputValues.Message
              ? `Your message (${
                  maxlength - inputValues.Message.length
                } characters left)`
              : "Your message"
          }
          onChange={(e) => handleChange("Message", e)}
          value={inputValues.Message}
          className={
            formErrors.Message
              ? "error_input"
              : inputValues.Message
              ? "underline input_value"
              : " "
          }
          icon={formErrors.Message}
          inputType="textarea"
          spellcheck="false"
          maxlength={maxlength}
          contenteditable="true"
          autosize
          onInput={handleInput}
        />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginBlock: "20px",
          }}
        >
          <ReCAPTCHA
            sitekey="6LfarnspAAAAAPbEZeizygiytGpk6tOYZHol0DBq"
            onChange={() => setIsCaptchaChecked(true)}
          />
        </div>
        {/* {loading ? (
          <Spinner />
        ) : ( */}
        <div style={{ display: "flex", justifyContent: "center", marginBlock: "20px" }}>
            <CheckBox
              id='appsteerPrivacy'
              labelText=' '
              onChange={() => setIsPrivacyPolicyChecked(!isPrivacyPolicyChecked)}
              value={isPrivacyPolicyChecked}
              checked={isPrivacyPolicyChecked}
              className={
                formErrors.PrivacyPolicy ? "error_input" : inputValues.PrivacyPolicy ? "input_value" : ""
              }
            />
            <span className="sideLinkWrap">
              By clicking on the check box you accept the
              <Link className="hyperlink-color" href="/privacy-policy">
                <span style={{ color: "#FFB47B" }}>&nbsp;privacy policies&nbsp;</span>
              </Link>
              of Appsteer Inc
            </span>
        </div>
        <ButtonWrapper>
          <StyledButton
            type="submit"
            title="Submit"
            onClick={handleSubmit}
            iconPosition="center"
            disabled={!isCaptchaChecked && !isPrivacyPolicyChecked}
            className="contact-us-btn"
          >
            {" "}
            Submit{" "}
          </StyledButton>
        </ButtonWrapper>
        {/* )} */}
      </form>
    </FormWrapper>
  );
};

export default ContactForm;
