import styled from "styled-components";
import themeGet from "@styled-system/theme-get";

export const StyledButton = styled.button`
  display: inline-flex;
  background-color: ${themeGet("colors.primary")};
  color: ${themeGet("colors.black")};
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  font-weight: 500;
  font-size: 20px;
  cursor: pointer;
  line-height: 22px;
  position: relative;
  padding: 19.5px 35px;
  border: none;
  outline: none;
  //   &:hover {
  //     background-color: ${themeGet("colors.primaryHover")};
  //   }
  transition: all 0.4s ease;
  i {
    margin-left: 10px;
    position: relative;
    top: -1px;
  }
  span {
    position: relative;
    display: flex;
  }
  @media (max-width: 468px) {
    padding: 10px 20px;
    font-size: 16px;
  }
`;
