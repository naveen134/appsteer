import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";
import pattern from "common/assets/image/contactUs/mapArrow.png";

const SectionWrapper = styled.section`
  width: 100%;
  padding: 0 0 81px 0;
  // background-color: #1e1e1e;
  @media only screen and (max-width: 1440px) {
    // padding:  0 0 70px 0;
  }
  @media only screen and (max-width: 1360px) {
    padding: 0 0 50px;
  }
  @media only screen and (max-width: 991px) {
    padding: 0 0 40px 0;
  }
  @media only screen and (max-width: 480px) {
    padding: 0;
  }
`;

export const Pattern = styled.div`
  @media only screen and (min-width: 319px) {
    display: none;
    position: absolute;
    width: 297px;
    height: 327px;
    left: 22%;
    top: -52px;
    &::before {
      content: "";
      position: absolute;
    }
    &::before {
      background: url(${pattern?.src}) no-repeat;
      right: 42%;
      top: 50px;
      background-position: right 0;
      width: 100%;
      height: 100%;
      // display: none;

      @media only screen and (min-width: 768px) {
        // background-position: right 0;
        // display: block;
      }
      @media only screen and (min-width: 1024px) {
      }
      @media only screen and (min-width: 1440px) {
      }
      @media only screen and (min-width: 1920px) {
      }
    }
  }
  @media only screen and (min-width: 768px) {
    display: block;
  }
`;

export const SectionHeader = styled.header`
  text-align: center;
  margin-bottom: 68px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  @media only screen and (max-width: 1440px) {
    margin-bottom: 60px;
  }
  @media only screen and (max-width: 991px) {
    margin-bottom: 54px;
  }
  @media only screen and (max-width: 480px) {
    margin-bottom: 0px;
  }

  h2 {
    font-weight: 500;
    font-size: 62px;
    line-height: 73px;
    text-align: center;
    letter-spacing: -0.02em;
    margin-bottom: 27px;
    max-width: 662px;
    @media only screen and (max-width: 1440px) {
      margin-bottom: 15px;
    }

    @media only screen and (max-width: 1024px) {
      font-size: 49px;
      line-height: 64px;
    }
    @media only screen and (max-width: 768px) {
      // font-size: 40px;
      // max-width: 100%;
      // text-align: center;
      max-width: 662px;
    }
    @media only screen and (max-width: 480px) {
      font-size: 35px;
      line-height: 45px;
    }
  }
  p {
    font-size: 16px;
    line-height: 28px;
    color: #496b96;
    max-width: 400px;
    @media only screen and (max-width: 768px) {
      max-width: 100%;
      text-align: center;
    }
  }
  .smallText {
    color: ${themeGet("colors.white", "fff")};
    font-size: 18px;
    line-height: 28px;
    margin-top: 20px;
  }
  .countries {
    display: flex;
    justify-content: space-between;
    width: 430px;
    margin-top: 10px;
    @media only screen and (max-width: 480px) {
      width: 100%;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .countriesSingle {
      display: flex;
      @media only screen and (max-width: 480px) {
        margin-bottom: 15px;
      }
      img {
        width: 24px;
        height: 24px;
      }
      p {
        font-size: 16px;
        line-height: 28px;
        color: #eeeeeee;
        margin: 0;
        padding: 0;
        margin-left: 15px;
      }
    }
  }
`;

export const LocationsMobile = styled.div`
  margin-bottom: 1rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
  @media only screen and (min-width: 480px) {
    display: none;
  }
  .locationMobile {
    background: #ffffff;
    border: 1px solid #191919;
    box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
    padding: 20px 20px 5px 20px;
    border-radius: 30px;
    min-width: 240px;
    font-weight: 500;
    font-size: 24px;
    line-height: 28px;
    text-align: center;
    letter-spacing: -0.02em;
    width: max-content;
    div {
      display: inline-flex;
      align-items: flex-start;
      column-gap: 6px;
    }
  }
`;

export const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  .countryIconImg {
    max-width: 80%;
    margin: auto;
  }
  @media only screen and (max-width: 480px) {
    display: none;
  }
  .countryIcon {
    position: absolute;
    height: 500px;
    width: 100%;

    .countryIconSingle {
      position: absolute;
      cursor: pointer;
      &:hover {
        .location {
          display: block;
        }
      }
      .location {
        display: none;
        position: absolute;
        background: #ffffff;
        border: 1px solid #191919;
        box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
        padding: 20px 20px 5px 20px;
        border-radius: 30px;
        min-width: 240px;
        left: -98px;
        top: -82px;
        z-index: 16;
        font-weight: 500;
        font-size: 24px;
        line-height: 28px;
        text-align: center;
        letter-spacing: -0.02em;
        width: max-content;
        div {
          display: inline-flex;
          align-items: flex-start;
          column-gap: 6px;
        }
        &::before {
          content: "";
          position: absolute;
          display: block;
          width: 20px;
          height: 20px;
          background: #ffffff;
          border-right: 1px solid #191919;
          border-bottom: 1px solid #191919;
          box-shadow: 0px 44px 124px rgb(0 0 0 / 8%);
          left: 0;
          right: 0;
          margin: auto;
          top: 60px;
          transform: rotate(45deg);
        }
      }
      &::before {
        content: "";
        position: absolute;
        display: block;
        width: 40px;
        height: 40px;
        background: ${themeGet("colors.primary")};
        box-shadow: 0 0 0 0.5px ${themeGet("colors.primary")};
        border-radius: 50%;
        top: 50%;
        left: 50%;
        opacity: 0.5;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        animation: pulse 1s ease-out infinite;
        backface-visibility: hidden;
        pointer-events: none;
        z-index: 11;
        &:hover {
        }
      }
      &::after {
        content: "";
        position: absolute;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        -webkit-transition: 0.25s ease-in-out;
        -webkit-transition: 0.25s ease-in-out;
        transition: 0.25s ease-in-out;
        z-index: 2;
      }
      &:nth-child(1) {
        top: 34%;
        position: absolute;
        left: 25%;
        .location {
          left: -131px;
        }
        @media only screen and (max-width: 768px) {
          top: 38%;
          left: 25%;
        }
      }
      &:nth-child(2) {
        top: 25%;
        position: absolute;
        left: 23%;
        @media only screen and (max-width: 768px) {
          top: 32%;
          left: 20%;
        }
        .location {
          left: -109px;
        }
      }
      &:nth-child(3) {
        position: absolute;
        top: 39%;
        left: 20%;
        @media only screen and (max-width: 768px) {
          top: 41%;
          left: 19%;
        }
      }
      &:nth-child(4) {
        position: absolute;
        top: 45%;
        left: 59%;
        @media only screen and (max-width: 768px) {
          left: 58%;
          top: 46%;
        }
      }
      &:nth-child(5) {
        position: absolute;
        top: 54%;
        left: 66%;
        @media only screen and (max-width: 768px) {
          top: 52%;
          left: 65%;
        }
        .location {
          left: -106px;
        }
      }
      &:nth-child(6) {
        position: absolute;
        bottom: 29%;
        right: 21%;
        @media only screen and (max-width: 768px) {
          bottom: 35%;
          right: 19%;
        }
      }
    }
  }
  @keyframes pulse {
    0% {
      transform: translateX(-50%) translateY(-50%) translateZ(0) scale(1);
      opacity: 0.3;
    }

    100% {
      transform: translateX(-50%) translateY(-50%) translateZ(0) scale(2);
      opacity: 0;
    }
  }
`;

export default SectionWrapper;
