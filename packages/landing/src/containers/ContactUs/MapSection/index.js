import React from "react";
import Heading from "common/components/Heading";
import Text from "common/components/Text";
import Image from "common/components/Image";
import Container from "common/components/UI/Container";
import india from "common/assets/image/contactUs/India.svg";
import singapore from "common/assets/image/contactUs/singapore.svg";

import usa from "common/assets/image/contactUs/usa.svg";
import canada from "common/assets/image/contactUs/canada.svg";
import uae from "common/assets/image/contactUs/uae.svg";

import Tada from "react-reveal/Fade";
import SectionWrapper, {
  SectionHeader,
  ImageWrapper,
  Pattern,
  LocationsMobile,
} from "./mapSection.style";

import mapImage from "common/assets/image/contactUs/world-map.png";
// import country1 from 'common/assets/image/cryptoModern/uk.png';
import country2 from "common/assets/image/cryptoModern/africa.png";
// import country3 from 'common/assets/image/cryptoModern/spain.png';
import countryIcon from "common/assets/image/contactUs/countryIcon.png";

const MapSection = () => {
  return (
    <SectionWrapper id="map">
      <Container width="1200px">
        <Pattern />
        <SectionHeader>
          <Heading content="We have offices all over the world" />
          {/* <div className="countries">
            <div className="countriesSingle">
              <Image src={?.src} alt="country image" />
              <Text className="smallText" content="United Kingdom" />
            </div>
            <div className="countriesSingle">
              <Text className="smallText" content="South Africa" />
            </div>
            <div className="countriesSingle">
              <Image src={country3?.src} alt="country image" />
              <Text className="smallText" content="Spain" />
            </div>
          </div> */}
          <LocationsMobile>
            <Tada>
              <div className="locationMobile" style={{display: 'none', visibility: 'hidden' }}>
                <div>
                  <Text className="" content="Washington DC, USA" />
                  <Image src={usa.src} alt="country image" />
                </div>
              </div>
            </Tada>
            <Tada>
              <div className="locationMobile">
                <div>
                  <Text className="" content="Toronto, Canada" />
                  <Image src={canada?.src} alt="country image" />
                </div>
              </div>
            </Tada>
            <Tada>
              <div className="locationMobile">
                <div>
                  <Text className="" content="Austin, USA" />
                  <Image src={usa.src} alt="country image" />
                </div>
              </div>
            </Tada>
            <Tada>
              <div className="locationMobile">
                <div>
                  <Text className="" content="Dubai, UAE" />
                  <Image src={uae?.src} alt="country image" />
                </div>
              </div>
            </Tada>
            <Tada>
              <div className="locationMobile">
                <div>
                  <Text className="" content="Bengaluru, India" />
                  <Image src={india?.src} alt="country image" />
                </div>
              </div>
            </Tada>
            <Tada>
              <div className="locationMobile">
                <div>
                  <Text className="" content="Singapore" />
                  <Image src={singapore?.src} alt="country image" />
                </div>
              </div>
            </Tada>
          </LocationsMobile>
        </SectionHeader>
        <ImageWrapper>
          <Image className="mainImg" src={mapImage?.src} alt="Map Image" />
          <div className="countryIcon">
            <div className="countryIconSingle" style={{display: 'none', visibility: 'hidden' }}>
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Washington DC, USA" />
                    <Image src={usa.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
            <div className="countryIconSingle">
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Toronto, Canada" />
                    <Image src={canada?.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
            <div className="countryIconSingle">
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Austin, USA" />
                    <Image src={usa.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
            <div className="countryIconSingle">
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Dubai, UAE" />
                    <Image src={uae?.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
            <div className="countryIconSingle">
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Bengaluru, India" />
                    <Image src={india?.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
            <div className="countryIconSingle">
              <Image
                className="countryIconImg"
                src={countryIcon?.src}
                alt="Country Icon"
              />
              <Tada>
                <div className="location">
                  <div>
                    <Text className="" content="Singapore" />
                    <Image src={singapore?.src} alt="country image" />
                  </div>
                </div>
              </Tada>
            </div>
          </div>
        </ImageWrapper>
      </Container>
    </SectionWrapper>
  );
};

export default MapSection;
