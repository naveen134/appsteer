import Container from "common/components/UI/Container";
import React from "react";
import { Section, SuccessText } from "./successBanner.style";

const SuccessBanner = () => {
  return (
    <Section>
      <Container>
        <SuccessText>
          Thank you for connecting with us. Our team will get in touch with you
          shortly
        </SuccessText>
      </Container>
    </Section>
  );
};

export default SuccessBanner;
