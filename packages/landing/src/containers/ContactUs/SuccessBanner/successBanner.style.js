import styled from "styled-components";

export const Section = styled.section`
  padding: 8rem 0 5rem;
  overflow: hidden;
  position: inherit;
  // background-color: #e5e5e5;
  @media (max-width: 1600px) {
    padding: 8rem 0 5rem;
  }
  @media (max-width: 1440px) {
    padding: 8rem 0 5rem;
  }
  @media (max-width: 768px) {
    padding: 8rem 0 5rem;
  }
  @media (max-width: 480px) {
    padding: 8rem 0 0;
  }
  > div.container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    @media only screen and (max-width: 768px) {
      flex-direction: column;
      padding: 50px 0;
    }
  }
`;

export const SuccessText = styled.h1`
  font-weight: 500;
  font-size: 44px;
  line-height: 67px;
  text-align: center;
  margin: 0;
`;
