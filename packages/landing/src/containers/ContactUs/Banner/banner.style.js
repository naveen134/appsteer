import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";
import pattern from "common/assets/image/contactUs/contactUsPattern.png";
import pattern2 from "common/assets/image/appsteer/stats-pattern2.png";
// import pattern2 from 'common/assets/image/webAppMinimal/banner-pattern-2.png';

const SectionWrapper = styled.section`
  padding: 15rem 0 5rem;
  overflow: hidden;
  position: inherit;
  // background-color: #e5e5e5;
  @media (max-width: 1600px) {
    padding: 15rem 0 5rem;
  }
  @media (max-width: 1440px) {
    padding: 17rem 0 5rem;
  }
  @media (max-width: 768px) {
    padding: 5rem 0 5rem;
  }
  @media (max-width: 480px) {
    padding: 5rem 0 0;
  }
  > div.container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    @media only screen and (max-width: 768px) {
      flex-direction: column;
      padding: 50px 0;
    }
  }
`;

export const Pattern = styled.div`
  @media only screen and (max-width: 768px) {
    display: none;
  }

  @media only screen and (min-width: 319px) {
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    &::before,
    &::after {
      content: "";
      position: absolute;
    }
    &::after {
      background: url(${pattern2?.src}) no-repeat;
      width: 440px;
      height: 440px;
      bottom: -490px;
      opacity: 0.7;
      right: 0;
      // height: 811px;
      @media only screen and (min-width: 768px) {
        // background-size: 30%;
      }
      @media only screen and (min-width: 1024px) {
        background-size: 58%;
        background-position: right;
      }
      @media only screen and (min-width: 1280px) {
        background-size: 93%;
      }
      @media only screen and (min-width: 1440px) {
        // background-size: 70%;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 58%;
        background-position: right;
        bottom: 10%;
        right: 25%;
      }
    }
    &::before {
      background: url(${pattern?.src}) no-repeat;
      right: 42%;

      top: 50px;
      background-position: right 0;
      width: 100%;
      height: 100%;

      @media only screen and (min-width: 768px) {
        // background-position: right 0;
      }
      @media only screen and (min-width: 1024px) {
        // background-size: 80%;
      }
      @media only screen and (min-width: 1440px) {
      }
      @media only screen and (min-width: 1920px) {
      }
    }
  }
`;

export const ThumbWrapper = styled.div`
  position: relative;
  @media only screen and (max-width: 768px) {
    width: 70%;
    margin-bottom: 50px;
  }
  @media only screen and (max-width: 1024px) {
    width: 100%;
  }
  @media only screen and (max-width: 480px) {
    width: fit-content;
  }
  @keyframes bubbleLeftRight {
    0% {
      transform: translateX(0px);
    }
    25% {
      transform: translateX(10px);
    }
    50% {
      transform: translateX(15px);
    }
    75% {
      transform: translateX(5px);
    }
    100% {
      transform: translateX(0px);
    }
  }
  @keyframes bubbleTopBottom {
    0% {
      transform: translateY(0px);
    }
    25% {
      transform: translateY(5px);
    }
    50% {
      transform: translateY(15px);
    }
    75% {
      transform: translateY(5px);
    }
    100% {
      transform: translateY(0px);
    }
  }
  .bubble-image-1 {
    position: absolute;
    top: -9px;
    right: 5px;
    animation: bubbleTopBottom 5s linear infinite;
    @media (max-width: 991px) {
      display: block;
      width: 35%;
    }
  }
  .bubble-image-2 {
    position: absolute;
    top: 180px;
    left: -190;
    animation: bubbleLeftRight 3s linear infinite;
    @media (max-width: 1600px) {
      left: -45px;
      width: 41%;
      top: 149;
    }
    @media (max-width: 991px) {
      width: 49%;
      left: -13px;
    }
    @media (max-width: 768px) {
      width: 34%;
      left: 1px;
    }
  }
  .bubble-image-3 {
    position: absolute;
    bottom: 85px;
    left: -115px;
    animation: bubbleLeftRight 1s linear infinite;
    @media (max-width: 1600px) {
      left: -80px;
    }
    @media (max-width: 991px) {
      display: none;
    }
  }
`;

export const TextWrapper = styled.div`
  width: 510px;
  @media only screen and (max-width: 1219px) {
    width: 640px;
  }
  @media only screen and (max-width: 1024px) {
    width: 100%;
    // margin-left: auto;
    // margin-right: auto;
  }
  @media only screen and (max-width: 624px) {
    width: 100%;
  }
  .section-header-two {
    h2{
      text-align: left;
      font-weight: 500;
      font-size: 74px;
      line-height: 97px;

      @media (min-width: 319px) {
        font-size: 38px;
        line-height: 47px;
        padding:0px 20px;
        text-align:center;
      }

      @media (min-width: 768px) {
        font-size: 52px;
        line-height: 68px;
        padding:0px 20px 0px;
        text-align:center;
      }

      @media (min-width: 1024px) {
        font-size: 52px;
        line-height: 68px;
        padding:0px 0px;
        text-align:left;
      }

      @media (min-width: 1280px) {
        font-weight: 500;
        font-size: 74px;
        line-height: 97px;
        text-align:left;
      }
      
      @media (min-width: 1440px) {
        font-weight: 500;
        font-size: 74px;
        line-height: 97px;
        text-align:left;
      }
    }
    p{
      font-weight: 400;
      font-size: 15px;
      line-height: 24px;
      padding:0px 20px;
      @media (min-width: 426px) {
        font-size: 18px;
        line-height: 25px;
        padding:0px 20px;
        text-align:center;
      }
      @media (min-width: 769px) {
        font-size: 20px;
        line-height: 36px;
        padding:0px 20px;
        text-align:center;
      }
      @media (min-width: 1024px) {
      
        font-weight: 400;
      font-size: 20px;
      line-height: 36px;

        padding:0px 50px 0px 0px;
        text-align:left;
      }
    }
    @media (max-width: 769px) {
      margin-left: 0;
      max-width:700px;
    }
  }

 
  
    .content__wrapper {
      padding-left: 20px;
      h3 {
        color: #666666;
        font-size: 18px;
        line-height: 1.45;
        font-weight: 700;
        margin-bottom: 12px;
        @media (max-width: 1600px) {
          font-size: 25px;
        }
      }
      p {
        color: #666666;
        font-size: 15px;
        line-height: 2;
        @media (max-width: 1600px) {
          font-size: 14px;
        }
      }
    }
    &:last-of-type {
      .content__wrapper {
        h3::after {
          content: 'Coming Soon';
          text-transform: uppercase;
          background-color: #108aff;
          border-radius: 6px;
          font-size: 11px;
          font-weight: 700;
          color: #fff;
          padding: 4px 7px;
          margin-left: 10px;
          @media (max-width: 1199px) {
            display: block;
            width: 95px;
            margin-left: 0;
            margin-top: 5px;
          }
        }
      }
    }
  }
`;

export const ContactUSFormWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
`;

export default SectionWrapper;
