import React from "react";
import PropTypes from "prop-types";
import Text from "common/components/Text";
import Image from "common/components/Image";
import NextImage from "common/components/NextImage";
import Heading from "common/components/Heading";
import Container from "common/components/UI/Container";
import { SectionHeader } from "../contactUs.style";
import appsteerlogo from "../../../common/assets/image/Industry/appstreer-listing-logo.png";
import headphoneImage from "common/assets/image/contactUs/headphoneImage.png";
import ContactForm from "containers/ContactUs/Form";

import Section, {
  SectionWrapper,
  ThumbWrapper,
  TextWrapper,
  Pattern,
  ContactUSFormWrapper,
} from "./banner.style";

// import { walletExperience } from "common/data/Appsteerlisting";

const AppsteerlistingBanner = ({ content }) => {
  // const { image, features } = walletExperience;
  const { title, description } = content;

  return (
    <Section>
      {/* <SectionWrapper> */}
      <Pattern />
      <Container contactUs={true}>
        <TextWrapper>
          <SectionHeader className="section-header-two">
            <Heading content={title} />
            <Text content={description} />
            <Image src={headphoneImage.src} className="" alt="headphoneImage" />
          </SectionHeader>
        </TextWrapper>
        <ThumbWrapper>
          <ContactUSFormWrapper>
            <ContactForm />
          </ContactUSFormWrapper>

          {/* <Image src={bannerimage} alt="Wallet Thumbnail" />
          {bannerimage1 && (
            <Image
              src={bannerimage1}
              className="bubble-image-2"
              alt="banner buble image"
            />
          )}
          <Image
            src={appsteerlogo.src}
            className="bubble-image-1"
            alt="Wallet Thumbnail"
          /> */}
        </ThumbWrapper>
      </Container>
      {/* </SectionWrapper> */}
    </Section>
  );
};

AppsteerlistingBanner.propTypes = {
  /** templet content */
  content: PropTypes.object,
};

AppsteerlistingBanner.defaultProps = {
  content: { title: "No data found", description: "No description data found" },
};

export default AppsteerlistingBanner;
