import React from "react";
import { Icon } from "react-icons-kit";
import Fade from "react-reveal/Fade";
import Image from "common/components/Image";
import { arrowRight } from "react-icons-kit/feather/arrowRight";
import Container from "common/components/UI/Container";
import Heading from "common/components/Heading";
import NextImage from "common/components/NextImage";
import Text from "common/components/Text";
import Link from "common/components/Link";

import { Section, SectionHeading, Grid, Article } from "./Usecase.style";

const Usecases = ({ content }) => {
  // console.log(Usecases)
  return (
    <Section id="industries">
      <Container width="1300px">
        <SectionHeading>
          <Heading content="Industries" />
        </SectionHeading>
        <Grid>
          {content &&
            content?.map((eachIndustry, index) => (
              <Fade key={index} up delay={index}>
                <Article>
                  <Link href={eachIndustry?.Link} key={index}>
                    <div>
                      <Image
                        src={eachIndustry?.feturedImage}
                        alt="use list image"
                      />
                    </div>

                    <Heading as="h4" content={eachIndustry?.Title} />
                  </Link>
                </Article>
              </Fade>
            ))}
        </Grid>
      </Container>
    </Section>
  );
};

export default Usecases;
