import React from "react";
import Container from "common/components/UI/Container";
import Heading from "common/components/Heading";
import Text from "common/components/Text";
import NextImage from "common/components/NextImage";
import Link from "next/link";
import Section, {
  Pattern,
  BannerContentWrapper,
  BannerContent,
  Figure,
} from "./banner.style";
import AnchorLink from "react-anchor-link-smooth-scroll";
// import dashboard from 'common/assets/image/webAppMinimal/dashboard.png';

const Banner = () => {
  return (
    <Section id="banner">
      <Pattern />
      <Container width="1400px">
        <BannerContentWrapper>
          <BannerContent>
            <Heading
              className="animate__animated animate__fadeInUp"
              content="A world of industries at your fingertips"
            />
            <Text
              className="animate__animated animate__fadeInUp"
              content="Unleash the power of Zero Code and Configurable Templates!
              No matter what Vertical you are in, which Horizontal your need is, we help it all! 
              Start AppSteer'ing today!"
            />
            <AnchorLink href="#industries" offset={0}>
              <button className="button">
                <span>{"View Industries"}</span>
              </button>
            </AnchorLink>
            {/* <Link href={"#industries"}></Link> */}
          </BannerContent>
        </BannerContentWrapper>
      </Container>
    </Section>
  );
};

export default Banner;
