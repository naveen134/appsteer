import React, { Fragment } from "react";
import Fade from "react-reveal/Fade";
import Text from "common/components/Text";
import Heading from "common/components/Heading";
import Button from "common/components/Button";
import NextImage from "common/components/NextImage";
import Container from "common/components/UI/Container";
import Rating from "common/components/Rating";
import GlideCarousel from "common/components/GlideCarousel";
import GlideSlide from "common/components/GlideCarousel/glideSlide";
// import { SectionHeader } from "../solutions.styles";
import SectionWrapper, { CarouselWrapper, Article } from "./related.style";
import leftArrow from "common/assets/image/appsteer/testimonial-arrow.png";
import rightArrow from "common/assets/image/appsteer/right-arrow.png";

import Image from "common/components/Image";
import Link from "common/components/Link";
import LookingForMore from "containers/Solutions/LookingForMore";

import {
  HeadingWrapper,
  ColoredText,
  SolutionContainerWrapper,
  SectionHeader,
} from "../solutions.styles";

// import { testimonial } from 'common/data/AppClassic';

const RelatedUseCase = ({ data }) => {
  const { title, relatedUsecases } = data;
  console.log("as", relatedUsecases.length);

  const glideOptions = {
    type: "slider",
    gap: 0,
    autoplay: 1000,
    rewind: true,
    bound: true,
    perView: 3,
    animationDuration: 700,
    breakpoints: {
      991: {
        perView: 2,
      },
      426: {
        perView: 1,
      },
    },
  };

  return (
    <SectionWrapper id="testimonial">
      <Container noGutter>
        {relatedUsecases?.length < 4 ? (
          <>
            {" "}
            <CarouselWrapper>
              <Fade up delay={100}>
                <GlideCarousel
                  options={glideOptions}
                  className="custom_glide_control"
                  nextButton={
                    <button
                      class="glide__arrow glide__arrow--right right"
                      data-glide-dir=">"
                    >
                      <Image src={rightArrow.src}></Image>
                    </button>
                  }
                  prevButton={
                    <button
                      class="glide__arrow glide__arrow--left left"
                      data-glide-dir="<"
                      aria-label="Next"
                      variant="fab"
                    >
                      <Image src={leftArrow.src}></Image>
                    </button>
                  }
                >
                  <Fragment>
                    {relatedUsecases?.map((item, index) => (
                      <GlideSlide key={`testimonial--key${item.id}`}>
                        <Article>
                          <Link href={item.slug} key={index}>
                            <Image src={item.image} alt="use list image" />
                            {/* <Text content={post.date} /> */}
                            <Heading as="h4" content={item.title} />
                          </Link>
                        </Article>
                      </GlideSlide>
                    ))}
                  </Fragment>
                </GlideCarousel>
              </Fade>
            </CarouselWrapper>
          </>
        ) : (
          <>
            {" "}
            <CarouselWrapper>
              <Fade up delay={100}>
                <GlideCarousel
                  options={glideOptions}
                  nextButton={
                    <button
                      class="glide__arrow glide__arrow--right right"
                      data-glide-dir=">"
                    >
                      <Image src={rightArrow.src}></Image>
                    </button>
                  }
                  prevButton={
                    <button
                      class="glide__arrow glide__arrow--left left"
                      data-glide-dir="<"
                      aria-label="Next"
                      variant="fab"
                    >
                      <Image src={leftArrow.src}></Image>
                    </button>
                  }
                >
                  <Fragment>
                    {relatedUsecases?.map((item, index) => (
                      <GlideSlide key={`testimonial--key${item.id}`}>
                        <Article>
                          <Link href={item.slug} key={index}>
                            <Image src={item.image} alt="use list image" />
                            {/* <Text content={post.date} /> */}
                            <Heading as="h4" content={item.title} />
                          </Link>
                        </Article>
                      </GlideSlide>
                    ))}
                  </Fragment>
                </GlideCarousel>
              </Fade>
            </CarouselWrapper>
          </>
        )}
      </Container>
    </SectionWrapper>
  );
};

export default RelatedUseCase;

// import React, { Fragment } from "react";
// import Fade from "react-reveal/Fade";
// import Text from "common/components/Text";
// import Heading from "common/components/Heading";
// import Button from "common/components/Button";
// import NextImage from "common/components/NextImage";
// import Container from "common/components/UI/Container";
// import Rating from "common/components/Rating";
// import GlideCarousel from "common/components/GlideCarousel";
// import GlideSlide from "common/components/GlideCarousel/glideSlide";
// // import { SectionHeader } from "../solutions.styles";
// import SectionWrapper, { CarouselWrapper, Article } from "./related.style";
// import leftArrow from "common/assets/image/appsteer/testimonial-arrow.png";
// import rightArrow from "common/assets/image/appsteer/right-arrow.png";

// import Image from "common/components/Image";
// import Link from "common/components/Link";
// import LookingForMore from "containers/Solutions/LookingForMore";

// import {
//   HeadingWrapper,
//   ColoredText,
//   SolutionContainerWrapper,
//   SectionHeader,
// } from "../solutions.styles";

// // import { testimonial } from 'common/data/AppClassic';

// const RelatedUseCase = ({ data }) => {
//   const { title, relatedUsecases } = data;
//   console.log("as", relatedUsecases.length);

//   const glideOptions = {
//     type: "slider",
//     gap: 0,
//     autoplay: 1000,
//     rewind: true,
//     bound: true,
//     perView: 3,
//     animationDuration: 700,
//     breakpoints: {
//       991: {
//         perView: 2,
//       },
//       426: {
//         perView: 1,
//       },
//     },
//   };

//   return (
//     <SectionWrapper id="testimonial">
//       <Container noGutter>
//         {/* <SectionHeader>
//         <Fade up>
//           <HeadingWrapper>
//             <span>Thanks! Here are some </span>
//             <ColoredText>solutions</ColoredText>
//           </HeadingWrapper>
//         </Fade>
//       </SectionHeader> */}
//         {/* <SolutionContainerWrapper> */}
//         <CarouselWrapper>
//           <Fade up delay={100}>
//             <GlideCarousel
//               options={glideOptions}
//               nextButton={
//                 <button
//                   class="glide__arrow glide__arrow--right right"
//                   data-glide-dir=">"
//                 >
//                   <Image src={rightArrow.src}></Image>
//                 </button>
//               }
//               prevButton={
//                 <button
//                   class="glide__arrow glide__arrow--left left"
//                   data-glide-dir="<"
//                   aria-label="Next"
//                   variant="fab"
//                 >
//                   <Image src={leftArrow.src}></Image>
//                 </button>
//               }
//             >
//               <Fragment>
//                 {relatedUsecases?.length < 4 ? (
//                   <>
//                     {relatedUsecases?.map((item, index) => (
//                       <GlideSlide key={`testimonial--key${item.id}`}>
//                         <Article>
//                           <Link href={item.slug} key={index}>
//                             <Image src={item.image} alt="use list image" />
//                             {/* <Text content={post.date} /> */}
//                             <Heading as="h4" content={item.title} />
//                           </Link>
//                         </Article>
//                       </GlideSlide>
//                     ))}
//                   </>
//                 ) : (
//                   <>
//                     {relatedUsecases?.map((item, index) => (
//                       <GlideSlide key={`testimonial--key${item.id}`}>
//                         <Article>
//                           <Link href={item.slug} key={index}>
//                             <Image src={item.image} alt="use list image" />
//                             {/* <Text content={post.date} /> */}
//                             <Heading as="h4" content={item.title} />
//                           </Link>
//                         </Article>
//                       </GlideSlide>
//                     ))}
//                   </>
//                 )}
//                 {/* {relatedUsecases?.length > 0 &&
//                    relatedUsecases?.map((item, index) => (
//                    <GlideSlide key={`testimonial--key${item.id}`}>
//                       <Article>
//                        <Link href={item.slug} key={index}>
//                            <Image src={item.image} alt="use list image" />
//                           <Heading as="h4" content={item.title} />
//                         </Link>
//                        </Article>
//                  </GlideSlide>
//               ))} */}
//               </Fragment>
//             </GlideCarousel>
//           </Fade>
//         </CarouselWrapper>
//       </Container>
//     </SectionWrapper>
//   );
// };

// export default RelatedUseCase;
