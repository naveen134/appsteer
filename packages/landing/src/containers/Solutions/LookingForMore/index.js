import React from "react";
import Fade from "react-reveal/Fade";
import Heading from "common/components/Heading";
import Container from "common/components/UI/Container";
// import { SectionHeader } from "../usecase.style";
// import AnchorLink from "react-anchor-link-smooth-scroll";
import Text from "common/components/Text";
import Link from "next/link";

import SectionWrapper, { SectionHeader } from "./lookingForMore.style";
import {
  HeadingWrapper,
  ColoredText,
  ContentWrapper,
} from "../solutions.styles";

// import { testimonial } from 'common/data/AppClassic';

const RelatedUseCase = () => {
  return (
    <SectionWrapper id="testimonial">
      <Container>
        <SectionHeader>
          <Fade up>
            {/* <Heading as="h5" content={slogan} /> */}
            <HeadingWrapper>
              <span>Looking for </span> <ColoredText>More?</ColoredText>{" "}
            </HeadingWrapper>
            <ContentWrapper>
              <Text
                className="animate__animated animate__fadeInUp"
                content="Our team of expert advisors are here to assist you in all your business endeavours"
              />
              <Link href="/contact-us">
                <button className="button">
                  <span>{"Let’s Connect"}</span>
                </button>
              </Link>

              {/* <div>{"Go back"}</div> */}
            </ContentWrapper>
          </Fade>
        </SectionHeader>
      </Container>
    </SectionWrapper>
  );
};

export default RelatedUseCase;
