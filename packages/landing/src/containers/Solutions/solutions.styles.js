import styled, { createGlobalStyle } from "styled-components";
import { themeGet } from "@styled-system/theme-get";
import { style } from "styled-system";

export const SectionHeader = styled.header`
  //   max-width: 961px;
  width: 100%;
  // margin: 5rem auto 58px;
  text-align: center;
  @media only screen and (max-width: 991px) {
    margin-bottom: 50px;
  }
  h5 {
    font-size: 14px;
    font-weight: 700;
    line-height: 24px;
    margin-bottom: 12px;
    letter-spacing: 1.5px;
    color: ${themeGet("colors.primary", "#2563FF")};
    text-transform: uppercase;
    @media only screen and (max-width: 991px) {
      font-size: 13px;
      margin-bottom: 10px;
    }
  }
  h2 {
    font-weight: 600;
    font-size: 62px;
    line-height: 74px;
    margin-bottom: 4rem;
    color: ${themeGet("colors.headingColor", "#0F2137")};
    margin: 0;
    letter-spacing: -1px;
    @media only screen and (max-width: 1366px) {
      // font-size: 28px;
      // letter-spacing: -0.7px;
    }
    @media only screen and (max-width: 1025px) {
      font-size: 38px;
      line-height: 50px;
    }
    @media only screen and (max-width: 991px) {
    }
  }
`;

export const SolutionsStyleWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
  margin-bottom: 5rem;
  max-width: 980px;
  margin-inline: auto;
`;

export const ColoredText = styled.span`
  color: #f27b31;
`;

export const HeadingWrapper = styled.h2`
  font-weight: 600;
  font-size: 62px;
  line-height: 74px;

  color: ${themeGet("colors.headingColor", "#0F2137")};
  margin: 0;
  letter-spacing: -1px;
  display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}
  @media only screen and (max-width: 1366px) {
    // font-size: 28px;
    // letter-spacing: -0.7px;
  }
  @media only screen and (max-width: 1025px) {
    font-size: 38px;
    line-height: 50px;
  }
  @media only screen and (max-width: 991px) {
  }
  .subHeading_content {
    padding-top: 20px;
    text-align: center;
    font-weight: 500;
    font-size: 24px;
    line-height: 40px;
    color: #666666;
    margin: 0;
    letter-spacing: 1px;
    width:80%
  }
  .custom_form{
    width:80%;
    margin-top: 40px;
  }
`;

export const ContentWrapper = styled.div`
  p {
    font-weight: 400;
    font-size: 20px;
    line-height: 36px;
    text-align: center;
    letter-spacing: -0.02em;
    color: #666666;
    max-width: 679px;
    margin: 0 auto;
    margin-bottom: 58px;
    margin-top: 58px;

    @media (max-width: 1366px) {
      max-width: 550px;
      font-size: 18px;
      line-height: 39px;
    }

    @media (max-width: 768px) {
      font-size: 16px;
      line-height: 33px;
      margin-bottom: 39px;
    }

    @media (max-width: 480px) {
      font-size: 14px;
      line-height: 2;
    }
  }
  .button {
    display: inline-flex;
    background-color: ${themeGet("colors.primary")};
    color: ${themeGet("colors.black")};
    align-items: center;
    justify-content: center;
    border-radius: 30px;
    font-weight: 500;
    font-size: 20px;
    line-height: 22px;
    position: relative;
    padding: 19.5px 70px;
    cursor: pointer;
    border: 0;
    &:hover {
      transform: translateY(-7px);
    }
    transition: all 0.4s ease;
    i {
      margin-left: 10px;
      position: relative;
      top: -1px;
    }
    span {
      position: relative;
      display: flex;
    }
    @media (max-width: 767px) {
      padding: 16px 50px;
      font-size: 20px;
    }
  }
`;
export const SolutionContainerWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 44px 124px rgba(0, 0, 0, 0.08);
  border-radius: 40px;
`;
