import React from "react";
import Container from "common/components/UI/Container";
import NextImage from "common/components/NextImage";
import Text from "common/components/Text";
import Heading from "common/components/Heading";
import Link from "next/link";
import Section, {
  Title,
  SliderWrapper,
  Figure,
  Description,
  TextWrappr,
  ButtonWrapper,
  StickyFooterWrapper,
} from "./stickyFooter.style";

const StickyFooter = () => {
  return (
    <Section>
      <Container width="1300px">
        <StickyFooterWrapper>
          <TextWrappr>
            <Title>
              <Heading as="h2" content={"Ready to get started"} />
            </Title>
            <Description>
              <Text content="Let us help you achieve your dreams" />
            </Description>
          </TextWrappr>
          <ButtonWrapper>
            <Link href="/contact-us">
              <a className="button">
                <span>
                  Connect with us
                  {/* <Icon icon={androidArrowForward} size={16} /> */}
                </span>
              </a>
            </Link>
          </ButtonWrapper>
        </StickyFooterWrapper>
      </Container>
    </Section>
  );
};

export default StickyFooter;
