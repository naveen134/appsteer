import Box from "common/components/Box";
import Heading from "common/components/Heading";
import Text from "common/components/Text";
import NextImage from "common/components/NextImage";
import Container from "common/components/UI/Container";
import { statsCounter } from "common/data/Appsteer";
import Link from "next/link";
import { Icon } from "react-icons-kit";
import { androidArrowForward } from "react-icons-kit/ionicons/androidArrowForward";
import LogoIcon from "../../../common/assets/image/appsteer/logo-stats.png";
import Section, { Grid, Pattern } from "./statsCounter.style";

const StatsCounter = ({stats}) => {
  const { blockTitle, posts } = stats;
  const { subtitle, title, text, button } = blockTitle;
  return (
    <Section>
      <Pattern />
      <Container width="1300px">
        <Grid>
          <Box className="blockTitle">
            {/* <Text as="span" className="subtitle" content={subtitle} /> */}
            <Heading as="h2" content={title} />
            <Text as="p" content={text} />
            <Link href={button.link}>
              <a className="button">
                <span>
                  {button.label}
                  {/* <Icon icon={androidArrowForward} size={16} /> */}
                </span>
              </a>
            </Link>
          </Box>
          <Box className="postsWrap">
            {posts.map(({ StatsValue, StatsName, title, symbol }, index) => (
              <div className="post-background">
                <Box className="post" key={`counter-post-key-${index}`}>
                  <Text as="p" content={title} />
                  <Box className="postCount">
                    <Heading as="h3" content={StatsValue} />
                    {/* <Text as="span" content={symbol} /> */}
                    <div>
                      <NextImage src={LogoIcon} alt="appsteer" />
                    </div>
                  </Box>
                  <Text as="p" content={StatsName} />
                </Box>
              </div>
            ))}
          </Box>
        </Grid>
      </Container>
    </Section>
  );
};

export default StatsCounter;
