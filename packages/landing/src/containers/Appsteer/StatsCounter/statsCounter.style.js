import { themeGet } from "@styled-system/theme-get";
import styled from "styled-components";

import pattern1 from "common/assets/image/appsteer/pattern-stats1.png";

import pattern2 from "common/assets/image/appsteer/stats-pattern2.png";

const Section = styled.section`
  padding-top: 320px;
  padding-bottom: 100px;
  @media only screen and (max-width: 1536px) {
    padding-top: 160px;
  }
  @media only screen and (max-width: 1440px) {
    padding-top: 140px;
  }
  @media only screen and (max-width: 1366px) {
    padding-top: 170px;
  }
  @media only screen and (max-width: 1280px) {
    padding-top: 160px;
  }
  @media only screen and (max-width: 820px) {
    padding-top: 140px;
  }
  @media only screen and (max-width: 768px) {
    padding-top: 110px;
  }
  @media only screen and (max-width: 667px) {
    padding-top: 60px;
  }

  .blockTitle {
    flex: 0 0 50%;
    @media (max-width: 991px) {
      flex: 0 0 100%;
      padding-bottom: 50px;
      max-width: 75%;
      margin-left: auto;
      margin-right: auto;
      text-align: center;
    }
    @media (max-width: 425px) {
      max-width: 100%;
      text-align: center;
    }

    .subtitle {
      color: ${themeGet("colors.secondary")};
      font-family: 'Work Sans', sans-serif;
      font-weight: 700;
      font-size: 28px;
      color: ${themeGet("colors.secondary")};
      line-height: 35px;
      letter-spacing: -0.02em;
      margin-bottom: 15px;
      display: block;
      @media (max-width: 480px) {
        font-size: 22px;
        margin-bottom: 6px;
      }
    }
    h2 {
      font-weight: 600;
      font-size: 62px;
      line-height: 74px;
      letter-spacing: -0.02em;
      margin: 0;
      @media (max-width: 1600px) {
        // font-size: 32px;
        // line-height: 1.41;
        max-width: 512px;
      }
      @media only screen and (max-width: 1536px) {
        max-width: 543px;
      }
      @media only screen and (max-width: 991px) {
        font-size: 62px;
      }
      @media (max-width: 768px) {
        max-width: 100%;
      }
      @media only screen and (max-width: 667px) {
        font-size: 42px;
        line-height: 1.5;
      }
    }
    p {
      margin: 0;
      font-weight: 400;
      font-size: 24px;
      line-height: 38px;
      margin-top: 30px;
      margin-bottom: 30px;
      @media (max-width: 1600px) {
        // font-size: 15px;
        // line-height: 1.87;
        max-width: 617px;
      }
      @media (max-width: 991px) {
        max-width: 100%;
      }
    }
    .button {
      display: inline-flex;
      background-color: ${themeGet("colors.primary")};
      color: ${themeGet("colors.black")};
      align-items: center;
      justify-content: center;
      border-radius: 30px;
      font-weight: 500;
      font-size: 20px;
      line-height: 22px;
      position: relative;
      padding: 19.5px 70px;
      &:hover {
        background-color: ${themeGet("colors.primaryHover")};
      }
      transition: all 0.4s ease;
      i {
        margin-left: 10px;
        position: relative;
        top: -1px;
      }
      span {
        position: relative;
        display: flex;
      }
      @media (max-width: 767px) {
        padding: 16px 50px;
        font-size: 20px;
      }
    }
  }
  .postsWrap {
    flex: 0 0 50%;
    display: grid;
    grid-gap: 25px;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    margin-top: -40px;
    @media (max-width: 991px) {
      flex: 0 0 100%;
      max-width: 79%;
      margin-left: auto;
      margin-right: auto;
      margin-top: 0px;
    }
    @media (max-width: 768px) {
      max-width: 100%;
      grid-gap: 20px;
    }
    @media (max-width: 425px) {
      grid-gap: 25px;
      grid-template-columns: repeat(1, minmax(0, 1fr));
    }
  }
  .post-background {
    background: ${themeGet("colors.primary")};
    position: relative;
    border-radius: 10px;
    box-shadow: 0px 14px 50px rgba(132, 159, 184, 0.15);
    @media (min-width: 426px) {
    &:nth-of-type(2) {
      position: relative;
      top: 40px;
    }
    &:nth-of-type(4) {
      position: relative;
      top: 40px;
    }
    &:nth-of-type(3) {
      .postCount {
        align-items: baseline;
        span {
          top: 0;
        }
      }
    }
  }
  .post {
    background: ${themeGet("colors.primaryHover")};
    border-radius: 10px;
    box-shadow: 0px 14px 50px rgba(132, 159, 184, 0.15);
    padding: 50px 36px;
    position: relative;
    overflow: hidden;
    transition-duration: 0.3s;
    transition-property: transform;
    transform-origin: left bottom;
    height:100%;
    &:hover {
      transform: rotate(-4.79deg);
    }
    @media (max-width: 425px) {
      padding: 30px;
    }
    p {
      margin: 0;
      font-weight: 400;
      font-size: 28px;
      line-height: 36px;
      color: ${themeGet("colors.black")};
      @media (max-width: 1600px) {
       // font-size: 17px;
      }
      @media (max-width: 1024px) {
        font-size: 22px;
        line-height: 30px;
       }
      @media (max-width: 375px) {
        font-size: 15px;
      }
    }
    @media (min-width: 426px) {
      &:nth-of-type(2) {
        position: relative;
        top: 40px;
      }
      &:nth-of-type(4) {
        position: relative;
        top: 40px;
      }
      &:nth-of-type(3) {
        .postCount {
          align-items: baseline;
          span {
            top: 0;
          }
        }
      }
    }
  }
  .postCount {
    display: flex;
    align-items: flex-end;
    margin-top: 5px;
    margin-bottom: 5px;
    align-items: baseline;
    width:max-content;
    h3 {
      margin: 0;
      font-weight: 500;
      font-size: 82px;
      line-height: 90px;
      font-family: 'Work Sans', sans-serif;
      letter-spacing: -0.02em;
      color: ${themeGet("colors.black")};
      @media (max-width: 1600px) {
        font-size: 82px;
      }
      @media only screen and (min-width: 1024px) {
        font-size: 62px;
      }
      @media (max-width: 768px) {
        font-size: 70px;
      }
      @media (max-width: 375px) {
        font-size: 58px;
      }
    }
    // span {
    //   margin: 0;
    //   font-family: DM Sans, sans-serif;
    //   font-weight: 500;
    //   font-size: 40px;
    //   line-height: 1;
    //   letter-spacing: -0.02em;
    //   color: ${themeGet("colors.black")};
    //   position: relative;
    //   top: 5px;
    //   margin-left: 5px;
    //   @media (max-width: 375px) {
    //     font-size: 30px;
    //   }
    // }
    span{
      right: -10px;
      bottom: 0;
    }
  }
`;
export default Section;

export const Pattern = styled.div`
  @media only screen and (min-width: 319px) {
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0.6;
    left: 0;
    top: 0;
    &::before,
    &::after {
      content: "";
      position: absolute;
    }
    &::before {
      background: url(${pattern1?.src}) no-repeat;
      width: 54%;
      top: 0;
      height: 811px;
      background-position: right 22%;
      background-size: 25%;
      transform: none;
      @media only screen and (max-width: 768px) {
        display: none;
      }
      @media only screen and (min-width: 768px) {
        background-size: 25%;
      }
      @media only screen and (min-width: 1024px) {
        background-size: 28%;
      }
      @media only screen and (min-width: 1440px) {
        background-size: 25%;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 15%;
        background-position: right 44%;
      }
    }
    &::after {
      background: url(${pattern2?.src}) no-repeat;
      right: 0;
      bottom: -100px;
      width: 444px;
      height: 654px;
      @media only screen and (min-width: 319px) {
        background-position: right 60%;
        background-size: 100%;
        width: 100%;
      }
      @media only screen and (min-width: 768px) {
        background-size: 60%;
        background-position: right 80%;
        width: 444px;
      }

      @media only screen and (min-width: 1024px) {
        background-size: 60%;
        background-position: right 80%;
        width: 444px;
      }
      @media only screen and (min-width: 1440px) {
        background-size: 75%;
        background-position: right bottom;
        width: 444px;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 75%;
        right: 20%;
        width: 444px;
      }
    }
  }
`;

export const Grid = styled.div`
  gap: 140px;
  display: grid;
  grid-template-columns: 595px 1fr;
  @media only screen and (max-width: 1366px) {
    gap: 100px;
  }
  @media only screen and (max-width: 1280px) {
    gap: 100px;
    grid-template-columns: 1fr 1fr;
  }
  @media only screen and (max-width: 991px) {
    gap: 60px;
    grid-template-columns: 1fr;
  }
  @media only screen and (max-width: 768px) {
    gap: 0;
    grid-template-columns: 1fr;
  }
`;
