import { themeGet } from "@styled-system/theme-get";
import styled from "styled-components";

const TestimonialsArea = styled.section`
  background: ${themeGet("colors.white")};
  box-shadow: 0px 15px 25px rgba(199, 212, 221, 0.25);
  border-radius: 10px;
  position: relative;
  max-width: 1170px;
  margin: auto;
  .glide__slide {
    height: auto;
  }
  .testimonial_card {
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  .testimonial_card_body {
    flex: auto;
    padding: 0px 80px 0px;
    @media (max-width: 1219px) {
      padding: 0px 45px 0px;
    }
    @media (max-width: 520px) {
      padding: 0px 25px 26px;
    }
    @media (min-width: 992px) {
      display: grid;
      grid-template-columns: repeat(6, minmax(0, 1fr));
      grid-gap: 0px;
    }
    .quoteImage {
      // position: absolute;
      // top: -56px;
      // left: 0;
      @media (max-width: 520px) {
        // top: -26px;
        // max-width: 60px;
      }
    }
    .testimonial_name {
      font-weight: 600;
      font-size: 18px;
      margin-bottom: 6px;
      @media (max-width: 520px) {
        font-size: 16px;
      }
    }
    .testimonial_designation {
      opacity: 0.7;
      display: block;
      @media (max-width: 520px) {
        font-size: 14px;
      }
    }
    .testimonial_logo {
      max-width: 130px;
    }
    .testimonial_name,
    .testimonial_designation {
      @media (max-width: 520px) {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  }
  .testimonials_right_content {
    grid-column: span 4;
    position: relative;
    display: flex;
    align-items: center;
    p {
      font-weight: 500;
      font-size: 20px;
      line-height: 33px;
      color: ${themeGet("colors.textColorAlt")};
      letter-spacing: -0.02em;
      padding-top: 10px;
      // font-style: italic;
      margin-bottom: 23px;
      @media (max-width: 768px) {
        font-size: 18px;
        line-height: 200%;
      }
      @media (max-width: 520px) {
        font-size: 18px;
      }
    }
  }
  .testimonials_left_content {
    grid-column: span 2;
    text-align: left;
    display: flex;
    align-items: center;
  }
  .testimonial_reviewer_image {
    position: relative;
    img {
      margin-left: auto;
      // width: 280px;
      height: 280px;
      @media (max-width: 1219px) {
        width: 250px;
        height: 250px;
      }
      border-radius: 20px;
      background-color: ${themeGet("colors.primaryHover")};
    }
    display: none;
    @media (min-width: 992px) {
      display: inline-block;
    }
  }
  .rating {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    bottom: 0;
    line-height: 1;
    background-color: ${themeGet("colors.white")};
    box-shadow: 0px 4px 20px rgb(151 172 190 / 15%);
    border-radius: 50px;
    font-size: 22px;
    color: ${themeGet("colors.rating")};
    padding: 14px 26px;
    white-space: nowrap;
    width: auto;
    i {
      &:not(last-child) {
        margin-right: 4px;
      }
    }
    svg {
      height: 1em;
      width: 1em;
    }
  }
  .testimonial_card_footer {
    background-color: #f8f9fa;
    text-align: center;
    padding: 23px 15px;
    flex-shrink: 0;
    a {
      color: #6720df;
      font-weight: 500;
      font-size: 15px;
      @media (max-width: 520px) {
        font-size: 13px;
      }
      svg {
        margin-left: 9px;
        display: inline-block;
        transform: translateX(2px);
        transition: 0.3s ease 0s;
      }
      &:hover {
        svg {
          transform: translateX(5px);
        }
      }
    }
  }
  .glide__bullets {
    position: absolute;
    line-height: 1;
    display: flex;
    margin-top: 40px;
    bottom: -50px;
    left: 50%;
    transform: translateX(-50%);

    .glide__bullet {
      background-color: rgba(0, 0, 0, 0.2);
      border: 0;
      border-radius: 50px;
      cursor: pointer;
      display: block;
      text-indent: -9999em;
      height: 9px;
      width: 12px;
      -webkit-transition: all 0.3s ease-in-out 0s;
      transition: all 0.3s ease-in-out 0s;

      &:not(:last-child) {
        margin-right: 8px;
      }

      &--active {
        background-color: #000000;
        width: 18px;
      }
    }
  }
  .reviewer_content_wrapper {
    .reviewer_content {
      display: flex;
      justify-content: space-between;
      align-items: center;
      @media (max-width: 520px) {
        display: block;
        > img {
          max-width: 90px;
        }
      }
    }
    .testimonial_reviewer_image {
      display: none;
    }
    @media (max-width: 991px) {
      display: flex;
      grid-gap: 10px;
      flex-wrap: wrap;
      .testimonial_reviewer_image {
        flex-shrink: 0;
        display: block;
        img {
          margin-left: 0;
          width: 75px;
          height: 75px;
        }
      }
      .reviewer_content {
        flex: 0 0 calc(100% - 85px);
        max-width: calc(100% - 85px);
      }
    }
  }
`;

export const SectionHeading = styled.div`
  --marginBottom: 65px;
  margin-bottom: var(--marginBottom);
  @media (max-width: 1024px) {
    --marginBottom: 55px;
  }
  @media (max-width: 600px) {
    --marginBottom: 45px;
  }
  @media (max-width: 520px) {
    --marginBottom: 35px;
  }
  text-align: center;
  h2 {
    font-weight: 500;
    font-size: 42px;
    line-height: 57px;
    letter-spacing: -0.02em;
    @media (min-width: 1024px) {
      font-size: 52px;
      line-height: 57px;
    }
    @media (max-width: 600px) {
      font-size: 52px;
      line-height: 57px;
    }
    @media (max-width: 520px) {
      font-size: 38px;
      line-height: 42px;
    }
  }
`;

export const UseCaseHeading = styled.div`
  padding-top: 140px;
  // padding-bottom: 72px;
  background-color: ${themeGet("colors.white")};
  @media (min-width: 1281px) and (max-width: 2000px) {
    // position: relative;
    // &:before {
    //   background-image: url("data:image/svg+xml,%3Csvg width='1920' height='122' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M1918.23 32.511S522.677-1.254 0 122V0h1920' fill='%23fff'/%3E%3C/svg%3E");
    //   content: "";
    //   position: absolute;
    //   left: 0;
    //   top: -1px;
    //   width: 100%;
    //   height: 122px;
    //   z-index: 1;
    //   background-repeat: no-repeat;
    //   background-position: top left;
    //   background-size: cover;
    // }
  }
  @media (max-width: 1563px) {
    padding-top: 100px;
    // padding-bottom: 72px;
  }
  @media (max-width: 1280px) {
    padding-top: 60px;
    // padding-bottom: 72px;
  }
  @media (max-width: 1024px) {
    padding-top: 60px;
    // padding-bottom: 72px;
  }
  @media (max-width: 768px) {
    padding-top: 40px;
    // padding-bottom: 72px;
  }
  .testimonial_area {
    padding-top: 150px;
    @media (max-width: 1563px) {
      padding-top: 100px;
    }
    @media (max-width: 1024px) {
      padding-top: 75px;
    }
    @media (max-width: 768px) {
      padding-top: 50px;
    }
  }
  .arrow {
    justify-content: space-between;
    display: flex;
    position: relative;
    top: -228px;
    button {
      background: transparent;
      border: 0;
      cursor: pointer;
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
`;

export const Section = styled.div`
  padding-top: 140px;
  padding-bottom: 72px;
  background-color: ${themeGet("colors.white")};
  @media (min-width: 1281px) and (max-width: 2000px) {
    // position: relative;
    // &:before {
    //   background-image: url("data:image/svg+xml,%3Csvg width='1920' height='122' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M1918.23 32.511S522.677-1.254 0 122V0h1920' fill='%23fff'/%3E%3C/svg%3E");
    //   content: "";
    //   position: absolute;
    //   left: 0;
    //   top: -1px;
    //   width: 100%;
    //   height: 122px;
    //   z-index: 1;
    //   background-repeat: no-repeat;
    //   background-position: top left;
    //   background-size: cover;
    // }
  }
  @media (max-width: 1563px) {
    padding-top: 100px;
    padding-bottom: 72px;
  }
  @media (max-width: 1280px) {
    padding-top: 60px;
    padding-bottom: 72px;
  }
  @media (max-width: 1024px) {
    padding-top: 60px;
    padding-bottom: 72px;
  }
  @media (max-width: 768px) {
    padding-top: 40px;
    padding-bottom: 72px;
  }
  .testimonial_area {
    padding-top: 150px;
    @media (max-width: 1563px) {
      padding-top: 100px;
    }
    @media (max-width: 1024px) {
      padding-top: 75px;
    }
    @media (max-width: 768px) {
      padding-top: 50px;
    }
  }
  .arrow {
    justify-content: space-between;
    display: flex;
    position: relative;
    top: -228px;
    button {
      background: transparent;
      border: 0;
      cursor: pointer;
    }

    @media (max-width: 768px) {
      display: none;
    }
    // @media (max-width: 600px) {
    //   top: -270px;
    // }
    // @media (max-width: 480px) {
    //   top: -300px;
    //   left: -10px;
    // }
    // @media (max-width: 415px) {
    //   top: -360px;
    //   left: -10px;
    // }
    // @media (max-width: 391px) {
    //   top: -401px;
    //   left: -10px;
    // }
    // @media (max-width: 376px) {
    //   top: -373px;
    //   left: -10px;
    // }
    // @media (max-width: 361px) {
    //   top: -402px;
    //   left: -10px;
    // }
    // @media (max-width: 321px) {
    //   top: -403px;
    //   left: -10px;
    // }
    // @media (max-width: 281px) {
    //   top: -438px;
    //   left: -10px;
    // }
  }
`;

export default TestimonialsArea;
// @media (max-width: 768px) {
//   display: none;
// }
