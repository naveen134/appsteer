import Heading from "common/components/Heading";
import { testimonialData } from "common/data/Appsteer";
import React from "react";
import Fade from "react-reveal/Fade";
import Carousel from "./carousel";
import TestimonialsArea, {
  SectionHeading,
  Section,
  UseCaseHeading,
} from "./testimonials.style";

const Testimonials = (props) => {
  const { testimonialData, ...rest } = props;
  return (
    <>
      {/* {rest?.useCase ? (
        <UseCaseHeading>
          <SectionHeading>
            <Heading content="Why businesses love AppSteer" />
          </SectionHeading>
        </UseCaseHeading>
      ) : ( */}
      <Section>
        <div {...props}>
          <SectionHeading>
            <Heading
              content={
                testimonialData?.title
                  ? testimonialData?.title
                  : "Why businesses love AppSteer"
              }
            />
          </SectionHeading>

          {testimonialData?.posts?.length > 0 ? (
            <Fade up>
              <TestimonialsArea id="testimonials">
                <Carousel data={testimonialData?.posts} />
              </TestimonialsArea>
            </Fade>
          ) : (
            ""
          )}
        </div>
      </Section>
      {/* )} */}
    </>
  );
};

export default Testimonials;
