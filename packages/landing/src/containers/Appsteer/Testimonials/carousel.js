import Glide from "@glidejs/glide";
import "@glidejs/glide/dist/css/glide.core.min.css";
import testimonialsQuote from "common/assets/image/appsteer/Quote.png";
import Heading from "common/components/Heading";
import Image from "common/components/Image";
import Link from "common/components/Link";
import Rating from "common/components/Rating";
import Text from "common/components/Text";
import React, { useEffect } from "react";

import leftArrow from "common/assets/image/appsteer/testimonial-arrow.png";
import rightArrow from "common/assets/image/appsteer/right-arrow.png";

const Carousel = ({ data }) => {
  useEffect(() => {
    const glide = new Glide("#glide_carousel", {
      type: "carousel",
      perView: 1,
      gap: 0,
      autoplay: 5000,
    });
    glide.mount();
  });

  return (
    <div className="glide" id="glide_carousel">
      <div className="slide__wrapper">
        <div className="glide__track" data-glide-el="track">
          <ul className="glide__slides">
            {data?.map(({ attributes }, index) => (
              <li className="glide__slide" key={`glide__slide--key${index}`}>
                <div className="testimonial_card">
                  <div className="testimonial_card_body">
                    <div className="testimonials_left_content">
                      <div className="testimonial_reviewer_image">
                        <Image
                          src={
                            attributes?.ClientProfileImage?.data?.attributes
                              ?.url
                          }
                          alt={attributes?.ClientName}
                        />
                        {/* <Rating rating={rating} /> */}
                      </div>
                    </div>
                    <div className="testimonials_right_content">
                      <div>
                        <Image
                          className="quoteImage"
                          src={testimonialsQuote?.src}
                          alt="testimonials image"
                        />
                        <Text as="p" content={attributes?.ClientFeedback} />
                        <div className="reviewer_content_wrapper">
                          <div className="testimonial_reviewer_image">
                            <Image
                              src={
                                attributes?.ClientProfileImage?.data?.attributes
                                  ?.url
                              }
                              alt={attributes?.ClientName}
                            />
                          </div>
                          <div className="reviewer_content">
                            <div>
                              <Heading
                                as="h3"
                                className="testimonial_name"
                                content={attributes?.ClientName}
                              />
                              <Text
                                as="span"
                                className="testimonial_designation"
                                content={attributes?.ClientDesignation}
                              />
                            </div>

                            <Image
                              className="testimonial_logo"
                              src={
                                attributes?.ClientCompanyLogo?.data?.attributes
                                  ?.url
                              }
                              alt={attributes?.ClientName}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="testimonial_card_footer">
                    <Link href={button?.link}>
                      {button?.label}
                      <svg width="16" height="14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="m8.469 1 5.625 5.625-5.625 5.625M13.313 6.625H1.905" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/></svg>
                    </Link>
                  </div> */}
                </div>
              </li>
            ))}
          </ul>
          <div class="glide__arrows arrow" data-glide-el="controls">
            <button
              class="glide__arrow glide__arrow--left left"
              data-glide-dir="<"
            >
              <Image src={leftArrow.src}></Image>
            </button>
            <button
              class="glide__arrow glide__arrow--right right"
              data-glide-dir=">"
            >
              <Image src={rightArrow.src}></Image>
            </button>
          </div>
        </div>
      </div>
      {/* <div className="glide__bullets" data-glide-el="controls[nav]">
        {data.map((button, index) => (
          <button
            className="glide__bullet"
            data-glide-dir={`=${index}`}
            key={`glide_bullet--key${index}`}
          />
        ))}
      </div> */}
    </div>
  );
};

export default Carousel;
