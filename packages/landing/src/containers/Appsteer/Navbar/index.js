import React, { useContext, useEffect, useState } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import { openModal, closeModal } from "@redq/reuse-modal";
import { Icon } from "react-icons-kit";

import { ic_keyboard_arrow_down_twotone } from "react-icons-kit/md/ic_keyboard_arrow_down_twotone";

import NavbarWrapper from "common/components/Navbar";
import Drawer from "common/components/Drawer";
import Button from "common/components/Button";
import Logo from "common/components/UIElements/Logo";
import Box from "common/components/Box";
import HamburgMenu from "common/components/HamburgMenu";
import Container from "common/components/UI/Container";
import { DrawerContext } from "common/contexts/DrawerContext";

import PrimaryButton from "common/components/Appsteer/primaryButton";
import logo from "common/assets/image/appsteer/logo.svg";
import logoWhite from "common/assets/image/appsteer/logo-white.svg";

import MegaMenu from "./MegaMenu";
import { MegaMenuContent } from "common/data/Appsteer";
import { NavLink, SideBarWrapper, SideBarWrapperOthers } from "./navBar.style";
import blog from "common/assets/image/appsteer/blog-featured.png";

const CloseModalButton = () => (
  <Button
    className="modalCloseBtn"
    variant="fab"
    onClick={() => closeModal()}
    icon={<i className="flaticon-plus-symbol" />}
  />
);

const Navbar = ({
  navbarStyle,
  logoStyle,
  button,
  row,
  menuWrapper,
  navData,
  data,
}) => {
  const { state, dispatch } = useContext(DrawerContext);

  const [megaMenuContent, setMegaMenuContent] = useState();
  const [MegaMenuStyle, setMegaMenuStyle] = useState();
  const [showMegaMenu, setShowMegaMenu] = useState(false);

  const megamenuData = {
    1: {
      id: 1,
      style: "columnThree",
      title: "Industries",
      link: "/industry-listing",
      content: data?.map((eachIndustry, index) => ({
        id: index + 1,
        text: eachIndustry?.attributes?.IndustryTitle,
        link: `/industry-vertical/${eachIndustry?.attributes?.slug}`,
        featuredImage:
          eachIndustry?.attributes?.MegamenuFeaturedImage?.data?.attributes
            ?.url,
        content: eachIndustry?.attributes?.use_cases?.data?.map(
          (eachUsecase, index) => ({
            id: index,
            text: eachUsecase?.attributes?.UseCaseTitle,
            link: `/usecase/${eachUsecase?.attributes?.slug}`,
          })
        ),
      })),
    },
    // 2: {
    //   id: 2,
    //   style: "columnTwo",
    //   title: "Resources",
    //   content: [
    //     {
    //       id: 11,
    //       text: "Blog",
    //       link: "/blogs",
    //       featuredImage: blog,
    //     },
    //     {
    //       id: 12,
    //       text: "Cafe",
    //       link: "/",
    //       featuredImage: blog,
    //     },
    //     {
    //       id: 13,
    //       text: "Comics",
    //       link: "/",
    //       featuredImage: blog,
    //     },
    //     {
    //       id: 14,
    //       text: "Newsroom",
    //       link: "/",
    //       featuredImage: blog,
    //     },
    //     {
    //       id: 15,
    //       text: "Event",
    //       link: "/",
    //       featuredImage: blog,
    //     },
    //   ],
    // },
  };

  // Toggle drawer
  const toggleHandler = () => {
    dispatch({
      type: "TOGGLE",
    });
  };

  const megaMenuToggleHandler = (key) => {
    if (key) {
      const content = megamenuData[key].content;
      const style = megamenuData[key].style;
      setMegaMenuContent(content);
      setMegaMenuStyle(style);
    }

    setShowMegaMenu(key);
    dispatch({
      type: "TOGGLE_MEGA_MENU",
      payload: key,
    });
  };

  useEffect(() => {
    // console.log({ MegaMenuContent });
  }, [MegaMenuContent]);

  return (
    <NavbarWrapper {...navbarStyle}>
      <Container width="1400px">
        <Box {...row}>
          <Box className="logo-container">
            <Logo
              href="/"
              logoSrc={logoWhite}
              title="AppSteer"
              logoStyle={logoStyle}
              className="main-logo"
              key="1"
            />
            <Logo
              href="/"
              logoSrc={logo}
              title="AppSteer"
              logoStyle={logoStyle}
              className="sticky-logo"
              key="2"
            />
          </Box>
          <div className="navLinkContainer">
            {Object.keys(megamenuData).map((key) => (
              <div key={key} className="navLink">
                <Link href={`${megamenuData[key]?.link}`}>
                  <NavLink
                    key={megamenuData[key]?.id}
                    onMouseEnter={() => megaMenuToggleHandler(key)}
                    active={key === showMegaMenu}
                  >
                    {megamenuData[key]?.title}
                    <Icon key={key} icon={ic_keyboard_arrow_down_twotone} />
                  </NavLink>
                </Link>
              </div>
            ))}

            {/* <div className="navLink">
              <NavLink
                href="#"
                onMouseEnter={() =>
                  megaMenuToggleHandler({
                    key: "Usecases",
                    content: navData,
                    style: "columnThree",
                  })
                }
                active={showMegaMenu === "Usecases"}
              >
                Usecases
                <Icon icon={ic_keyboard_arrow_down_twotone} />
              </NavLink>
            </div>

            <div key="teams" className="navLink">
              <NavLink href="#" key="teams">
                Resources
              </NavLink>
            </div> */}

            {/* <div key="teams" className="navLink">
              <Link href="/team">
                <NavLink key="teams">Team</NavLink>
              </Link>
            </div> */}
          </div>
          <Box {...menuWrapper} className="mainMenuWrapper">
            {/* <ScrollSpyMenu
              className="main_menu"
              menuItems={menu_items}
              offset={-70}
            /> */}

            {/* <Button
              {...button}
              iconPosition="left"
              icon={<img src={lockIcon?.src} alt="lock icon" />}
              title="Login Now"
              onClick={handleLoginModal}
              className="navbar_button navbar_button_one"
            /> */}

            <Link href="/explore">
              <a className="navbar_button navbar_button_one">
                <PrimaryButton text="Explore" />
              </a>
            </Link>
            <Drawer
              width="330px"
              placement="right"
              drawerHandler={<HamburgMenu barColor="#108AFF" />}
              open={state.isOpen}
              toggleHandler={toggleHandler}
            >
              {/* <ScrollSpyMenu
                className="mobile_menu"
                menuItems={menu_items}
                drawerClose={true}
                offset={-100}
              /> */}
              <div>
                {/* <SideBarWrapper>
                  <div className="sideLinkWrap exploreWrap">
                    <Link href="/explore">Explore</Link>
                  </div>
                  <div className="sideLinkWrap">
                    <Link href="/industry-listing">Industry Listing</Link>
                  </div>
                </SideBarWrapper> */}
                {/* <h2>Industries</h2> */}
                {/* <div className="sideLinkWrap">
                    <Link href="/explore">Explore</Link>
                  </div> */}
                {/* <div> */}
                {/* <Link href="/industry-listing">Industry Listing</Link> */}
                {/* {Object.keys(MegaMenuContent[1].content).map((key) => (
                      <div className="sideLinkWrap">
                        <Link href={MegaMenuContent[1].content[key].link}>
                          {MegaMenuContent[1].content[key].text}
                        </Link>
                      </div>
                    ))} */}
                {/* </div> */}
                {/* </SideBarWrapper> */}
                {/* <SideBarWrapper> */}{" "}
                {/* {Object.keys(megamenuData).map((key) => (
                    <div key={key} className="navLink">
                      <Link href="/">
                        <NavLink
                          key={megamenuData[key]?.id}
                          onMouseEnter={() => megaMenuToggleHandler(key)}
                          active={key === showMegaMenu}
                        >
                          {megamenuData[key]?.title}
                          <Icon
                            key={key}
                            icon={ic_keyboard_arrow_down_twotone}
                          />
                        </NavLink>
                      </Link>
                    </div>
                  ))} */}
                {/* </SideBarWrapper> */}
                <SideBarWrapperOthers>
                  <div>
                    <div className="sideLinkWrap">
                      <Link href="/explore">Explore</Link>
                    </div>
                    <div className="sideLinkWrap">
                      <Link href="/industry-listing">Industries</Link>
                    </div>
                    {/* <div className="sideLinkWrap">
                      <Link href="/about-us">About Us</Link>
                    </div> */}
                    {/* <div className="sideLinkWrap">
                      <Link href="#">Careers</Link>
                    </div>
                    <div className="sideLinkWrap">
                      <Link href="#">Community</Link>
                    </div> */}
                    <div className="sideLinkWrap">
                      <Link href="/contact-us">Contact Us</Link>
                    </div>
                    {/* <div className="sideLinkWrap">
                      <Link href="/terms-and-conditions">
                        Terms & Conditions
                      </Link>
                    </div> */}
                    <div className="sideLinkWrap">
                      <Link href="/privacy-policy">Privacy Policy</Link>
                    </div>
                    <div className="sideLinkWrap">
                      <Link href="/blogs">Blog</Link>
                    </div>
                    {/* <div className="sideLinkWrap">
                      <Link href="/team">Team</Link>
                    </div> */}
                  </div>
                </SideBarWrapperOthers>
              </div>
            </Drawer>
          </Box>
        </Box>
      </Container>
      <MegaMenu
        menuStyle={MegaMenuStyle}
        list={megaMenuContent}
        open={showMegaMenu}
        setShowMegaMenu={megaMenuToggleHandler}
        top={state.isOpen ? "90px" : "70px"}
      />
    </NavbarWrapper>
  );
};

Navbar.propTypes = {
  navbarStyle: PropTypes.object,
  logoStyle: PropTypes.object,
  button: PropTypes.object,
  row: PropTypes.object,
  menuWrapper: PropTypes.object,
};

Navbar.defaultProps = {
  navbarStyle: {
    className: "web_app_minimal_navbar",
    minHeight: "70px",
    display: "block",
  },
  row: {
    flexBox: true,
    alignItems: "center",
    width: "100%",
  },
  logoContainer: {},
  logoStyle: {
    maxWidth: ["190px", "190px"],
  },
  button: {},
  menuWrapper: {
    flexBox: true,
    alignItems: "center",
  },
};

export default Navbar;
