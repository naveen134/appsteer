import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";
import downArrow from "common/assets/image/appsteer/icon-down-arrow.svg";
// import {AiOutlineDown} from

export const MegaMenuWrapper = styled.div`
  // ${(props) => !props.show && "display: none;"}
  z-index: 10;
  position: fixed;
  top: 90px;
  margin: auto;
  right: 0;
  left: 0;
  transition: 0.5s ease;
  .animate {
    animation-duration: 0.5s;
    animation-delay: 0s;
    animation-name: animate-fade;
    animation-timing-function: cubic-bezier(0.26, 0.53, 0.74, 1.48);
    animation-fill-mode: backwards;
    transition: all ease 0.5s;
  }
  /* Grow top */
  .animate .grow-top {
    animation-duration: 0.4s;
    animation-delay: 0s;
    animation-name: animate-grow-top;
  }
  .animate .grow-top::after {
    animation-name: animate-fade-out;
    animation-timing-function: ease;
    animation-duration: 0.4s;
  }
  @keyframes animate-grow-top {
    0% {
      opacity: 0;
      transform: translate(0, -50px);
      visibility: hidden;
    }
    100% {
      opacity: 1;
      transform: translate(1, 1) scale(1, 1);
    }
  }

  .row {
    display: flex;
    .industry {
      padding-right: 10px;
      border-right: 1px solid #d9d9d9;
      margin-right: 10px;
    }
  }
  .center-wraper {
    margin: auto;
    width: fit-content;
    background-color: ${themeGet("colors.dimWhite")};
    padding: 30px;
    border-radius: 20px;
  }
  .megamenu-image {
    @media (max-width: 1140px) {
      display: none;
    }
  }
`;

export const MegaMenuIndustryColum = styled.div`
  min-width: 288px;
  padding: 15px;
  cursor: pointer;
  border-radius: 10px;
  background-color:  ${(props) => props.selected && themeGet("colors.white")}};
  display: flex;
//  color: ${(props) => (props.selected ? "#191919" : "#666666")}};
  justify-content: space-between;
  img{
    transform: rotate(90deg);
  }
  &:hover {
    background-color: ${themeGet("colors.white")};
    font-weight: 500;
    font-size: 16px;
    line-height: 18px;

  }
`;

export const MegaMenuIndustryColumTwo = styled.div`
min-width: 450px;
  padding: 15px;
  cursor: pointer;
  border-radius: 10px;
  background-color:  ${(props) => props.selected && themeGet("colors.white")}};
  display: flex;
  // color: ${(props) => (props.selected ? "#191919" : "#666666")}};
 // color: #191919;
  justify-content: space-between;
  img{
    transform: rotate(90deg);
  }
  &:hover {
    background-color: ${themeGet("colors.white")};
    font-weight: 500;
    font-size: 16px;
    line-height: 18px;

  }
`;

export const MegaMenuIndustryColumImage = styled.div`
min-width: 190px;
  padding: 15px;
  cursor: pointer;
  border-radius: 10px;
  background-color:  ${(props) => props.selected && themeGet("colors.white")}};
  display: flex;
  color: ${(props) => (props.selected ? "#191919" : "#666666")}};
  justify-content: space-between;
  img{
   
  }
  &:hover {
    background-color: ${themeGet("colors.white")};
    font-weight: 500;
    font-size: 16px;
    line-height: 18px;

  }
`;

export const NavLink = styled.li`
  color: ${(props) => (props.active ? themeGet("colors.primary") : "auto")};
  margin: auto 20px 0 0;
  max-width: fit-content;
  cursor: pointer;
  color: ${(props) => (props.active ? themeGet("colors.primary") : "auto")};
  a {
    color: inherit;
    &:hover {
      color: ${themeGet("colors.primary")};
    }
  }
  svg {
    transform: ${(props) => (props.active ? "rotate(180deg)" : "rotate(0deg)")};
    transition: all 0.3s ease-out;
    position: relative;
    right: -7px;
  }
  // &::after {
  //   background: url(${downArrow}) no-repeat;
  //   content: "";
  //   right: -10px;
  //   width: 20px;
  //   height: 20px;
  // }
`;

export const SideBarWrapper = styled.div`
  text-align: right;
  @media only screen and (min-width: 991px) {
    display: none;
  }
  h2 {
    font-size: 15px;
    color: ${themeGet("colors.textColorAlt")};
    font-weight: 400;
    margin-bottom: 20px;
    @media only screen and (min-width: 768px) {
      font-size: 20px;
    }
  }
  .exploreWrap {
    @media only screen and (min-width: 769px) {
      display: none;
    }
  }
  .sideLinkWrap {
    margin-bottom: 20px;
    @media only screen and (min-width: 768px) {
      font-size: 40px;
    }
  }
  a {
    font-size: 15px;
    color: ${themeGet("colors.black")};
    font-weight: 500;
    cursor: pointer;
    @media only screen and (min-width: 768px) {
      font-size: 20px;
    }
    &:hover {
      color: ${themeGet("colors.primary")};
    }
    &:focus {
      color: ${themeGet("colors.primary")};
    }
  }
`;

export const SideBarWrapperOthers = styled.div`
  text-align: right;
  @media only screen and (min-width: 991px) {
    margin-top: 40px;
  }
  h2 {
    font-size: 15px;
    color: ${themeGet("colors.textColorAlt")};
    font-weight: 400;
    margin-bottom: 20px;
    @media only screen and (min-width: 768px) {
      font-size: 20px;
    }
  }
  .sideLinkWrap {
    margin-bottom: 20px;
    @media only screen and (min-width: 768px) {
      font-size: 40px;
    }
  }
  a {
    font-size: 15px;
    color: ${themeGet("colors.black")};
    font-weight: 500;
    cursor: pointer;
    @media only screen and (min-width: 768px) {
      font-size: 20px;
    }
    &:hover {
      color: ${themeGet("colors.primary")};
    }
    &:focus {
      color: ${themeGet("colors.primary")};
    }
  }
`;

// export const NavIcon = styled.div`
//   width: 28px;
//   height: 20px;
//   position: relative;
//   margin: 0px 0px auto auto;
//   -webkit-transform: rotate(0deg);
//   -moz-transform: rotate(0deg);
//   -o-transform: rotate(0deg);
//   transform: rotate(0deg);
//   -webkit-transition: 0.5s ease-in-out;
//   -moz-transition: 0.5s ease-in-out;
//   -o-transition: 0.5s ease-in-out;
//   transition: 0.5s ease-in-out;
//   cursor: pointer;

//   span {
//     display: block;
//     position: absolute;
//     height: 2px;
//     width: 100%;
//     border-radius: 9px;
//     opacity: 1;
//     left: 0;
//     background: ${props =>
//       props.isLight ? theme.backgroundLight : theme.primaryColor};
//     -webkit-transform: rotate(0deg);
//     -moz-transform: rotate(0deg);
//     -o-transform: rotate(0deg);
//     transform: rotate(0deg);
//     -webkit-transition: 0.25s ease-in-out;
//     -moz-transition: 0.25s ease-in-out;
//     -o-transition: 0.25s ease-in-out;
//     transition: 0.25s ease-in-out;
//   }
//   &:hover {
//     span {
//       width: 100% !important;
//       background: ${theme.primaryColor};
//     }
//   }

//   ${props =>
//     props.show
//       ? `
//       position: absolute;
//     top: 20px;
//     right: 42px;
//       span:nth-child(1) {
//       top: 18px;
//       -webkit-transform: rotate(135deg);
//       -moz-transform: rotate(135deg);
//       -o-transform: rotate(135deg);
//       transform: rotate(135deg);
//     }

//      span:nth-child(2) {
//       opacity: 0;
//       left: -60px;
//     }

//      span:nth-child(3) {
//       top: 18px;
//       -webkit-transform: rotate(-135deg);
//       -moz-transform: rotate(-135deg);
//       -o-transform: rotate(-135deg);
//       transform: rotate(-135deg);
//     }`
//       : `span:nth-child(1) {
//       top: 0px;
//       width: 100%;
//     }

//     span:nth-child(2) {
//       top: 9px;
//       width: 80%;
//     }

//     span:nth-child(3) {
//       top: 18px;
//       width: 50%;
//     }
//   `}
// `
// export const CloseIcon = styled.div`
//   width: 28px;
//   height: 20px;
//   position: absolute;
//   top: 20px;
//   right: 42px;
//   margin: 8px 0px auto auto;
//   -webkit-transform: rotate(0deg);
//   -moz-transform: rotate(0deg);
//   -o-transform: rotate(0deg);
//   transform: rotate(0deg);
//   -webkit-transition: 0.5s ease-in-out;
//   -moz-transition: 0.5s ease-in-out;
//   -o-transition: 0.5s ease-in-out;
//   transition: 0.5s ease-in-out;
//   cursor: pointer;

//   span {
//     display: block;
//     position: absolute;
//     height: 2px;
//     width: 100%;
//     border-radius: 9px;
//     opacity: 1;
//     left: 0;
//     background: ${theme.backgroundDark};
//     -webkit-transform: rotate(0deg);
//     -moz-transform: rotate(0deg);
//     -o-transform: rotate(0deg);
//     transform: rotate(0deg);
//     -webkit-transition: 0.25s ease-in-out;
//     -moz-transition: 0.25s ease-in-out;
//     -o-transition: 0.25s ease-in-out;
//     transition: 0.25s ease-in-out;
//   }
//   &:hover {
//     span {
//       background: ${theme.primaryColor};
//     }
//   }

//   span:nth-child(1) {
//     top: 18px;
//     -webkit-transform: rotate(135deg);
//     -moz-transform: rotate(135deg);
//     -o-transform: rotate(135deg);
//     transform: rotate(135deg);
//   }

//   span:nth-child(2) {
//     opacity: 0;
//     left: -60px;
//   }

//   span:nth-child(3) {
//     top: 18px;
//     -webkit-transform: rotate(-135deg);
//     -moz-transform: rotate(-135deg);
//     -o-transform: rotate(-135deg);
//     transform: rotate(-135deg);
//   }
// `

// export const NavSideBarWrapper = styled.div`
//   z-index: 6;
//   ${props => !props.show && "visibility: hidden;"}
//   width: 100%;
//   background: transparent;
//   height: 100%;
//   position: fixed;
//   backdrop-filter: blur(6px);
//   div {
//     ${props => !props.show && "visibility: hidden;"}
//   }
// `

// export const NavSideBarContainer = styled.div`
//   background-color: azure;
//   height: 100%;
//   width: 300px;
//   margin-left: auto;
//   padding: 120px 60px 20px;
//   text-align: right;
//   display: flex;
//   flex-direction: column;
//   justify-content: space-evenly;
//   @media ${device.mobileL} {
//     width: 380px;
//   }
// `

// export const SideBarLinkWrapper = styled.div`
//   ${props => props.mobile && "visibility: hidden;"}

//   cusor: pointer;
//   &:hover {
//     color: ${theme.primaryColor};
//   }
//   a {
//     text-decoration: none;
//     color: inherit;
//   }
//   a:hover {
//     color: ${theme.primaryColor};
//   }
//   @media ${device.mobileL} {
//     display: block;
//   }
// `

// export const DropdownWrapper = styled.div`
//   display: block;
//   ul {
//     display: none;
//   }
//   @media ${device.tablet} {
//     ${props => props.mobile && "display: none;"}
//   }
// `
