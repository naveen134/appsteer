import React, { useEffect, useState } from "react";
import {
  MegaMenuWrapper,
  MegaMenuIndustryColum,
  MegaMenuIndustryColumTwo,
  MegaMenuIndustryColumImage,
} from "./navBar.style";
import Link from "next/link";
import { Reveal } from "react-reveal";
import NextImage from "common/components/NextImage";

import logoIcon from "common/assets/image/appsteer/logo-stats.png";

const MegaMenu = ({ menuStyle, list, setShowMegaMenu, open, top }) => {
  // console.log("the gh list", list);
  const [secondColumnList, setsecondColumnList] = useState(
    list && list[0]?.content ? list[0]?.content : null
  );
  const [imageUrl, setImageUrl] = useState(list && list[0]?.featuredImage);
  const [selectedIndustry, setSelectedIndustry] = useState(0);

  useEffect(() => {
    if (menuStyle === "columnTwo") {
      const url = list[0]?.featuredImage;
      setImageUrl(url);
    } else if (menuStyle === "columnThree") {
      const columnContent = list[0].content ? list[0].content : null;
      if (columnContent) {
        setsecondColumnList(columnContent);
      } else setsecondColumnList(null);

      const url = list[0]?.featuredImage;
      setImageUrl(url);
    }
  }, [open]);

  const handleMegaMenuColumn = (index) => {
    setSelectedIndustry(index);
    if (menuStyle === "columnTwo") {
      const url = list[index]?.featuredImage;
      setImageUrl(url);
    } else if (menuStyle === "columnThree") {
      const content = list[index].content ? list[index].content : null;
      if (content) {
        setsecondColumnList(content);
      } else setsecondColumnList(null);

      const url = list[index]?.featuredImage;
      setImageUrl(url);
    }
  };

  // const handleMegaMenuColumn = (item) => {
  //   setSelectedIndustry(item?.attributes?.slug);
  //   if (menuStyle === "columnTwo") {
  //     const url = item?.featuredImage;
  //     setImageUrl(url);
  //   } else if (menuStyle === "columnThree") {
  //     const content = item?.use_cases?.data;
  //     if (content) {
  //       setsecondColumnList(content);
  //     } else setsecondColumnList(null);

  //     const url =
  //       item?.IndustryFeaturedImage?.data?.attributes?.formats?.thumbnail?.url;
  //     setImageUrl(url);
  //     console.log(url);
  //   }
  // };

  return (
    open && (
      <MegaMenuWrapper className="mega-top-fix">
        <Reveal effect="animate grow-top">
          <div
            className="center-wraper"
            onMouseLeave={() => setShowMegaMenu(false)}
            role="none"
          >
            <div className="row">
              {menuStyle === "columnThree" && (
                <>
                  <div className="col-12 col-md-4 industry">
                    {list &&
                      list.map((eachIndustry, index) => (
                        <Link href={eachIndustry.link}>
                          <MegaMenuIndustryColum
                            key={eachIndustry.id}
                            onMouseEnter={() => handleMegaMenuColumn(index)}
                            selected={selectedIndustry === index}
                            onClick={() => setShowMegaMenu(false)}
                          >
                            <span> {eachIndustry.text}</span>
                            {selectedIndustry === index && (
                              <NextImage src={logoIcon} alt="logo" />
                            )}
                          </MegaMenuIndustryColum>
                        </Link>
                      ))}
                    {/* {list &&
                      list?.map((item) => (
                        <Link href={item?.attributes?.slug} key={item?.id}>
                          <MegaMenuIndustryColum
                            onMouseEnter={() => handleMegaMenuColumn(item)}
                            selected={
                              selectedIndustry === item?.attributes?.slug
                            }
                          >
                            <span> {item?.attributes?.IndustryTitle}</span>
                            {selectedIndustry === item?.attributes?.slug && (
                              <NextImage src={logoIcon} alt="logo" />
                            )}
                          </MegaMenuIndustryColum>
                        </Link>
                      ))} */}
                  </div>
                  <div className="col-12 col-md-4">
                    {secondColumnList &&
                      secondColumnList.map((eachUsecase, index) => (
                        <Link href={eachUsecase.link}>
                          <MegaMenuIndustryColumTwo
                            onClick={() => setShowMegaMenu(false)}
                            key={eachUsecase.text}
                          >
                            {eachUsecase.text}
                          </MegaMenuIndustryColumTwo>
                        </Link>
                      ))}
                  </div>
                  <div className="col-12 col-md-4 megamenu-image">
                    <MegaMenuIndustryColumImage>
                      {" "}
                      {imageUrl && (
                        <NextImage
                          width="236px"
                          height="336px"
                          src={imageUrl}
                          alt="featured Image"
                        />
                      )}
                    </MegaMenuIndustryColumImage>
                  </div>
                </>
              )}

              {menuStyle === "columnTwo" && (
                <>
                  <div className="col-12 col-md-6">
                    {list &&
                      list.map((eachItem, index) => (
                        <Link href={eachItem.link}>
                          <MegaMenuIndustryColum
                            key={eachItem.id}
                            onMouseEnter={() => handleMegaMenuColumn(index)}
                            onClick={() => setShowMegaMenu(false)}
                          >
                            {eachItem.text}
                          </MegaMenuIndustryColum>
                        </Link>
                      ))}
                  </div>
                  <div className="col-12 col-md-6">
                    <MegaMenuIndustryColumImage>
                      {" "}
                      {imageUrl && (
                        <NextImage
                          src={imageUrl}
                          width="236px"
                          height="336px"
                          alt="featured Image"
                        />
                      )}
                    </MegaMenuIndustryColumImage>
                  </div>
                </>
              )}
            </div>
          </div>
        </Reveal>
      </MegaMenuWrapper>
    )
  );
};

export default MegaMenu;

