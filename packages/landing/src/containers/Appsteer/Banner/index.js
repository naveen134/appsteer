import React from "react";
import Container from "common/components/UI/Container";
import Heading from "common/components/Heading";

import Lottie from "react-lottie";
import Animation from "common/assets/image/appsteer/banner-anime.json";
import Text from "common/components/Text";
import NextImage from "common/components/NextImage";
import Section, {
  Pattern,
  BannerContentWrapper,
  BannerContent,
  Figure,
} from "./banner.style";
// import dashboard from "common/assets/image/appsteer/dashboard.png";
import dashboard from "common/assets/image/appsteer/bannerImage.gif";

const Banner = ({title}) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: Animation,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <Section id="home">
      <Pattern />
      <Container width="1400px">
        <BannerContentWrapper>
          <BannerContent>
            <Heading
              className="animate__animated animate__fadeInUp"
              content={title}
              as="h1"
            />
            {/* <Text
              className="animate__animated animate__fadeInUp"
              content="Join 30,000+ businesses that use Segment's software and APIs to collect, clean, and control their customer data."
            /> */}
          </BannerContent>
          <Figure className="animate__animated animate__fadeInUp animate__fast lottiee">
            <div className="bannerImage">
              <Lottie options={defaultOptions} />
            </div>
            {/* <NextImage src={dashboard} alt="dashboard" /> */}
          </Figure>
        </BannerContentWrapper>
      </Container>
    </Section>
  );
};

export default Banner;
