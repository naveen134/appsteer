import { rgba } from "polished";
import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";
import pattern1 from "common/assets/image/appsteer/banner-pattern1.png";

import pattern2 from "common/assets/image/appsteer/banner-pattern2.png";

const Section = styled.section`
  background-color: ${themeGet("colors.black")};
  position: relative;
  z-index: 0;
  padding-bottom: 6rem;
  &::after {
    background-color: #fff;
    content: "";
    position: absolute;
    width: 100%;
    bottom: -1px;
    // height: 45px;
    @media only screen and (min-width: 768px) {
      bottom: -1px;
      //height: 80px;
    }
    @media only screen and (min-width: 1024px) {
      //  height: 90px;
    }
    @media only screen and (min-width: 1280px) {
      // height: 170px;
    }
    @media only screen and (min-width: 1366px) {
      // height: 130px;
    }
  }
  .container {
    @media only screen and (max-width: 1366px) {
      max-width: 1170px;
    }
    @media only screen and (max-width: 1024px) {
      max-width: 960px;
    }
  }
`;

export const Pattern = styled.div`
  @media only screen and (min-width: 319px) {
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    &::before,
    &::after {
      content: "";
      position: absolute;
    }
    &::before {
      background: url(${pattern1?.src}) no-repeat;
      width: 50%;
      top: 0;
      height: 100%;
      opacity: 0.6;
      @media only screen and (min-width: 319px) {
        background-size: 90%;
        transform: rotate(347deg);
        height: 139%;
      }
      @media only screen and (min-width: 768px) {
        background-size: 70%;
      }
      @media only screen and (min-width: 1024px) {
        background-size: 86%;
      }
      @media only screen and (min-width: 1280px) {
        background-size: 68%;
      }
      @media only screen and (min-width: 1440px) {
        background-size: 80%;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 50%;
      }
    }
    &::after {
      background: url(${pattern2?.src}) no-repeat;
      right: 0;
      top: 20%;
      width: 50%;
      height: 100%;
      opacity: 0.6;
      @media only screen and (min-width: 319px) {
        background-size: 30%;
        background-position: right 0;
        top: 30%;
      }
      @media only screen and (min-width: 1024px) {
        background-size: 25%;
      }
      @media only screen and (min-width: 1440px) {
        background-size: 25%;
      }
      @media only screen and (min-width: 1920px) {
        background-size: 25%;
      }
    }
  }
`;

export const BannerContentWrapper = styled.div`
  @media (min-width: 1280px) {
    min-height: 100vh;
  }
  // .lottiee {
  //   background: ${themeGet("colors.backgroundDim")};
  // }
`;

export const BannerContent = styled.div`
  padding-top: 210px;
  max-width: 1065px;
  margin: 0 auto;
  text-align: center;
  @media (max-width: 1366px) {
    padding-top: 170px;
    max-width: 950px;
  }
  @media (max-width: 1280px) {
    padding-top: 170px;
    max-width: 1050px;
  }
  @media (max-width: 1024px) {
    max-width: 690px;
  }
  @media (max-width: 768px) {
    max-width: 550px;
    padding-top: 130px;
  }
  @media (max-width: 480px) {
    padding-top: 100px;
  }
  h1 {
    color: ${themeGet("colors.white")};
    font-weight: 500;
    font-size: 74px;
    line-height: 92px;
    text-align: center;
    letter-spacing: -0.02em;
    @media (max-width: 1366px) {
      font-size: 74px;
    }
    @media (max-width: 1024px) {
      font-size: 50px;
      line-height: 72px;
    }
    @media (max-width: 768px) {
      font-size: 54px;
    }
    @media (max-width: 480px) {
      font-size: 30px;
      line-height: 42px;
    }
  }
  p {
    font-weight: 500;
    font-size: 18px;
    line-height: 2.11;
    color: ${rgba("#fff", 0.6)};
    max-width: 600px;
    margin: 0 auto;

    @media (max-width: 480px) {
      font-size: 14px;
      line-height: 1.6;
    }
  }
`;

export const Figure = styled.figure`
  margin: 30px 0 0;
  position: relative;
  text-align: center;
  z-index: 10;
  .bannerImage {
    max-width: 1178px;
    height: 144px;
    margin: auto;
    background: ${themeGet("colors.white")};
    border-radius: 20px;
    @media (min-width: 768px) {
      height: 192px;
    }
    @media (min-width: 1024px) {
      height: 290px;
    }
    @media (min-width: 1280px) {
      height: 338px;
    }
    @media (min-width: 1440px) {
      height:440px ;
    }
    @media (min-width: 1680px) {
      height:500px ;
    }
  }
  > div {
    filter: drop-shadow(0px 4px 50px rgba(86, 99, 132, 0.1));
  }
  @media (min-width: 768px) {
    margin-top: 30px;
    max-width: 600px;
    margin-left: auto;
    margin-right: auto;
  }
  @media (min-width: 1024px) {
    max-width: 1080px;
    margin-top: 50px;
  }
  @media (min-width: 1366px) {
    max-width: 1040px;
  }
  @media (min-width: 1440px) {
    max-width: 100%;
  }
  @media (min-width: 1600px) {
    margin-top: 60px;
  }
`;

export default Section;
