import React, { useReducer } from "react";

const initialState = {
  isOpen: false,
  isMegaMenuOpen: false,
};

function reducer(state, action) {
  switch (action.type) {
    case "TOGGLE":
      return {
        ...state,
        isOpen: !state.isOpen,
      };
    case "TOGGLE_MEGA_MENU":
      return {
        ...state,
        isMegaMenuOpen: action.payload,
      };
    default:
      return state;
  }
}
export const DrawerContext = React.createContext({});

export const DrawerProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <DrawerContext.Provider value={{ state, dispatch }}>
      {children}
    </DrawerContext.Provider>
  );
};
