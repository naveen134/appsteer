import softtech from "common/assets/image/appsteer/softtech.png";
import eaglestar from "common/assets/image/appsteer/eaglestar.png";
import isteer from "common/assets/image/appsteer/isteer.png";
import tekenso from "common/assets/image/appsteer/tekenso.png";
import curebay from "common/assets/image/appsteer/curebay.png";

import siteLogo from "common/assets/image/appsteer/logo-white.svg";

import facebook from "common/assets/image/appsteer/facebook.png";
import twitter from "common/assets/image/appsteer/twitter.png";
import linkedin from "common/assets/image/appsteer/linkedIn.png";

import { default as testimonial2 } from "common/assets/image/appsteer/testimonialImage2.png";
import { default as testimonial1 } from "common/assets/image/appsteer/testimonialImage1.png";
import { default as testimonial2Logo } from "common/assets/image/appsteer/testimonial1Logo.png";
import { default as testimonial1Logo } from "common/assets/image/appsteer/testimonial1Logo.png";

import shipping from "common/assets/image/appsteer/shipping.png";
import retail from "common/assets/image/appsteer/retail.png";
import constuction from "common/assets/image/appsteer/construction.png";
import manufacture from "common/assets/image/appsteer/manufacture.png";
import finance from "common/assets/image/appsteer/finance.png";
import healthcare from "common/assets/image/appsteer/healthcare.png";
import blog from "common/assets/image/appsteer/blog-featured.png";

export const menuLinks = [
  {
    label: "Use Cases",
    path: "/",
  },
  {
    label: "Resources",
    path: "/",
  },
];

export const MegaMenuContent = {
  1: {
    id: 1,
    style: "columnThree",
    title: "Industries",
    content: {
      1: {
        id: 21,
        text: "Construction and Real Estate",
        link: "/industry-vertical/construction-and-real-estate/",
        featuredImage: constuction,
        content: {
          1: {
            text: "Vendor management",
            link: "/",
          },
          2: {
            text: "Asset maintenance and management",
            link: "/",
          },
          3: {
            text: "Stakeholder management",
            link: "/",
          },
          4: {
            text: "Budget Allocation and Controls",
            link: "/",
          },
        },
      },
      2: {
        id: 22,
        text: "Health Care",
        link: "/industry-vertical/health-care/",
        featuredImage: healthcare,
        content: {
          1: {
            text: "Patient management",
            link: "/",
          },
          2: {
            text: "Billing and Claims management",
            link: "/",
          },
          3: {
            text: "Hospital facilities management",
            link: "/",
          },
          4: {
            text: "Treatment and Diagnostic services management",
            link: "/",
          },
        },
      },
      3: {
        id: 33,
        text: "Finance",
        link: "/industry-vertical/finance/",
        featuredImage: finance,
        content: {
          1: {
            text: "Customer Onboarding with e-KYC",
            link: "/",
          },
          2: {
            text: "Biometrics based Multi Factor Authentication (MFA)",
            link: "/",
          },
          3: {
            text: "Process and Document Digitization",
            link: "/",
          },
          4: {
            text: "Remote Banking (Branchless)",
            link: "/",
          },
        },
      },
      4: {
        id: 44,
        text: "Manufacturing",
        link: "/industry-vertical/manufacturing/",
        featuredImage: manufacture,
        content: {
          1: {
            text: "Shop floor Digitization",
            link: "/",
          },
          2: {
            text: "Yard Quality Inspection",
            link: "/",
          },
          3: {
            text: "Compliance management",
            link: "/",
          },
          4: {
            text: "Dashboards for Unified Data management",
            link: "/",
          },
        },
      },
      5: {
        id: 55,
        text: "Retail and E-commerce",
        link: "/industry-vertical/retail-and-e-commerce/",
        featuredImage: retail,
        content: {
          1: {
            text: "Inventory management",
            link: "/",
          },
          2: {
            text: "Order management",
            link: "/",
          },
          3: {
            text: "Vendor management",
            link: "/",
          },
          4: {
            text: "Customer management",
            link: "/",
          },
        },
      },
      6: {
        id: 66,
        text: "Shipping and Logistics",
        link: "/industry-vertical/shipping-and-logistics/",
        featuredImage: shipping,
        content: {
          1: {
            text: "Fleet management",
            link: "/",
          },
          2: {
            text: "Maintenance, Repair and Overhaul (MRO)",
            link: "/",
          },
          3: {
            text: "Warehouse management",
            link: "/",
          },
          4: {
            text: "Supply Chain and Reverse Logistics",
            link: "/",
          },
        },
      },
    },
  },
  2: {
    id: 2,
    style: "columnTwo",
    title: "Resources",
    content: {
      1: {
        id: 11,
        text: "Blog",
        link: "/",
        featuredImage: blog,
      },
      2: {
        id: 12,
        text: "Cafe",
        link: "/",
        featuredImage: blog,
      },
      3: {
        id: 13,
        text: "Comics",
        link: "/",
        featuredImage: blog,
      },
      4: {
        id: 14,
        text: "Newsroom",
        link: "/",
        featuredImage: blog,
      },
      5: {
        id: 15,
        text: "Event",
        link: "/",
        featuredImage: blog,
      },
    },
  },
};

// stats counter section
export const statsCounter = {
  blockTitle: {
    title: `10x Thinking & 
    Rapid Prototyping`,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit metus id.",
    button: {
      link: "/explore",
      label: "Explore",
    },
  },
  posts: [
    {
      count: "2.1x",
      title: "",
      text: "Happier Business Units",
    },
    {
      count: "70%",
      title: "",
      text: "Reduce dev time",
    },
    {
      count: "10x",
      title: "",
      text: "Faster IT Life cycle",
    },
    {
      count: "2.5x",
      title: "",
      text: "ROI from Low-code",
    },
  ],
};

export const testimonialData = {
  title: "Why businesses love AppSteer",
  posts: [
    {
      image: testimonial1,
      logo: testimonial1Logo,
      text: "OMG! I cannot believe that I have got a brand new landing page after getting this template we are able to use our most used e-commerce template with modern and trending design. We deliver on such an expansive with innovation agenda with so many theme projects.",
      name: "Mariana Dickey",
      designation: "Head of Design by GeekWire",
      rating: 5,
    },
    {
      image: testimonial2,
      logo: testimonial2Logo,
      text: "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.",
      name: "Jonathan Taylor",
      designation: "Head of Communication by Envato",
      rating: 4,
    },
  ],
};

export const clients = [softtech, eaglestar, isteer, tekenso, curebay];

export const footerTop = {
  about: {
    logo: siteLogo,
  },
  widgets: [
    {
      id: 2,
      title: "Usecases",
      list: [
        {
          id: 1,
          title: "Construction and Real Estate",
          link: "/industry-vertical/construction-and-real-estate",
        },
        {
          id: 2,
          title: "Health Care",
          link: "/industry-vertical/health-care",
        },
        {
          id: 3,
          title: "Finance",
          link: "/industry-vertical/finance",
        },
        {
          id: 4,
          title: "Manufacturing",
          link: "/industry-vertical/manufacturing",
        },
        {
          id: 5,
          title: "Retail and E-commerce",
          link: "/industry-vertical/retail-and-e-commerce",
        },
        {
          id: 6,
          title: "Shipping and Logistics",
          link: "/industry-vertical/shipping-and-logistics",
        },
      ],
    },
    {
      id: 3,
      title: "Resources",
      list: [
        {
          id: 1,
          title: "Blog ",
          link: "#",
        },
        {
          id: 2,
          title: "Cafe",
          link: "#",
        },
        {
          id: 3,
          title: "Comics",
          link: "#",
        },
        {
          id: 4,
          title: "Newsroom",
          link: "#",
        },
        {
          id: 5,
          title: "Event",
          link: "#",
        },
      ],
    },
    {
      id: 4,
      title: "Pathfinder",
      list: [
        {
          id: 1,
          title: "Community",
          link: "#",
        },
        {
          id: 2,
          title: "Team",
          link: "#",
        },
        {
          id: 3,
          title: "About Us",
          link: "/about-us",
        },
        {
          id: 4,
          title: "Careers",
          link: "#",
        },
        {
          id: 5,
          title: "Contact Us",
          link: "#",
        },
        {
          id: 6,
          title: "Terms & Conditions",
          link: "#",
        },
        {
          id: 7,
          title: "Appsteer DPA",
          link: "#",
        },
      ],
    },
  ],
  contactInfo: {
    title: "Contact info",
    address: `Suite 100, 5900 Balcones Drives
Austin, Texas 78731, United States`,
    phone: `+1 (972) 370-3085`,
    email: `sales@appsteer.io`,
  },
  socialLinks: [
    // {
    //   id: 1,
    //   link: "http://facebook.com",
    //   icon: facebook,
    //   label: "Facebook",
    // },
    // {
    //   id: 2,
    //   link: "http://twitter.com",
    //   icon: twitter,
    //   label: "Twitter",
    // },
    {
      id: 3,
      link: "https://www.linkedin.com/company/appsteer",
      icon: linkedin,
      label: "LinkedIn",
    },
  ],
};
