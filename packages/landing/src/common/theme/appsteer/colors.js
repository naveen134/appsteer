import { rgba } from "polished";

const colors = {
  transparent: "transparent", // 0
  black: "#191919", // 1
  white: "#ffffff", // 2
  dimWhite: "#F5F5F5",
  backgroundDim: "#FAFAFA",
  headingColor: "#0F2137", // 3
  darkHeading: "#B0B0B0",
  textColor: rgba("#0F2137", 0.65), // 4
  textColorAlt: "#666666", // 5
  textColorLight: rgba("#fff", 0.7), // 6
  labelColor: "#767676", // 7
  inactiveField: "#f2f2f2", // 8
  inactiveButton: "#b7dbdd", // 9
  inactiveIcon: "#EBEBEB", // 10
  primary: "#FFB47B", // 11
  primaryHover: "#FFF4EB", // 12
  secondary: "#FF9B3E", // 13
  borderColor: "#E5ECF4", //14
  linkColor: "#2C6FEE", // 15
  industrybannerColor: "#faebd7;",
  primaryBoxShadow: "0px 8px 20px -6px rgba(42, 162, 117, 0.57)",
  secondaryBoxShadow: "0px 8px 20px -6px rgba(237, 205, 55, 0.5)",
};

export default colors;
