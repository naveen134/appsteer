import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { themeGet } from "@styled-system/theme-get";

export const ButtonWrapper = styled.button`
  background-color: ${themeGet("colors.primary", "#ffb47b")};
  color: #191919;
  min-height: 48px;
  border-radius: 40px;
  padding-inline: 24px;
  border: none;
  cursor: pointer;
  span {
    font-weight: 500;
    font-size: 18px;
    line-height: 20px;
  }
`;

const PrimaryButton = (props) => {
  return (
    <ButtonWrapper>
      <span>{props.text}</span>
    </ButtonWrapper>
  );
};

PrimaryButton.propTypes = {
  // Type defines the thype of the Button
  type: PropTypes.string,
  //Display text
  text: PropTypes.string,
};

export default PrimaryButton;
