import React from "react";
import Modal from "react-modal";

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.75)",
    backdropFilter: "blur(10px)",
    zIndex: 9999,
  },
  content: {
    backgroundColor: "rgba(0, 0, 0, 0)",
    border: "none",
    width: "100%",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    maxWidth: "900px",
    transform: "translate(-50%, -50%)",
    // width: "40%",
    borderRadius: "30px",
  },
};

Modal.setAppElement("#__next");

const ModalContact = ({ children, open, close }) => {
  return (
    <Modal
      isOpen={open}
      style={customStyles}
      contentLabel="Connect Modal"
      onRequestClose={close}
      shouldCloseOnOverlayClick
    >
      {children}
    </Modal>
  );
};

export default ModalContact;
