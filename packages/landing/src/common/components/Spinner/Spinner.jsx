import React from "react";
import BeatLoader from "react-spinners/BeatLoader";

const Spinner = () => {
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <BeatLoader color="#FFB47B" />
    </div>
  );
};

export default Spinner;
