import "animate.css";
import ResetCSS from "common/assets/css/style";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { theme } from "common/theme/appsteer";
import Banner from "containers/Appsteer/Banner";
import StatsCounter from "containers/Appsteer/StatsCounter";

import StickyFooter from "containers/Appsteer/StickyFooter";
import Clients from "containers/Appsteer/Clients";
import Footer from "containers/Appsteer/Footer";
import Navbar from "containers/Appsteer/Navbar";
import Testimonials from "containers/Appsteer/Testimonials";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Head from "next/head";
import React, { Fragment, useEffect } from "react";
import Sticky from "react-stickynode";
import { ThemeProvider } from "styled-components";
import { fetcher } from "../../lib/api";
import { hotjar } from "react-hotjar";

const Appsteer = ({ homepage, footerData, industryList, megamenuData }) => {
  const stats = {
    blockTitle: {
      title: homepage?.StatasticsHeading,
      text: homepage?.StatasticsDescription,
      button: {
        link: "/explore",
        label: "Explore",
      },
    },
    posts: [
      homepage?.HomeStatastics1,
      homepage?.HomeStatastics2,
      homepage?.HomeStatastics3,
      homepage?.HomeStatastics4,
    ],
  };

  const testimonialData = {
    title: homepage?.HomeTestimonialTitle,
    posts: homepage?.client_testimonials?.data,
  };

  // console.log("the list ", megamenuData);

  useEffect(() => {
    hotjar.initialize(3404478, 6);
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer | Build Apps Faster Than Ever</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta
            property="og:image"
            content="https://appsteer-strapi-images-prod.s3.amazonaws.com/Group_138_272d13a0ce.png?updated_at=2023-03-16T13:21:00.512Z"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
          <script src="https://platform.illow.io/banner.js?siteId=e5c8e0ce-6fd4-49d0-8056-c541700dd668"></script>
          <script type="text/javascript" src="/static/script.js"></script>
          <meta
            name="facebook-domain-verification"
            content="dig02hlk5ttetmmpn4g35u9hdinftz"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <Banner title={homepage?.HomeBannerTitle} />
          <StatsCounter stats={stats} />
          <Testimonials testimonialData={testimonialData} />
          <Clients clients={homepage?.ClientLogos} />
          <StickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default Appsteer;

export async function getServerSideProps() {
  const homepage = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/homepage`
  );

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      homepage: homepage?.data?.attributes,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
