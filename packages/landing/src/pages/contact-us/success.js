import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import Banner from "containers/ContactUs/Banner";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import StickyFooter from "containers/Appsteer/StickyFooter";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../../lib/api";
import Container from "common/components/UI/Container";
import SuccessBanner from "containers/ContactUs/SuccessBanner/SuccessBanner";
import FormData from "form-data";
import axios from "axios";

const Success = ({ footerData, industryList, megamenuData, leadResponse }) => {
  // console.log({ leadResponse });
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>
            AppSteer- Construction and real estate | Build Apps Faster Than Ever
          </title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />
          <SuccessBanner />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default Success;
export async function getServerSideProps({ query }) {
  const { data } = query;

  const formData = JSON.parse(data);
  // console.log({ formData }, "<== Line 68");

  const leadPayload = {
    First_Name: formData?.Name,
    Lead_Source: "Appsteer website - Contact us",
    Email: formData?.Email,
    Mobile: formData?.Number,
    Company: formData?.Company,
    Designation: formData?.JobTitle,
    Description: formData?.Message,
  };

  const accessTokenPayload = new FormData();
  accessTokenPayload.append("client_id", process.env.ZOHO_CRM_CLIENT_ID);
  accessTokenPayload.append(
    "client_secret",
    process.env.ZOHO_CRM_CLIENT_SECRET
  );
  accessTokenPayload.append("refresh_token", process.env.ZOHO_REFRESH_TOKEN);
  accessTokenPayload.append("grant_type", process.env.ZOHO_CRM_GRANT_TYPE);

  const accessToken = await axios.post(
    `${process.env.ZOHO_CRM_ACCOUNT_URL}/token`,
    accessTokenPayload,
    {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }
  );

  const postLead = await axios.post(
    `${process.env.ZOHO_CRM_URL}/Leads`,
    {
      data: [{ ...leadPayload }],
      lar_id: "",
    },
    {
      headers: {
        Authorization: `Zoho-oauthtoken ${accessToken.data?.access_token?.replaceAll(
          "\r\n",
          ""
        )}`,
        "Content-Type": "application/json",
      },
    }
  );
  // console.log({ lead: postLead.data?.data?.[0] }, "<== Line 112");

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      //   leadResponse: postLead.data,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
