import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import Banner from "containers/ContactUs/Banner";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import StickyFooter from "containers/Appsteer/StickyFooter";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../../lib/api";
import MapSection from "containers/ContactUs/MapSection";

const ContactUs = ({
  contactUsData,
  footerData,
  industryList,
  megamenuData,
}) => {
  const BannerContent = {
    title: contactUsData?.BannerTitle,
    description: contactUsData?.BannerDescription,
  };

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>
            AppSteer- Contact Us | Build Apps Faster Than Ever
          </title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />
          <Banner content={BannerContent} />
          <MapSection />
          <StickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default ContactUs;
export async function getServerSideProps() {
  const contactUsData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/contact-us`
  );

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      contactUsData: contactUsData?.data?.attributes,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
