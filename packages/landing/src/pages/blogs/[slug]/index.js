import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../../../lib/api";
import BlogList from "containers/Blogs/BlogList";
import BlogStickyFooter from "containers/Blogs/BlogStickyFooter";
import BlogTypeDetailsBanner from "containers/Blogs/BlogTypeDetails/BlogTypeDetailsBanner";

const BlogTypeWiseList = ({
  footerData,
  industryList,
  megamenuData,
  blogs,
  blogTypeData,
}) => {
  // console.log({ blogTypeData });
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer- Blogs</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />
          <BlogTypeDetailsBanner data={blogTypeData?.attributes} />
          <BlogList
            textSearch
            blogs={blogs}
            blogType={blogTypeData?.attributes?.slug}
          />
          <BlogStickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default BlogTypeWiseList;
export async function getServerSideProps({ params }) {
  const { slug } = params;

  let blogTypeData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/blog-types?filters[slug]=${slug}`
  );

  let blogsData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/blogs?sort=publishDate:ASC&limit=10&filters[blog_type][slug]=${slug}`
  );

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      blogTypeData: blogTypeData?.data?.[0],
      blogs: blogsData?.data,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
