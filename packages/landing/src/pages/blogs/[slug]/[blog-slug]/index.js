import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../../../../lib/api";
import BlogStickyFooter from "containers/Blogs/BlogStickyFooter";
import BlogDetailsScreen from "containers/Blogs/BlogDetailsScreen/BlogDetailsScreen";

const BlogDetails = ({
  footerData,
  industryList,
  megamenuData,
  blogData,
  blogTypeData,
  relatedBlogs,
}) => {
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer- Blog Details</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          {/* <meta
            property="og:image"
            content={
              blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.url
            }
          /> */}
          {/* 
          <meta
            property="twitter:image"
            content={`${blogData?.[0]?.attributes?.blogBanner?.data?.attributes?.url}`}
          />
          <meta
            property="twitter:url"
            content={`https://www.appsteer.io/blogs/${blogTypeData?.attributes?.slug}/${blogData?.[0]?.attributes?.slug}`}
          />
          <meta
            property="twitter:title"
            content={blogData?.[0]?.attributes?.title}
          /> */}
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />
          <BlogDetailsScreen
            blogTypeData={blogTypeData}
            blogData={blogData}
            relatedBlogs={relatedBlogs}
          />
          <BlogStickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default BlogDetails;

export async function getServerSideProps({ params }) {
  // console.log({ first: params?.slug });
  let blogTypeData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/blog-types?filters[slug]=${params?.slug}`
  );

  let blogsData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/blogs?sort=publishDate:ASC&limit=10&filters[slug]=${params?.["blog-slug"]}`
  );
  let relatedBlogs = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/blogs?sort=publishDate:ASC&limit=3&filters[blog_type][slug]=${params?.slug}`
  );
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      relatedBlogs: relatedBlogs?.data,
      blogTypeData: blogTypeData?.data?.[0],
      blogData: blogsData?.data,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
