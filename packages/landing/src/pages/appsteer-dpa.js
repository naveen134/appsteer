import "animate.css";
import ResetCSS from "common/assets/css/style";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { theme } from "common/theme/appsteer";
import Banner from "containers/Appsteer/Banner";
import StatsCounter from "containers/Appsteer/StatsCounter";

import StickyFooter from "containers/Appsteer/StickyFooter";
import Clients from "containers/Appsteer/Clients";
import Footer from "containers/Appsteer/Footer";
import Navbar from "containers/IndustryListing/Navbar";
import Testimonials from "containers/Appsteer/Testimonials";
import {
    ContentWrapper,
    GlobalStyle,
    TermsWrapper,
} from "containers/Appsteer/appsteer.style";
import Head from "next/head";
import React, { Fragment } from "react";
import Sticky from "react-stickynode";
import { ThemeProvider } from "styled-components";
import { fetcher } from "../../lib/api";
import ScrollSpyMenu from "common/components/ScrollSpyMenu";
import Container from "common/components/UI/Container";
import ReactHtmlParser from "react-html-parser";

import styled from 'styled-components';
import { display } from "styled-system";

const StyledTable = styled.table`
  border-collapse: collapse;
  width: 100%;
`;

const StyledTh = styled.th`
  border: 1px solid black;
  padding: 8px; /* Adjust padding as needed */
  text-align: left;
`;

const StyledTd = styled.td`
  border: 1px solid black;
  padding: 8px; /* Adjust padding as needed */
  text-align: left;
`;

const AppsteerDpa = ({
    pageData,
    footerData,
    industryList,
    megamenuData,
    ContentSectionData,
}) => {
    return (
        <ThemeProvider theme={theme}>
            <Fragment>
                <Head>
                    <title>AppSteer | Build Apps Faster Than Ever</title>
                    <meta
                        name="Description"
                        content="AppSteer | Build Apps Faster Than Ever"
                    />
                    <meta name="theme-color" content="#191919" />
                    {/* Load google fonts */}
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                    <link
                        href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
                        rel="stylesheet"
                    />
                </Head>
                <ResetCSS />
                <GlobalStyle />
                <ContentWrapper>
                    <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
                        <DrawerProvider>
                            <Navbar data={megamenuData} />
                        </DrawerProvider>
                    </Sticky>

                    <Container>
                        <TermsWrapper>
                            <div className="heading">
                                <h1>Data Protection Addendum</h1>
                            </div>
                            <div className="body" style={{ display: 'block' }}>
                                <div className="scrolebleBody">
                                    <p>
                                        <span>
                                            This Data Processing Agreement (&ldquo;DPA&rdquo;) forms part of the Terms of
                                            Use
                                            (or other similarly titled written or electronic agreement addressing the same subject matter)
                                            (&ldquo;
                                        </span>
                                        <span>Agreement</span>
                                        <span>
                                            &rdquo;) between Customer (as
                                            defined in
                                            the Agreement) and &ldquo;
                                        </span>
                                        <span>
                                            AppSteer
                                            Inc
                                        </span>
                                        <span>&rdquo;</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            under which the
                                            Processor provides the Controller with the
                                            software and services (the
                                        </span>
                                        <span>&ldquo;Services&rdquo;</span>
                                        <span>
                                            ). The
                                            Controller and the Processor are individually referred to as a
                                        </span>
                                        <span>&ldquo;Party&rdquo;</span>
                                        <span>&nbsp;and collectively as the</span>
                                        <span>&ldquo;Parties&rdquo;</span>
                                        <span>.</span>
                                    </p>
                                    <p>
                                        <span>
                                            The Parties seek to implement this DPA to comply with the requirements of EU GDPR
                                            (defined hereunder) in relation to Processor&rsquo;s processing of Personal Data (as defined
                                            under the EU
                                            GDPR) as part of its obligations under the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            This DPA shall apply to Processor&rsquo;s processing of Personal Data, provided
                                            by
                                            the Controller as part of Processor&rsquo;s obligations under the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Except as modified below, the terms of the Agreement shall remain in full force
                                            and
                                            effect. &nbsp;
                                        </span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <h2>1. Definitions</h2>
                                    <p>
                                        <span>
                                            Terms not otherwise defined herein shall have the meaning given to them in the
                                            EU
                                            GDPR or the Agreement. The following terms shall have the corresponding meanings assigned to
                                            them
                                            below:
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.1.</span>
                                        <span>&nbsp;</span>
                                        <span>&quot;</span>
                                        <span>
                                            Data
                                            Transfer
                                        </span>
                                        <span>
                                            &quot; means a
                                            transfer of
                                            the Personal Data from the Controller to the Processor, or between two establishments of the
                                            Processor, or
                                            with a Sub-processor by the Processor.
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.2.</span>
                                        <span>&nbsp;</span>
                                        <span>&ldquo;</span>
                                        <span>EU</span>
                                        <span>&nbsp;</span>
                                        <span>GDPR</span>
                                        <span>
                                            &rdquo;
                                            means the Regulation (EU) 2016/679 of the
                                            European
                                            Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard
                                            to the
                                            processing of personal data and on the free movement of such data and repealing Directive
                                            95/46/EC (General
                                            Data Protection Regulation).
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.3.</span>
                                        <span>&nbsp;</span>
                                        <span>&ldquo;</span>
                                        <span>
                                            Standard Contractual
                                            Clauses
                                        </span>
                                        <span>
                                            &rdquo;
                                            means the contractual clauses attached hereto as Schedule 1 pursuant to the European
                                            Commission&rsquo;s
                                            Implementing Decision (EU) 2021/914 of 4 June 2021 on Standard Contractual Clauses for the
                                            transfer of
                                            Personal Data to processors established in third countries which do not ensure an adequate level
                                            of data
                                            protection.
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.4.</span>
                                        <span>&nbsp;</span>
                                        <span>&ldquo;</span>
                                        <span>Controller</span>
                                        <span>
                                            &rdquo; means the
                                            natural or
                                            legal person, public authority, agency, or other body which, alone or jointly with others,
                                            determines the
                                            purposes and means of the processing of personal data; where the purposes and means of such
                                            processing are
                                            determined by Union or Member State law, the controller or the specific criteria for its
                                            nomination may be
                                            provided for by Union or Member State law.
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.5.</span>
                                        <span>&nbsp;</span>
                                        <span>&ldquo;Processor&rdquo;</span>
                                        <span>
                                            means a natural or legal person,
                                            public
                                            authority, agency, or other body which processes personal data on behalf of the
                                            controller.
                                        </span>
                                    </p>
                                    <p>
                                        <span>1.6.</span>
                                        <span>&nbsp;</span>
                                        <span>&ldquo;</span>
                                        <span>Sub-processor</span>
                                        <span>
                                            &rdquo; means a
                                            processor/
                                            sub-contractor appointed by the Processor for the provision of all or parts of the Services and
                                            Processes
                                            the Personal Data as provided by the Controller.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>2.</span>
                                        <span>&nbsp;</span>
                                        <span>&nbsp;</span>
                                        <span>Purpose of this Agreement</span>
                                    </h2>
                                    <p>
                                        <span>
                                            This DPA sets out various obligations of the Processor in relation to the
                                            Processing of Personal Data and shall be limited to the Processor&rsquo;s obligations under the
                                            Agreement.
                                            If there is a conflict between the provisions of the Agreement and this DPA, the provisions of
                                            this DPA
                                            shall prevail.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Categories of Personal
                                            Data and Data
                                            Subjects
                                        </span>
                                    </h2>
                                    <p>
                                        <span>
                                            The Controller authorizes permission to the Processor to process the Personal
                                            Data
                                            to the extent of which is determined and regulated by the Controller. The current nature of the
                                            Personal
                                            Data is specified in Annex I to Schedule 1 to this DPA.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>4.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Purpose
                                            of Processing
                                        </span>
                                    </h2>
                                    <p>
                                        <span>
                                            The objective of Processing of Personal Data by the Processor shall be limited
                                            to
                                            the Processor&rsquo;s provision of the Services to the Controller and or its Client, pursuant to
                                            the
                                            Agreement.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>5.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Duration
                                            of Processing
                                        </span>
                                    </h2>
                                    <p>
                                        <span>
                                            The Processor will Process Personal Data for the duration of the Agreement,
                                            unless
                                            otherwise agreed upon in writing by the Controller.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>6.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Data
                                            Controller&rsquo;s Obligations
                                        </span>
                                    </h2>
                                    <p>
                                        <span>6.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Data
                                            Controller shall warrant that it has all necessary rights to provide the Personal Data to the
                                            Data Processor
                                            for the Processing to be performed in relation to the agreed services. To the extent required by
                                            Data
                                            Privacy LAWS &amp; Azure, Data Controller is responsible for ensuring that it provides such
                                            Personal Data to
                                            Data Processor based on an appropriate legal basis allowing lawful processing activities,
                                            including any
                                            necessary Data Subject consents to this Processing are obtained, and for ensuring that a record
                                            of such
                                            consents is maintained. Should such consent be revoked by the Data Subject, the Data Controller
                                            is
                                            responsible for communicating the fact of such revocation to the Data Processor.
                                        </span>
                                    </p>
                                    <p>
                                        <span>6.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Data
                                            Controller shall provide all natural persons from whom it collects Personal Data with the
                                            relevant privacy
                                            notice.
                                        </span>
                                    </p>
                                    <p>
                                        <span>6.3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Data
                                            Controller shall request the Data Processor to purge Personal Data when required by the Data
                                            Controller or
                                            any Data Subject whom it collects Personal Data unless the Data Processor is otherwise required
                                            to retain
                                            the Personal Data by applicable law.
                                        </span>
                                    </p>
                                    <p>
                                        <span>6.4.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Data
                                            Controller shall immediately advise the Data Processor in writing if it receives or learns of
                                            any:
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            6.4.1. Complaint or allegation indicating a violation of Data Privacy LAWS
                                            &amp;
                                            Azure regarding Personal Data;
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            6.4.2. Request from one or more individuals seeking to access, correct, or
                                            delete
                                            Personal Data;
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            6.4.3. Inquiry or complaint from one or more individuals relating to the
                                            collection, processing, use, or transfer of Personal Data; and
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            6.4.4. Any regulatory request, search warrant, or other legal, regulatory,
                                            administrative, or governmental process seeking Personal Data
                                        </span>
                                    </p>
                                    <h2>
                                        <span>7.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Data Processor&rsquo;s
                                            Obligations
                                        </span>
                                    </h2>
                                    <p>
                                        <span>7.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Processor
                                            will follow written and documented instructions received, including email, from the Controller,
                                            its
                                            affiliate, agents, or personnel, with respect to the Processing of Personal Data (each, an
                                            &ldquo;
                                        </span>
                                        <span>Instruction</span>
                                        <span>&rdquo;).</span>
                                    </p>
                                    <p>
                                        <span>7.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Processing
                                            described in the Agreement and the relating documentation shall be considered as Instruction
                                            from the
                                            Controller.
                                        </span>
                                    </p>
                                    <p>
                                        <span>7.3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            At the
                                            Data
                                            Controller&rsquo;s request, the Data Processor will provide reasonable assistance to the Data
                                            Controller in
                                            responding to/ complying with requests/ directions by Data Subject in exercising their rights or
                                            of the
                                            applicable regulatory authorities regarding Data Processor&rsquo;s Processing of Personal
                                            Data.
                                        </span>
                                    </p>
                                    <p>
                                        <span>7.4.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            In
                                            relation to
                                            the Personal Data, Data Processor shall obtain consent (where necessary) and/or provide notice
                                            to the Data
                                            Subject in accordance with Data Protection LAWS &amp; Azure to enable shared Personal Data to be
                                            provided
                                            to, and used by, the other Party as contemplated by this Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>7.5.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Where
                                            shared
                                            Personal Data is transferred outside the Data Processor&rsquo;s territorial boundaries, the
                                            transferor shall
                                            ensure that the recipient of such data is under contractual obligations to protect such Personal
                                            Data to the
                                            same or higher standards as those imposed under this Addendum and the Data Protection LAWS &amp;
                                            Azure.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>8.</span>
                                        <span>&nbsp;</span>
                                        <span>Data Secrecy</span>
                                    </h2>
                                    <p>
                                        <span>8.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            To Process
                                            the
                                            Personal Data, the Processor will use personnel who are
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            8.1.1. Informed of the confidential nature of the Personal Data, and
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            8.1.2. Perform the Services in accordance with the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>8.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Processor
                                            will regularly train individuals having access to Personal Data in data security and data
                                            privacy in
                                            accordance with accepted industry practice and shall ensure that all the Personal Data is kept
                                            strictly
                                            confidential.
                                        </span>
                                    </p>
                                    <p>
                                        <span>8.3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Processor
                                            will maintain appropriate technical and organizational measures for protection of the security,
                                            confidentiality, and integrity of the Personal Data as per the specifications as per the
                                            standards mutually
                                            agreed in writing by the Parties.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>9.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Audit
                                            Rights
                                        </span>
                                    </h2>
                                    <p>
                                        <span>9.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Upon
                                            Controller&rsquo;s reasonable request, the Processor will make available to the Controller,
                                            information as
                                            is reasonably necessary to demonstrate Processor&rsquo;s compliance with its obligations under
                                            the EU GDPR
                                            or other applicable lAWS &amp; Azure in respect of its Processing of the Personal Data.
                                        </span>
                                    </p>
                                    <p>
                                        <span>9.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            When the
                                            Controller wishes to conduct the audit (by itself or through a representative) at
                                            Processor&rsquo;s site, it
                                            shall provide at least fifteen (15) days&rsquo; prior written notice to the Processor; the
                                            Processor will
                                            provide reasonable cooperation and assistance in relation to audits, including inspections,
                                            conducted by the
                                            Controller or its representative.
                                        </span>
                                    </p>
                                    <p>
                                        <span>9.3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Controller
                                            shall bear the expense of such an audit.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>10.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Mechanism of
                                            Data Transfers
                                        </span>
                                    </h2>
                                    <p>
                                        <span>
                                            Any Data Transfer for the purpose of Processing by the Processor in a
                                            country
                                            outside the European Economic Area (the &ldquo;
                                        </span>
                                        <span>EEA</span>
                                        <span>
                                            &rdquo;)
                                            shall only take place in compliance as detailed in Schedule 1 to the DPA. Where such model
                                            clauses have not
                                            been executed at the same time as this DPA, the Processor shall not unduly withhold the
                                            execution of such
                                            template model clauses, where the transfer of Personal Data outside of the EEA is required for
                                            the
                                            performance of the Agreement.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>11.</span>
                                        <span>&nbsp;</span>
                                        <span>Sub-processors</span>
                                    </h2>
                                    <p>
                                        <span>11.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The
                                            Controller
                                            acknowledges and agrees that the Processor, may engage a third-party Sub-processor(s) in
                                            connection with the
                                            performance of the Services, provided such Sub-processor(s) take technical and organizational
                                            measures to
                                            ensure confidentiality of Personal Data shared with them; The current Sub-processors engaged by
                                            the
                                            Processors and approved by the Controller are listed in Annex III of Schedule 1 hereto. In
                                            accordance with
                                            Article 28(4) of the GDPR, the Processor shall remain liable to Controller for any failure on
                                            behalf of a
                                            Sub-processor to fulfil its data protection obligations under the DPA in connection with the
                                            performance of
                                            the Services.
                                        </span>
                                    </p>
                                    <p>
                                        <span>11.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            If the
                                            Controller
                                            has a concern that the Sub-processor(s) Processing of Personal Data is reasonably likely to
                                            cause the
                                            Controller to breach its data protection obligations under the GDPR, the Controller may object
                                            to
                                            Processor&rsquo;s use of such Sub-processor and the Processor and Controller shall confer in
                                            good faith to
                                            address such concern.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>12.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Personal
                                            Data
                                            Breach Notification
                                        </span>
                                    </h2>
                                    <p>
                                        <span>12.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Processor shall
                                            maintain defined
                                            procedures in case of a Personal Data Breach (as defined under the GDPR) and shall without undue
                                            delay
                                            notify Controller if it becomes aware of any Personal Data Breach unless such Data Breach is
                                            unlikely to
                                            result in a risk to the rights and freedoms of natural persons.
                                        </span>
                                    </p>
                                    <p>
                                        <span>12.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Processor shall
                                            provide the
                                            Controller with all reasonable assistance to comply with the notification of Personal Data
                                            Breach to
                                            Supervisory Authority and/or the Data Subject, to identify the cause of such Data Breach and
                                            take such
                                            commercially reasonable steps as reasonably required to mitigate and remedy such Data
                                            Breach.
                                        </span>
                                    </p>
                                    <p>
                                        <span>12.3.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            No Acknowledgement of
                                            Fault by
                                            Processor. Processor&rsquo;s notification of or response to a Personal Data Breach under this
                                            DPA will not
                                            be construed as an acknowledgement by Processor of any fault or liability with respect to the
                                            data
                                            incident.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>13.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Return and
                                            Deletion of Personal Data
                                        </span>
                                    </h2>
                                    <p>
                                        <span>13.1.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            The Processor shall at least thirty
                                            (30) days from the end of the Agreement or
                                            cessation of the
                                            Processor&rsquo;s Services under the Agreement, whichever occurs earlier, shall return to the
                                            Controller all
                                            the Personal Data, or if the Controller so instructs, the Processor shall have the Personal Data
                                            deleted.
                                            The Processor shall return such Personal Data in a commonly used format or in the current format
                                            in which it
                                            was stored at discretion of the Controller, soon as reasonably practicable following receipt of
                                            Controller&rsquo;s notification.
                                        </span>
                                    </p>
                                    <p>
                                        <span>13.2.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            In any case, the Processor shall
                                            delete Personal Data including all the copies of it
                                            as soon as
                                            reasonably practicable following the end of the Agreement.
                                        </span>
                                    </p>
                                    <h2>
                                        <span>14.</span>
                                        <span>&nbsp;</span>
                                        <span>
                                            Technical
                                            and
                                            Organizational Measures
                                        </span>
                                    </h2>
                                    <p>
                                        <span>
                                            Having regard to the state of technological development and the cost of
                                            implementing any measures, the Processor will take appropriate technical and organizational
                                            measures against
                                            the unauthorized or unlawful processing of Personal Data and against the accidental loss or
                                            destruction of,
                                            or damage to, Personal Data to ensure a level of security appropriate to: (a) the harm that
                                            might result
                                            from unauthorized or unlawful processing or accidental loss, destruction or damage; and (b) the
                                            nature of
                                            the data to be protected [including the measures stated in Annex II of Schedule 1]
                                        </span>
                                    </p>
                                    <p>
                                        <h2>SCHEDULE 1</h2>
                                    </p>
                                    <p>
                                        <span>ANNEX I</span>
                                    </p>
                                    <p>
                                        <h3>A. LIST OF PARTIES</h3>
                                    </p>
                                    <p>
                                        <span>Data exporter(s):</span>
                                    </p>
                                    <p>
                                        <span>Name :</span>
                                        <span>
                                            Customer (As set forth in the relevant
                                            Agreement).
                                        </span>
                                    </p>
                                    <p>
                                        <span>Address</span>
                                        <span>:</span>
                                        <span>
                                            As set forth in
                                            the
                                            relevant Agreement
                                        </span>
                                        <span>.</span>
                                    </p>
                                    <p>
                                        <span>
                                            Contact person&rsquo;s name, position, and contact details:
                                        </span>
                                        <span>
                                            As set forth in the relevant Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Activities relevant to the data transferred under these Clauses
                                        </span>
                                        <span>:</span>
                                        <span>
                                            Recipient of the Services provided by AppSteer Inc in
                                            accordance
                                            with the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>Signature and date:</span>
                                        <span>
                                            Signature and date are set out
                                            in
                                            the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>Role Controller/ Processor):</span>
                                        <span>Controller</span>
                                    </p>
                                    <p>
                                        <h>Data importer(s):</h>
                                    </p>
                                    <p>
                                        <span>Name:</span>
                                        <span>AppSteer Inc</span>
                                    </p>
                                    <p>
                                        <span>
                                            Address: 5900 Balcones Drives Austin, Texas 78731, United States
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Contact person&rsquo;s name, position, and contact details:
                                        </span>
                                        <span>
                                            Umashanker Krishnamaraju, CEO, uraju@appsteer.io
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Activities relevant to the data transferred under these Clauses
                                        </span>
                                        <span>
                                            : Provision of the Services to the Customer in accordance with the
                                            Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>Signature and date</span>
                                        <span>:</span>
                                        <span>
                                            Signature
                                            and date are set out in the Agreement.
                                        </span>
                                    </p>
                                    <p>
                                        <span>Role (controller/processor):</span>
                                        <span>Processor.</span>
                                    </p>
                                    <h3>
                                        <span>B. DESCRIPTION OF TRANSFER</span>
                                    </h3>
                                    <p>
                                        <span>
                                            Categories of data subjects whose personal data is transferred
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Customer&rsquo;s authorized users of the Services
                                        </span>
                                        <span>.</span>
                                    </p>
                                    <p>
                                        <span>Categories of personal data transferred</span>
                                    </p>
                                    <p>
                                        <span>
                                            Name, Address, Date of Birth, Age, Education, Email, Gender, Image, Job,
                                            Language, Phone, Related person, Related URL, User ID, Username.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Sensitive data transferred (if applicable) and applied restrictions or
                                            safeguards
                                            that fully take into consideration the nature of the data and the risks involved, such as for
                                            instance
                                            strict purpose limitation, access restrictions (including access only for staff having followed
                                            specialized
                                            training), keeping a record of access to the data, restrictions for onward transfers or
                                            additional security
                                            measures.
                                        </span>
                                    </p>
                                    <p>
                                        <span>No sensitive data collected.</span>
                                    </p>
                                    <p>
                                        <span>
                                            The frequency of the transfer (e.g., whether the data is transferred on a
                                            one-off
                                            or continuous basis).
                                        </span>
                                    </p>
                                    <p>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <h4>
                                        <span>Continuous basis</span>
                                    </h4>
                                    <h4>
                                        <span>
                                            Nature of the processing
                                        </span>
                                    </h4>
                                    <p>
                                        <span>Collecting</span>
                                    </p>
                                    <p>
                                        <span>Recording</span>
                                    </p>
                                    <p>
                                        <span>Organizing</span>
                                    </p>
                                    <p>
                                        <span>Structuring</span>
                                    </p>
                                    <p>
                                        <span>Storing</span>
                                    </p>
                                    <p>
                                        <span>
                                            Purpose(s) of the data transfer and further processing
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            The purpose of the transfer is to facilitate the performance of the Services
                                            more fully described in the Agreement and accompanying order forms.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            The period for which the personal data will be retained, or, if that is not
                                            possible, the criteria used to determine that period
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            The period for which the Customer Personal Data will be retained is more
                                            fully
                                            described in the Agreement, Addendum, and accompanying order forms.
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            For transfers to (sub-) processors, also specify subject matter, nature, and
                                            duration of the processing
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            The subject matter, nature, and duration of the Processing more fully
                                            described
                                            in the Agreement, Addendum, and accompanying order forms.
                                        </span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <h3>
                                        <span>C.COMPETENT SUPERVISORY AUTHORITY</span>
                                    </h3>
                                    <p>
                                        <span>
                                            Data exporter is established in an EEA country.
                                        </span>
                                    </p>
                                    <p>
                                        <span>The competent supervisory authority is</span>
                                        <span>
                                            as determined
                                            by
                                            application of Clause 13 of the EU SCCs.
                                        </span>
                                    </p>
                                    <p>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <p>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>ANNEX II</span>
                                    </p>
                                    <p>
                                        <span>
                                            TECHNICAL AND ORGANISATIONAL MEASURES INCLUDING TECHNICAL AND ORGANISATIONAL
                                            MEASURES TO ENSURE THE SECURITY OF THE DATA
                                        </span>
                                    </p>
                                    <p>
                                        <span>
                                            Description of the &nbsp;technical and organisational security measures
                                            implemented by AppSteer Inc as the data processor/data importer to ensure an appropriate level
                                            of security,
                                            taking into account the nature, scope, context, and purpose of the processing, and the risks for
                                            the rights
                                            and freedoms of natural persons.
                                        </span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <h3>
                                        <span>o</span>
                                        <span>&nbsp;</span>
                                        <span>Security</span>
                                    </h3>
                                    <ul>
                                        <li>
                                            <span>Security Management System</span>
                                            <span>.</span>
                                        </li>
                                    </ul>
                                    <ul>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Organization</span>
                                            <span>
                                                . AppSteer Inc
                                                designates
                                                qualified security personnel whose responsibilities include development, implementation,
                                                and ongoing
                                                maintenance of the Information Security Program.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Policies</span>
                                            <span>
                                                . Management reviews and
                                                supports
                                                all security related policies to ensure the security, availability, integrity and
                                                confidentiality of
                                                Customer Personal Data. &nbsp;These policies are updated at least once annually.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Assessments</span>
                                            <span>
                                                . AppSteer Inc engages
                                                a
                                                reputable independent third-party to perform risk assessments of all systems containing
                                                Customer
                                                Personal Data at least once annually.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Risk Treatment</span>
                                            <span>
                                                . AppSteer Inc
                                                maintains a
                                                formal and effective risk treatment program that includes penetration testing,
                                                vulnerability management
                                                and patch management to identify and protect against potential threats to the security,
                                                integrity or
                                                confidentiality of Customer Personal Data.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Vendor Management</span>
                                            <span>
                                                . AppSteer Inc
                                                maintains
                                                an effective vendor management program
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Incident Management</span>
                                            <span>
                                                . AppSteer Inc
                                                reviews
                                                security incidents regularly, including effective determination of root cause and
                                                corrective
                                                action.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Standards</span>
                                            <span>
                                                . AppSteer Inc operates
                                                an
                                                information security management system that complies with the requirements of
                                                SOC2.
                                            </span>
                                        </li>
                                    </ul>
                                    <h3>
                                        <li>
                                            <span>Personnel Security.</span>
                                        </li>
                                    </h3>
                                    <ul>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>
                                                AppSteer Inc personnel are required to conduct themselves
                                                in a
                                                manner consistent with the company&rsquo;s guidelines regarding confidentiality,
                                                business ethics,
                                                appropriate usage, and professional standards. AppSteer Inc conducts reasonably
                                                appropriate background
                                                checks on any employees who will have access to client data under this Agreement,
                                                including in relation
                                                to employment history and criminal records, to the extent legally permissible and in
                                                accordance with
                                                applicable local labor law, customary practice and statutory regulations.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>
                                                Personnel are required to execute a confidentiality
                                                agreement in
                                                writing at the time of hire and to protect Customer Personal Data at all times.
                                                Personnel must
                                                acknowledge receipt of, and compliance with, AppSteer Inc&rsquo;s confidentiality,
                                                privacy and security
                                                policies. Personnel are provided with privacy and security training on how to implement
                                                and comply with
                                                the Information Security Program. Personnel handling Customer Personal Data are required
                                                to complete
                                                additional requirements appropriate to their role (e.g., certifications). AppSteer
                                                Inc&rsquo;s personnel
                                                will not process Customer Personal Data without authorization.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Access Controls</span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Access Management</span>
                                            <span>
                                                . AppSteer Inc
                                                maintains
                                                a formal access management process for the request, review, approval and provisioning of
                                                all personnel
                                                with access to Customer Personal Data to limit access to Customer Personal Data and
                                                systems storing,
                                                accessing or transmitting Customer Personal Data to properly authorized persons having a
                                                need for such
                                                access. Access reviews are conducted periodically to ensure that only those personnel
                                                with access to
                                                Customer Personal Data still require it.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Infrastructure Security Personnel</span>
                                            <span>
                                                .
                                                AppSteer Inc has, and maintains, a security policy for its personnel, and requires
                                                security training as
                                                part of the training package for its personnel. AppSteer Inc&rsquo;s infrastructure
                                                security personnel
                                                are responsible for the ongoing monitoring of AppSteer Inc&rsquo;s security
                                                infrastructure, the review
                                                of the Services, and for responding to security incidents.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Access Control and Privilege Management</span>
                                            <span>
                                                .
                                                AppSteer Inc&rsquo;s and Customer&rsquo;s administrators and end users must authenticate
                                                themselves via
                                                a Multi-Factor authentication system or via a single sign on system in order to use the
                                                Services
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>
                                                Internal Data Access Processes and Policies &ndash; Access
                                                Policy
                                            </span>
                                            <span>
                                                . AppSteer Inc&rsquo;s internal data access processes and
                                                policies are
                                                designed to protect against unauthorized access, use, disclosure, alteration or
                                                destruction of Customer
                                                Personal Data. AppSteer Inc designs its systems to only allow authorized persons to
                                                access data they are
                                                authorized to access based on principles of &ldquo;least privileged&rdquo; and
                                                &ldquo;need to
                                                know&rdquo;, and to prevent others who should not have access from obtaining access.
                                                &nbsp;AppSteer Inc
                                                requires the use of unique user IDs, strong passwords, two factor authentication and
                                                carefully monitored
                                                access lists to minimize the potential for unauthorized account use. The granting or
                                                modification of
                                                access rights is based on: the authorized personnel&rsquo;s job responsibilities; job
                                                duty requirements
                                                necessary to perform authorized tasks; a need to know basis; and must be in accordance
                                                with AppSteer
                                                Inc&rsquo;s internal data access policies and training. Approvals are managed by
                                                workflow tools that
                                                maintain audit records of all changes. Access to systems is logged to create an audit
                                                trail for
                                                accountability. Where passwords are employed for authentication (e.g., login to
                                                workstations), password
                                                policies follow industry standard practices. These standards include password
                                                complexity, password
                                                expiry, password lockout, restrictions on password reuse and re-prompt for password
                                                after a period of
                                                inactivity
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Data Center and Network Security</span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Data Centers.</span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Infrastructure.</span>
                                            <span>
                                                &nbsp;AppSteer Inc
                                                has AWS
                                                &amp; AZURE as its data center.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Resiliency.</span>
                                            <span>
                                                &nbsp;Multi Availability
                                                Zones
                                                are enabled on AWS &amp; AZURE and AppSteer Inc conducts Backup Restoration Testing on
                                                regular basis to
                                                ensure resiliency.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Server Operating Systems.</span>
                                            <span>
                                                AppSteer
                                                Inc&rsquo;s servers are customized for the application environment and the servers have
                                                been hardened
                                                for the security of the Services. AppSteer Inc employs a code review process to increase
                                                the security of
                                                the code used to provide the Services and enhance the security products in production
                                                environments.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Disaster Recovery</span>
                                            <span>
                                                . AppSteer Inc
                                                replicates
                                                data over multiple systems to help to protect against accidental destruction or loss.
                                                AppSteer Inc has
                                                designed and regularly plans and tests its disaster recovery programs.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Security Logs.</span>
                                            <span>
                                                AppSteer Inc&rsquo;s
                                                systems
                                                have logging enabled to their respective system log facility in order to support the
                                                security audits,
                                                and monitor and detect actual and attempted attacks on, or intrusions into, AppSteer
                                                Inc&rsquo;s
                                                systems.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Vulnerability Management.</span>
                                            <span>
                                                AppSteer
                                                Inc
                                                performs regular vulnerability scans on all infrastructure components of its production
                                                and development
                                                environment. &nbsp;Vulnerabilities are remediated on a risk basis, with Critical, High
                                                and Medium
                                                security patches for all components installed as soon as commercially possible.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Networks and Transmission.</span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>Data Transmission</span>
                                            <span>
                                                . Transmissions
                                                on
                                                production environment are transmitted via Internet standard protocols.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>External Attack Surface</span>
                                            <span>
                                                . AWS &amp;
                                                AZURE
                                                Security Group which is equivalent to virtual firewall is in place for Production
                                                environment on AWS
                                                &amp; AZURE.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Incident Response</span>
                                            <span>
                                                . AppSteer Inc
                                                maintains
                                                incident management policies and procedures, including detailed security incident
                                                escalation procedures.
                                                AppSteer Inc monitors a variety of communication channels for security incidents, and
                                                AppSteer
                                                Inc&rsquo;s security personnel will react promptly to suspected or known incidents,
                                                mitigate harmful
                                                effects of such security incidents, and document such security incidents and their
                                                outcomes.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                        <li>
                                            <span>Encryption Technologies</span>
                                            <span>
                                                . AppSteer
                                                Inc
                                                makes HTTPS encryption (also referred to as SSL or TLS) available for data in
                                                transit.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <ul>
                                        <li>
                                            <span>
                                                Data Storage, Isolation, Authentication, and Destruction.
                                            </span>
                                            <span>
                                                AppSteer Inc stores data in a multi-tenant environment on AWS &amp;
                                                AZURE servers.
                                                Data, the Services database and file system architecture are replicated between multiple
                                                availability
                                                zones on AWS &amp; AZURE. AppSteer Inc logically isolates the data of different
                                                customers. A central
                                                authentication system is used across all Services to increase uniform security of data.
                                                AppSteer Inc
                                                ensures secure disposal of Client Data through the use of a series of data destruction
                                                processes.
                                            </span>
                                        </li>
                                        <span>&nbsp;</span>
                                    </ul>
                                    <p>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <p>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>ANNEX III</span>
                                    </p>
                                    <p>
                                        <span>LIST OF SUB-PROCESSORS</span>
                                    </p>
                                    <p>
                                        <span>
                                            The controller has authorized the use of the following sub-processors:
                                        </span>
                                    </p>
                                    <a id="t.43cb6e260bd12489c5e5fa5730a74b50e1207ac1"></a>
                                    <a id="t.1"></a>
                                    <StyledTable>
                                        <thead>
                                            <tr>
                                                <StyledTh>Sub-Processor Name</StyledTh>
                                                <StyledTh>Description of Processing</StyledTh>
                                                <StyledTh>Location of Other Processor</StyledTh>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <StyledTd>Amazon Web Services</StyledTd>
                                                <StyledTd>Hosting the Production Environment</StyledTd>
                                                <StyledTd>USA</StyledTd>
                                            </tr>
                                            <tr>
                                                <StyledTd>Microsoft Azure</StyledTd>
                                                <StyledTd>Hosting the Production Environment</StyledTd>
                                                <StyledTd>USA</StyledTd>
                                            </tr>
                                            <tr>
                                                <StyledTd>Github</StyledTd>
                                                <StyledTd>Version control for source code</StyledTd>
                                                <StyledTd>USA</StyledTd>
                                            </tr>
                                            <tr>
                                                <StyledTd>Google Workspace</StyledTd>
                                                <StyledTd>Developer Identity management, device management & communication</StyledTd>
                                                <StyledTd>USA</StyledTd>
                                            </tr>
                                            <tr>
                                                <StyledTd>FortiSASE</StyledTd>
                                                <StyledTd>Internet, endpoint security & Antivirus solution</StyledTd>
                                                <StyledTd>USA</StyledTd>
                                            </tr>
                                        </tbody>
                                    </StyledTable>

                                    <p>
                                        <span>&nbsp;</span>
                                    </p>
                                    <p>
                                        <span></span>
                                    </p>
                                </div>
                            </div>
                        </TermsWrapper>
                    </Container>

                    <Footer footerData={footerData} industryList={industryList} />
                </ContentWrapper>
            </Fragment>
        </ThemeProvider>
    );
};
export default AppsteerDpa;

export async function getServerSideProps() {
    let footerData = await fetcher(
        `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
    );

    const industryList = await fetcher(
        `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
    );

    const formatedIndustrylist = industryList?.data?.map((industry) => ({
        id: industry.id,
        linktext: industry?.attributes?.IndustryTitle,
        linkname: `/industry-vertical/${industry?.attributes?.slug}`,
    }));

    return {
        props: {
            //   pageData: pageData?.data?.attributes,
            footerData: footerData?.data?.attributes,
            industryList: formatedIndustrylist,
            megamenuData: industryList?.data
        },
    };
}