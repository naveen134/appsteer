import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import Usecase from "containers/IndustryListing/Usecase";
import Banner from "containers/IndustryListing/Banner";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { construction } from "common/data/Industry";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import StickyFooter from "containers/Appsteer/StickyFooter";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../../lib/api";
const Industryvertical = ({
  IndustryVerticalDetails,
  footerData,
  industryList,
  megamenuData,
}) => {
  const BannerContent = {
    title: IndustryVerticalDetails?.IndustryTitle,
    description: IndustryVerticalDetails?.IndustryDescription,
    bannerimage:
      IndustryVerticalDetails?.IndustryListingFeaturedimage?.data?.attributes
        ?.url,
    bannerimage1:
      IndustryVerticalDetails?.industeryfeaturedtitleimage?.data?.attributes
        ?.url,
    Usecaselists: IndustryVerticalDetails?.use_cases,
  };

  const SeoContent = {
    title: IndustryVerticalDetails?.SeoContent?.MetaTitle,
    discription: IndustryVerticalDetails?.SeoContentt?.MetaDescription,
  };
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>{SeoContent.title}</title>
          <meta name="Description" content={SeoContent.discription} />
          {/* <title>
            AppSteer- Construction and real estate | Build Apps Faster Than Ever
          </title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          /> */}
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>

          <ResetCSS />
          <GlobalStyle />
          {/* <AppWrapper> */}

          <Banner content={BannerContent} />
          <Usecase Usecases={BannerContent?.Usecaselists} />
          <StickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
        {/* </AppWrapper> */}
      </Fragment>
    </ThemeProvider>
  );
};
export default Industryvertical;
export async function getServerSideProps({ params }) {
  const { slug } = params;
  // console.log(`${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC&filters[$and][0][slug][$eq]=${slug}`,"industry vertical response");
  //const BlogListResponse = await fetcher(`${process.env.NEXT_PUBLIC_STRAPI_URL}/slugify/slugs/category/${slug}?populate=*`);
  const IndustryverticalResponse = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC&filters[$and][0][slug][$eq]=${slug}`
  );
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));
  if (IndustryverticalResponse?.data?.length === 0) {
    return { redirect: { permanent: false, destination: "/404" }, props: {} };
  } else {
    return {
      props: {
        IndustryVerticalDetails: IndustryverticalResponse?.data[0]?.attributes,
        footerData: footerData?.data?.attributes,
        industryList: formatedIndustrylist,
        megamenuData: industryList?.data,

        //   Usecaselists: IndustryverticalResponse.data[0].attributes.use_cases,
        // usecases:IndustryverticalResponse?.data?.attributes?.use_cases?.data ?IndustryverticalResponse?.data?.attributes?.use_cases.data  : [],
      },
    };
  }
  // const IndustryUsecaseverticalList = IndustryverticalResponse.data[0].attributes.use_cases.data[0]
  // // console.log(IndustryUsecaseverticalList,"IndustryverticalResponse");
}
