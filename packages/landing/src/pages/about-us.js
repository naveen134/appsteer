import React, { Fragment } from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import Head from "next/head";
import ResetCSS from "common/assets/css/style";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import Navbar from "containers/IndustryListing/Navbar";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../lib/api";
import BlogStickyFooter from "containers/Blogs/BlogStickyFooter";
import Banner from "containers/AboutUs/Banner";
import AppsteerBeginning from "containers/AboutUs/AppsteerBeginning";
import AboutUsStats from "containers/AboutUs/Stats";
import AboutUsBusinesses from "containers/AboutUs/Businesses";
import IndustryBuzz from "containers/AboutUs/IndustryBuzz";
import GameChangers from "containers/AboutUs/GameChangers";
import CoreValues from "containers/AboutUs/CoreValues";

const AboutUs = ({ footerData, industryList, megamenuData, aboutUsData }) => {
  const {
    bannerHeading,
    bannerSubHeading,
    bannerImage,
    secondSectionHeading,
    secondSectionDescription,
    secondSectionImage,
    aboutUsStats,
    aboutUsCoreValues,
    businessHeading,
    clientLogos,
    industryBuzzHeading,
    industryBuzzSubheading,
    industryBuzzImage,
    industryBuzzImageText,
    gameChangers,
  } = aboutUsData?.attributes;

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer- About us</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />

          <Banner
            bannerHeading={bannerHeading}
            bannerSubHeading={bannerSubHeading}
            bannerImage={bannerImage}
          />
          <AppsteerBeginning
            secondSectionHeading={secondSectionHeading}
            secondSectionDescription={secondSectionDescription}
            secondSectionImage={secondSectionImage}
          />
          <AboutUsStats aboutUsStats={aboutUsStats} />
          <CoreValues aboutUsCoreValues={aboutUsCoreValues} />
          <AboutUsBusinesses
            businessHeading={businessHeading}
            clientLogos={clientLogos}
          />
          <IndustryBuzz
            industryBuzzHeading={industryBuzzHeading}
            industryBuzzSubheading={industryBuzzSubheading}
            industryBuzzImage={industryBuzzImage}
            industryBuzzImageText={industryBuzzImageText}
          />
          <GameChangers gameChangers={gameChangers} />

          <BlogStickyFooter />

          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default AboutUs;

export async function getServerSideProps() {
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const aboutUsData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/about-us`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
      aboutUsData: aboutUsData?.data,
    },
  };
}
