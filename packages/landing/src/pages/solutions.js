import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../lib/api";

import RelatedUseCase from "containers/Solutions/Related";
import LookingForMore from "containers/Solutions/LookingForMore";
import Fade from "react-reveal/Fade";
import { Section } from "containers/Explore/Heading/heading.style";
import CustomNeedForm from "containers/Explore/ExploreForm/CustomNeedForm";

import {
  SolutionsStyleWrapper,
  HeadingWrapper,
  ColoredText,
  SectionHeader,
  SolutionContainerWrapper,
} from "containers/Solutions/solutions.styles";

const Solutions = ({
  UsecaseDetails,
  footerData,
  industryList,
  megamenuData,
}) => {
  const RealatedSectionData = {
    title: "More problems solved",
    relatedUsecases: UsecaseDetails?.map((eachUsecase) => ({
      title: eachUsecase?.attributes?.UseCaseTitle,
      slug: `/usecase/${eachUsecase?.attributes?.slug}`,
      image:
        eachUsecase?.attributes?.UseCaseFeaturedImage?.data?.attributes?.formats
          ?.thumbnail?.url,
    })),
  };

  // console.log({ RealatedSectionData });

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>{"Appsteer | Solutions"}</title>
          <meta name="Description" content={"Appsteer | Solutions"} />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <Section>
            <SectionHeader>
              <Fade up>
                <HeadingWrapper>
                  {RealatedSectionData.relatedUsecases?.length <= 0 ? (
                    <>
                      <div>
                        <span>Thank you for your interest in </span>
                        <ColoredText>AppSteer</ColoredText>
                      </div>

                      <div className="subHeading_content">
                        We apologize that you could not find a suitable use case
                        for your combinatorial needs. We would love to assist
                        you in creating a customized use case that meets your
                        unique requirements.{" "}
                      </div>
                      <div className="custom_form">
                        <CustomNeedForm />
                      </div>
                    </>
                  ) : (
                    <>
                      <span>Thanks! Here are some </span>
                      <ColoredText>solutions</ColoredText>
                    </>
                  )}
                </HeadingWrapper>
              </Fade>
            </SectionHeader>
          </Section>

          <SolutionsStyleWrapper>
            {/* <SectionHeader>
              <Fade up>
                <HeadingWrapper>
                  <span>Thanks! Here are some </span>
                  <ColoredText>solutions</ColoredText>
                </HeadingWrapper>
              </Fade>
            </SectionHeader> */}

            {/* <SolutionContainerWrapper> */}
            {/* <RelatedUseCase data={RealatedSectionData} /> */}
            {RealatedSectionData.relatedUsecases?.length > 0 && (
              <RelatedUseCase data={RealatedSectionData} />
            )}

            <LookingForMore />
            {/* </SolutionContainerWrapper> */}
          </SolutionsStyleWrapper>

          {/* <StickyFooter /> */}
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default Solutions;
export async function getServerSideProps({ resolvedUrl }) {
  const UsecaseDetailResponse = await fetcher(
    `${
      process.env.NEXT_PUBLIC_STRAPI_URL
    }/use-cases?sort=UseCaseTitle:ASC&${decodeURI(resolvedUrl?.split("?")[1])}`
  );
  // console.log({
  //   UsecaseDetailResponse,
  //   params: decodeURI(resolvedUrl?.split("?")[1]),
  // });
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );
  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      UsecaseDetails: UsecaseDetailResponse?.data,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
