import React, { Fragment } from "react";
import "animate.css";
import Head from "next/head";
import { ThemeProvider } from "styled-components";
import { theme } from "common/theme/appsteer";
import ResetCSS from "common/assets/css/style";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Navbar from "containers/IndustryListing/Navbar";
import Sticky from "react-stickynode";
import { DrawerProvider } from "common/contexts/DrawerContext";
import Footer from "containers/Appsteer/Footer";
import { fetcher } from "../../lib/api";
import ExploreHeading from "containers/Explore/Heading";
import ExploreForm from "containers/Explore/ExploreForm";

const Explore = ({
  footerData,
  industryList,
  megamenuData,
  obstacles,
  elimination,
  teamSize,
  futureThings,
}) => {
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer- Explore</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <ResetCSS />
          <GlobalStyle />
          <ExploreHeading />
          <ExploreForm
            obstacles={obstacles}
            industries={industryList}
            elimination={elimination}
            futureThings={futureThings}
            teamSize={teamSize}
          />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default Explore;
export async function getServerSideProps() {
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  const obstaclesData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/obstacles`
  );
  const eliminationData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/eliminates`
  );
  const teamSizeData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/team-sizes`
  );
  const futureThingsData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/future-things`
  );

  return {
    props: {
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
      obstacles: obstaclesData?.data,
      elimination: eliminationData?.data,
      teamSize: teamSizeData?.data,
      futureThings: futureThingsData?.data,
    },
  };
}
