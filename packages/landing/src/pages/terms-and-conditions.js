import "animate.css";
import ResetCSS from "common/assets/css/style";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { theme } from "common/theme/appsteer";
import Banner from "containers/Appsteer/Banner";
import StatsCounter from "containers/Appsteer/StatsCounter";

import StickyFooter from "containers/Appsteer/StickyFooter";
import Clients from "containers/Appsteer/Clients";
import Footer from "containers/Appsteer/Footer";
import Navbar from "containers/IndustryListing/Navbar";
import Testimonials from "containers/Appsteer/Testimonials";
import {
  ContentWrapper,
  GlobalStyle,
  TermsWrapper,
} from "containers/Appsteer/appsteer.style";
import Head from "next/head";
import React, { Fragment } from "react";
import Sticky from "react-stickynode";
import { ThemeProvider } from "styled-components";
import { fetcher } from "../../lib/api";
import ScrollSpyMenu from "common/components/ScrollSpyMenu";
import Container from "common/components/UI/Container";
import ReactHtmlParser from "react-html-parser";

const TermsAndConditions = ({
  pageData,
  footerData,
  industryList,
  megamenuData,
  ContentSectionData,
}) => {
  const data = {
    menuItems: ContentSectionData.map((eachItem, index) => ({
      label: `${index + 1}. ${eachItem.Heading}`,
      path: `#section${index}`,
      offset: "100",
    })),
  };

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer | Build Apps Faster Than Ever</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>

          <Container>
            <TermsWrapper>
              <div className="heading">
                <h1>Terms & Conditions</h1>
              </div>
              <div className="body">
                <div className="scrollspyMenu">
                  <ScrollSpyMenu
                    menuItems={data.menuItems}
                    drawerClose={true}
                    offset={-100}
                  />
                </div>
                <div className="scrolebleBody">
                  {ContentSectionData.map((eachItem, index) => (
                    <section id={`section${index}`}>
                      <h2>{`${index + 1}. ${eachItem.Heading}`}</h2>
                      {ReactHtmlParser(eachItem.Content)}
                    </section>
                  ))}
                </div>
              </div>
            </TermsWrapper>
          </Container>

          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default TermsAndConditions;

export async function getServerSideProps() {
  const ContentSectionData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/terms-and-conditions`
  );

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      //   pageData: pageData?.data?.attributes,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
      ContentSectionData: ContentSectionData?.data?.attributes?.ContentSection,
    },
  };
}
