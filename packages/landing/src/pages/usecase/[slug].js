// import React, { Fragment } from "react";
// import Head from "next/head";
// import Sticky from "react-stickynode";
// import { ThemeProvider } from "styled-components";
// import ResetCSS from "common/assets/css/style";

// import Navbar from "containers/Appsteer/Navbar";
// import {
//   GlobalStyle,
//   ContentWrapper,
// } from "containers/Appsteer/appsteer.style.js";
// import { theme } from "common/theme/appsteer";

// import BannerSection from "containers/Appsteerlanding/Banner";
// import Solution from "containers/Appsteerlanding/Solutions";
// import Related from "containers/Appsteerlanding/Related";
// import { DrawerProvider } from "common/contexts/DrawerContext";

// // lib fetch
// import { fetcher } from "../../../lib/api";
// const Industryvertical = ({ UsecaseDetailData }) => {
//   // console.log(UsecaseDetailData)

//   return (
//     <ThemeProvider theme={theme}>
//       <Fragment>
//         {/* Start agency head section */}
//         <Head>
//           <title>Agency | A react next landing page</title>
//           <meta name="Description" content="React next landing page" />
//           <meta name="theme-color" content="#10ac84" />

//           {/* Load google fonts */}
//           <link
//             href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500&family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
//             rel="stylesheet"
//           />
//         </Head>
//         <ResetCSS />
//         <GlobalStyle />
//         {/* End of agency head section */}
//         {/* Start agency wrapper section */}
//         <ContentWrapper>
//           <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
//             <DrawerProvider>
//               <Navbar />
//             </DrawerProvider>
//           </Sticky>
//           <BannerSection usecasebanner={UsecaseDetailData} />
//           <Solution usecasebanner={UsecaseDetailData} />
//           {/* <Related /> */}
//         </ContentWrapper>
//         {/* End of agency wrapper section */}
//       </Fragment>
//     </ThemeProvider>
//   );
// };
import "animate.css";
import ResetCSS from "common/assets/css/style";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { theme } from "common/theme/appsteer";
import Banner from "containers/Usecase/Banner";
import Solution from "containers/Usecase/Solutions";
import StatsCounter from "containers/Usecase/StatsCounter";
import RelatedUseCase from "containers/Usecase/Related";

import StickyFooter from "containers/Appsteer/StickyFooter";
import Clients from "containers/Appsteer/Clients";
import Footer from "containers/Appsteer/Footer";
import Navbar from "containers/Appsteer/Navbar";
import Testimonials from "containers/Appsteer/Testimonials";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Head from "next/head";
import React, { Fragment } from "react";
import Sticky from "react-stickynode";
import { ThemeProvider } from "styled-components";
import { fetcher } from "../../../lib/api";

const Usecase = ({ UsecaseDetais, footerData, industryList, megamenuData }) => {
  const BannerSectionData = {
    usecaseCategory: {
      name: UsecaseDetais?.industry_vertical?.data?.attributes?.IndustryTitle,
      slug: UsecaseDetais?.industry_vertical?.data?.attributes?.slug,
    },
    usecaseTitle: UsecaseDetais?.UseCaseTitle,
    title: UsecaseDetais?.UseCaseBannerTitle,
    description: UsecaseDetais?.UseCaseBannerDescription,
    bannerImage: UsecaseDetais?.UseCaseFeaturedImage?.data?.attributes?.url,
  };

  const SolutionSectionData = {
    title: UsecaseDetais?.SolutionTitle,
    description: UsecaseDetais?.SolutionListItems,
    bannerImage: UsecaseDetais?.SolutionImage?.data?.attributes?.url,
  };

  const BusinessImpactSection = [
    UsecaseDetais?.BussinessImpact1,
    UsecaseDetais?.BussinessImpact2,
    UsecaseDetais?.BussinessImpact3,
    UsecaseDetais?.BussinessImpact4,
  ];

  const SeoContent = {
    title: UsecaseDetais?.SeoContent?.MetaTitle,
    discription: UsecaseDetais?.SeoContent?.MetaDescription,
  };

  const testimonialData = {
    title: UsecaseDetais?.TestimonialsTitle,
    posts: UsecaseDetais?.client_testimonials?.data,
  };
  // console.log(testimonialData, "the usecase page");
  const RealatedSectionData = {
    title: "More problems solved",
    relatedUsecases:
      UsecaseDetais?.industry_vertical?.data?.attributes?.use_cases?.data?.map(
        (eachUsecase) => ({
          title: eachUsecase?.attributes?.UseCaseTitle,
          slug: `/usecase/${eachUsecase?.attributes?.slug}`,
          image:
            eachUsecase?.attributes?.UseCaseFeaturedImage?.data?.attributes
              ?.formats?.thumbnail?.url,
        })
      ),
  };

  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>{SeoContent.title}</title>
          <meta name="Description" content={SeoContent.discription} />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <Banner data={BannerSectionData} />
          <Solution data={SolutionSectionData} />
          <StatsCounter data={BusinessImpactSection} />
          <Testimonials testimonialData={testimonialData} useCase={true} />
          <RelatedUseCase data={RealatedSectionData} />
          <StickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default Usecase;
export async function getServerSideProps({ params }) {
  const { slug } = params;
  const UsecaseDetailResponse = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/use-cases?sort=UseCaseTitle:ASC&filters[$and][0][slug][$eq]=${slug}`
  );
  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );
  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));
  // const RelatedUsecaseResponse = await fetcher(`${process.env.NEXT_PUBLIC_STRAPI_URL}/use-cases?sort=UseCaseTitle:ASC&filters[$and][0][industry_vertical][IndustryTitle][$eq]=${UsecaseDetailResponse?.industry_vertical?.data.attributes.slug}`)
  // console.log(RelatedUsecaseResponse,"industry vertical response");

  return {
    props: {
      UsecaseDetais: UsecaseDetailResponse.data[0].attributes,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
