import "animate.css";
import ResetCSS from "common/assets/css/style";
import { DrawerProvider } from "common/contexts/DrawerContext";
import { theme } from "common/theme/appsteer";
import Banner from "containers/Team/Banner";
import StickyFooter from "containers/Team/StickyFooter";
import Footer from "containers/Appsteer/Footer";
import Navbar from "containers/Appsteer/Navbar";
import {
  ContentWrapper,
  GlobalStyle,
} from "containers/Appsteer/appsteer.style";
import Head from "next/head";
import React, { Fragment } from "react";
import Sticky from "react-stickynode";
import { ThemeProvider } from "styled-components";
import { fetcher } from "../../lib/api";
import CoreAdvisoryCard from "containers/Team/CoreAdvisoryTeam";
import FounderTeam from "containers/Team/FounderTeam";
import GroupImages from "containers/Team/TeamGroupImages";

const Appsteer = ({ teampage, footerData, industryList, megamenuData }) => {
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <Head>
          <title>AppSteer | Build Apps Faster Than Ever</title>
          <meta
            name="Description"
            content="AppSteer | Build Apps Faster Than Ever"
          />
          <meta name="theme-color" content="#191919" />
          {/* Load google fonts */}
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600&display=swap"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar data={megamenuData} />
            </DrawerProvider>
          </Sticky>
          <Banner data={teampage} />
          <CoreAdvisoryCard data={teampage} />
          <FounderTeam data={teampage} />
          <GroupImages data={teampage} />
          <StickyFooter />
          <Footer footerData={footerData} industryList={industryList} />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
export default Appsteer;

export async function getServerSideProps() {
  const teampage = await fetcher(`${process.env.NEXT_PUBLIC_STRAPI_URL}/team`);

  const homepage = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/homepage`
  );

  let footerData = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/footer`
  );

  const industryList = await fetcher(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/industry-verticals?sort=slug:ASC`
  );

  const formatedIndustrylist = industryList?.data?.map((industry) => ({
    id: industry.id,
    linktext: industry?.attributes?.IndustryTitle,
    linkname: `/industry-vertical/${industry?.attributes?.slug}`,
  }));

  return {
    props: {
      teampage: teampage?.data?.attributes,
      homepage: homepage?.data?.attributes,
      footerData: footerData?.data?.attributes,
      industryList: formatedIndustrylist,
      megamenuData: industryList?.data,
    },
  };
}
