const withPlugins = require("next-compose-plugins");

const nextConfig = {
  // distDir: '../../dist/functions/next'
  images: {
    domains: [
      "pbs.twimg.com",
      "appsteerwebsitecontainer",
      "appsteer-strapi-images-prod.s3.amazonaws.com",
    ],
  },
};

module.exports = withPlugins([], nextConfig);
